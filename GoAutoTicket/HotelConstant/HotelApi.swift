//
//  HotelApi.swift
//  GoAutoTicket
//
//  Created by Tushar Lama on 07/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation


struct HotelAPI {
    static let baseURL = "https://preprod-coreapi.goautoticket.com/"
    
    static func getAuthAPI() -> String{
        return baseURL + "api/v1/auth"
    }
}
