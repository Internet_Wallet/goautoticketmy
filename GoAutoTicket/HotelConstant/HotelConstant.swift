//
//  HotelConstant.swift
//  GoAutoTicket
//
//  Created by Tushar Lama on 07/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//


import Foundation

struct HotelConstant {
    static let kHotelRoomDetail = "HotelRoomDetail"
    static let kHotelDetailCell = "HotelDetailCell"
    static let kHotelAboutHotelCell = "HotelAboutHotelCell"
    static let kHotelDetailRoomCell = "HotelDetailRoomCell"
    static let kHotelRoomDetailCell = "HotelRoomDetailCell"
    static let kHotelDetailCellRoom = "HotelDetailCellRoom"
    static let kHotelDetailFacilityCell = "HotelDetailFacilityCell"
    static let kHotelGuestInfoVC = "HotelGuestInfoVC"
    static let kHotelGuestInfoCell = "HotelGuestInfoCell"
    static let kHotelGuestDetailCell = "HotelGuestDetailCell"
    static let kHotelGuestContactDetailCell = "HotelGuestContactDetailCell"
    static let kHotelGuestAddressCell = "HotelGuestAddressCell"
    static let kHotelGuestPriceDetailCell = "HotelGuestPriceDetailCell"
    static let kHotelNameArray = ["Winner Inn Yangon","Ambassodar Hill Yangon","SAvoy Yangon","Hotel Glory Yangon","Yuzana Hotel Yangon","Hotel Yankin Yangon","Vista Residence","Sunny Holiday Hotel","New Yangon Hotel","Hotel H Valley Yangon","Hotel Oriana Yangon","Asahi Hotel"]
    static let kHotelGuestSpecialRequestCell = "HotelGuestSpecialRequestCell"
    static let kApplicationID = "dbee2f5c580793ed8452121b16fc94d7721700b3eb6f8d3d91e6d853abc3216c"
}



struct  HotelServerMessage {
    static let kSuccess = "Success"
    static let kErrorMessage = "Error During Server Call"
    static let kError = "Error!!!"
    static let kNoInterNet = "No Internet Connection Low"
}
