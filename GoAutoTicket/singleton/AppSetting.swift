//
//  AppSetting.swift
//  LC-Travel
//
//  Created by Tushar Lama on 04/10/18.
//  Copyright © 2018 Tushar Lama. All rights reserved.
//

import Foundation

protocol AppSettable
{
    func loadFromSettings(appSettings:AppSettings)
    func saveToSettings(appSettings:AppSettings) -> AppSettings
}

struct AppSettings
{
    let userDefaults : UserDefaults = UserDefaults.standard
    
    enum SettingItems : String
    {
      
        case
       
        authKey  = "authKey",
        applicationID = "applicationID"
        
        static func getKeys() -> [String] {
            return [authKey.rawValue,
                    applicationID.rawValue]
        }
    }
    
    fileprivate var cachedSettings : [String : Any] = [String : Any]()
    
    mutating func loadSettings()
    {
        let keys : [String] = SettingItems.getKeys()
        keys.forEach({
            if let value = userDefaults.object(forKey: $0) {
                cachedSettings[$0] = value
            }
        })
    }
    
    func saveSettings(){
        let keys : [String] = Array(cachedSettings.keys)
        keys.forEach({
            userDefaults.set(cachedSettings[$0], forKey: $0)
        })
        userDefaults.synchronize()
    }
    
    func getSettingStringFor(key:SettingItems) -> String?
    {
        return cachedSettings[key.rawValue] as? String
    }
    
    func getSettingFor(key:SettingItems) -> Any?
    {
        return cachedSettings[key.rawValue]
    }
    
    mutating func updateSettingFor(key:SettingItems, value:Any)
    {
        cachedSettings[key.rawValue] = value
    }
}
