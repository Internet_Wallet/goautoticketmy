//
//  ApplicationState.swift
//  LC-Travel
//
//  Created by Tushar Lama on 04/10/18.
//  Copyright © 2018 Tushar Lama. All rights reserved.
//

import Foundation

final class ApplicationState{
    
    var currentUser: ActiveUser!
    
    // the application settings object
    fileprivate var appSettings : AppSettings = AppSettings();
    
    
    public static let sharedAppState : ApplicationState = {
        let instance = ApplicationState();
        return instance;
    }()
    
    
    private init()
    {
        
        currentUser = ActiveUser()
        // load in the app settings
        // takes place after default user creation in case we are keeping login from previous run
        loadAppSettings()
        
        
    }
}

extension ApplicationState // Save/load settings
{
    /// loads in previous settings and then set the vars in proper objects
    internal func loadAppSettings()
    {
        appSettings.loadSettings()
        
        // update the active user with needed settings
        currentUser.loadFromSettings(appSettings: appSettings)
    }
    
    /// saveAppSettings - saves settings to device through appSettings
    func saveAppSettings()
    {
        // get the user settings
        appSettings = currentUser.saveToSettings(appSettings: appSettings)
        
        appSettings.saveSettings()
    }
    
}

