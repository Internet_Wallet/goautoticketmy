//
//  ActiveUSer.swift
//  LC-Travel
//
//  Created by Tushar Lama on 04/10/18.
//  Copyright © 2018 Tushar Lama. All rights reserved.
//

import Foundation

final class ActiveUser {
    
    var authKey : String = ""
    var applicationID: String = ""
    
    
    // the application settings object
    fileprivate var appSettings : AppSettings = AppSettings();
    
    init()
    {
    }
    
}

extension ActiveUser {
  
    func saveApplicationID(applicationID: String){
        self.applicationID = applicationID
    }
    
    func saveAuthKey(authKey: String){
        self.authKey = authKey
        
    }

   
}

extension ActiveUser : AppSettable
{
    
    func loadFromSettings(appSettings:AppSettings){
        //gather information here
        authKey = appSettings.getSettingStringFor(key: .authKey) ?? ""
        applicationID = appSettings.getSettingStringFor(key: .applicationID) ?? ""
        
    }
    
    func saveToSettings(appSettings:AppSettings) -> AppSettings{
        var appSettings : AppSettings = appSettings
        //save auhtKey
        appSettings.updateSettingFor(key: .authKey, value: authKey)
        appSettings.updateSettingFor(key: .applicationID, value: applicationID)
      
        return appSettings
    }
}

extension ActiveUser{
    func logoutCurrentUser(){
//        email = ""
//        passWord = ""
//        agentID = ""
//      //  authKey = ""
//      //  sessionKey = ""
//        maskID = ""
//        cartID = ""
//        creditBalance = ""
    }
}
