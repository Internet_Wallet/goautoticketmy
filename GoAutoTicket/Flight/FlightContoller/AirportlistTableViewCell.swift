//
//  AirportlistTableViewCell.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 1/22/20.
//  Copyright © 2020 E J ANTONY. All rights reserved.
//

import UIKit

class AirportlistTableViewCell: UITableViewCell {
    
    
    @IBOutlet var airportcodeLabel: UILabel!
    @IBOutlet var airportnameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
