//
//  TravelDetailViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 1/3/20.
//  Copyright © 2020 E J ANTONY. All rights reserved.
//

import UIKit

class TravelDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var travelFromLabel: UILabel!
    
    @IBOutlet var travelTolabel: UILabel!
    
    
    @IBOutlet var travelerView: UIView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var paymentButton: UIButton!
    @IBOutlet var priceTypeButton: UIButton!
    @IBOutlet var pricedetailsButton: UIButton!
    
    @IBOutlet var travelTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        travelerView.layer.cornerRadius = 2
        travelerView.layer.shadowColor = UIColor.black.cgColor
        travelerView.layer.shadowOffset = CGSize(width: 0.5, height: 4.0); //Here your control your spread
        travelerView.layer.shadowOpacity = 0.5
        travelerView.layer.shadowRadius = 5.0 //Here your
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
         return 1
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FlightListTableViewCell
         
         cell.selectionStyle = .none
         return cell
        }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
        
     }
    
    @IBAction func backButtonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func paymentButtonClick(_ sender: Any) {
        var homeVC: PaymentOptionsViewController?
        
        homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "PaymentOptionsViewController") as? PaymentOptionsViewController
        
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
