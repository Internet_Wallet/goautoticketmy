//
//  FilterTableViewCell.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 12/18/19.
//  Copyright © 2019 E J ANTONY. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
    }
    @IBOutlet var filterImageView: UIImageView!
    
    @IBOutlet var categoryNameLabel: UILabel!
    @IBOutlet var subcategoryNameLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
