//
//  FlightDetailViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 12/19/19.
//  Copyright © 2019 E J ANTONY. All rights reserved.
//

import UIKit

class FlightDetailViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var fromLabel: UILabel!
    @IBOutlet var toLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var flightDetailTableView: UITableView!
    
    @IBOutlet var priceDetailsButton: UIButton!
    
    @IBOutlet var bookButton: UIButton!
    
    @IBOutlet var priceLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
       
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
     return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FlightDetailTableViewCell
        
        cell.selectionStyle = .none
     
     
     return cell
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func pricedetailButtonClick(_ sender: Any) {
        var homeVC: PriceDetailsViewController?
                     
                     homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "PriceDetailsViewController") as? PriceDetailsViewController
                     
                     if let homeVC = homeVC {
                         navigationController?.pushViewController(homeVC, animated: true)
                     }
        
    }
    
    @IBAction func bookButtonClick(_ sender: Any) {
        
        var homeVC: ContactDetailsViewController?
        
        homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "ContactDetailsViewController") as? ContactDetailsViewController
        
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
}
