//
//  FlightListViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 12/16/19.
//  Copyright © 2019 E J ANTONY. All rights reserved.
//

import UIKit
class FlightListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var deparutreLabel: UILabel!
    @IBOutlet var arrivalLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var filterButton: UIButton!
    
    @IBOutlet var optionsSegement: UISegmentedControl!
    
    @IBOutlet var flightListTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        optionsSegement.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)

              // color of other options
        optionsSegement.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray], for: .normal)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
        return 10
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FlightListTableViewCell
        
        cell.selectionStyle = .none
        return cell
       }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var homeVC: FlightDetailViewController?
                           
                           homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "FlightDetailViewController") as? FlightDetailViewController
                           
                           if let homeVC = homeVC {
                               navigationController?.pushViewController(homeVC, animated: true)
                           }
    }
    
    @IBAction func optionsSegmentClick(_ sender: Any) {
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    @IBAction func filterButtonClick(_ sender: Any) {
        
        var homeVC: FilterViewController?
        
        homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController
        
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
