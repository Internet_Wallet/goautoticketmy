//
//  AirportListViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 1/22/20.
//  Copyright © 2020 E J ANTONY. All rights reserved.
//

import UIKit

class AirportListViewController: UIViewController{
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var flightSearchBar: UISearchBar!
    
    @IBOutlet var flightTableView: UITableView!
    
    var airportcode = [String]()
    var  airportname = [String]()
    var  mainAirportName = [String]()

    var leavingstr: String = ""
    var goStr : String = ""
    
    var locationLat : String = ""
    var locationLong : String = ""
    
    var  airlocationArray = [String]()
    var  mainairlocationArray = [[String : Any]]()
    
    var locationSectionTitles = [String]()
    
    var airsearchparams:[String : Any] = [:]
    
    weak var applybuttondelegate : FlightHomeViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
          self.navigationController?.setNavigationBarHidden(true, animated: false)
         self.location()
        flightSearchBar.delegate = self
        flightSearchBar.showsCancelButton = false
        if #available(iOS 13.0, *) {
                  flightSearchBar.searchTextField.textColor = .white
              } else {
                  // Fallback on earlier versions
              }
        self.flightTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    func locationparamsapi(){
        
        var locationparams:[String : Any] = [:]
        
        locationparams = [
            "Location": [
                "Latitude": locationLat,
                "Longitude": locationLong
            ],
            "Request": ""
        ]
        print("Location Params", locationparams)
        self.apirequestAirsearchcall(parameter:locationparams)
    }
    
    func location(){
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
            VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
                        //                        if let street = addressDic["street"]  {
                        //                            self.township = street
                        //                        }
                        //                        if let region = addressDic["region"]  {
                        //                            self.devision = region
                        //                        }
                        //                        self.googleAddress = "\(addressDic)"
                        
                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                        self.locationparamsapi()
                    }
                    
                }
            }
        }
        else{
            // showLocationDisabledpopUp()
        }
    }
    @IBAction func backButtonClick(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: false)
    }
}

  // Search Bar Delegate
  extension AirportListViewController : UISearchBarDelegate{
        // Search Bar Delegate
         
         func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
          {
            //  UnApprovedvendorListArray = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
              var tempArray = [String]()
              if searchText.count == 0
              {
                  flightTableView.reloadData()
                  return
              }
              if searchText.count >= 3{
                airsearchparams = [

                "Location": [
                    "Latitude": locationLat,
                    "Longitude": locationLong
                ],
                "Request": searchText
                ]
               self.apirequestAirsearchcall(parameter: airsearchparams)
              }
              for i in 0..<mainairlocationArray.count
              {
                let vendorDict:[[String:Any]] = [mainairlocationArray[i]]
                 let vendorId: String = String(describing: vendorDict)
                 if substring(searchText, existsIn: vendorId)
                  {
                      tempArray.append(vendorId)
                  }
              }
              airlocationArray = tempArray
            //  flightTableView.reloadData()
          }
          
          func substring(_ substr: String, existsIn str: String) -> Bool
          {
              if str.range(of: substr) != nil
              {
                  return true
              }
              return false
          }
          
          func searchBarTextDidBeginEditing(_ templeSearchBar: UISearchBar)
          {
              flightSearchBar.setShowsCancelButton(true, animated: true)
              flightTableView.allowsSelection = false
              flightTableView.isScrollEnabled = true
              var searchBarTextField: UITextField? = nil
              for mainview: UIView in flightSearchBar.subviews {
                  for subview: UIView in mainview.subviews {
                      if (subview is UITextField) {
                          searchBarTextField?.backgroundColor = UIColor.clear
                          searchBarTextField = (subview as? UITextField)
                          break
                      }
                  }
              }
              searchBarTextField?.enablesReturnKeyAutomatically = false
          }
        
        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
             flightTableView.allowsSelection = true
        }
          
          func updateSearchResults(for searchController: UISearchController)
             {
                 let searchText: String? = flightSearchBar.text
                 if searchText?.count == 0
                 {
                    // airportname = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
                     flightTableView.reloadData()
                     return
                 }
             }
          
          func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
          {
              flightSearchBar.showsCancelButton = false
              flightSearchBar.resignFirstResponder()
           //   flightSearchBar = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
              flightTableView.reloadData()
          }
          
          func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
          {
              flightSearchBar.showsCancelButton = false
              flightSearchBar.resignFirstResponder()
              // Do the search...
          }
        
    }
 // UITableView Delgate and Datasource
extension AirportListViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.mainairlocationArray.count > 0{
           return self.mainairlocationArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                return self.mainairlocationArray[section]["type"] as? String
            }
        case 1:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                 return self.mainairlocationArray[section]["type"] as? String
            }
        case 2:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                return self.mainairlocationArray[section]["type"] as? String
            }
        default:
            return nil // when return nil no header will be shown
        }
        return nil
      
        
       
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if let val = mainairlocationArray[section]["item"] {
            return (val as AnyObject).count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        guard let headerView = view as? UITableViewHeaderFooterView else { return }
        headerView.textLabel?.textColor = .red
        headerView.backgroundView?.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // var cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BusLocationSearchTableViewCell
         var cell:AirportlistTableViewCell? = self.flightTableView.dequeueReusableCell(withIdentifier: "Cell") as? AirportlistTableViewCell
        if cell == nil {
           cell = AirportlistTableViewCell(style: .default, reuseIdentifier: "Cell")
        }
  
         if let val = self.mainairlocationArray[indexPath.section]["item"] {
            let itemDict:[[String:Any]] = val as!  [[String:Any]]
            cell?.airportnameLabel.text = itemDict[indexPath.row]["name"] as? String
            cell?.airportcodeLabel.text = itemDict[indexPath.row]["id"] as? String
        }
            
        cell?.selectionStyle = .none
        
         self.flightTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell:AirportlistTableViewCell? = self.flightTableView.dequeueReusableCell(withIdentifier: "Cell") as? AirportlistTableViewCell
         let itemDict:[[String:Any]]
        if let val = self.mainairlocationArray[indexPath.section]["item"] {
            itemDict = val as!  [[String:Any]]
            cell?.airportnameLabel.text = itemDict[indexPath.row]["name"] as? String
            cell?.airportcodeLabel.text = itemDict[indexPath.row]["id"] as? String
        }
    
        if leavingstr == "Leaving"{
             self.applybuttondelegate?.flightSelected(airportName: cell?.airportcodeLabel.text ?? "", status: "Leaving")
        }
            
        else
        {
            self.applybuttondelegate?.flightSelected(airportName: cell?.airportcodeLabel.text ?? "", status: "Going")
            
        }
        
        self.navigationController?.popViewController(animated: false)
    }
}

// Api Call
extension AirportListViewController: ApiRequestProtocol{
    
    
    func apirequestAirsearchcall(parameter:Any){
        //   AppUtility.showLoading(self.view)
           let apiReqObj = ApiRequestClass()
           apiReqObj.customDelegate = self
           DispatchQueue.main.async {
               print("****",parameter)
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/air/search/location")!, requestData:parameter, requestType: RequestType.RequestAirSearch, httpMethodName: "POST")
           }
       }
       
       func httpResponse(responseObj: Any, reqType: RequestType)
       {
           DispatchQueue.main.async{
               
               AppUtility.hideLoading(self.view)
               
               if let respMsg = responseObj as? String
               {
                   AppUtility.alert(message: respMsg, title: "", controller: self)
               }
               else
               {
                   
                   var respDict = [String:Any]()
                   if responseObj is [Any]
                   {
                      // self.tokenarray = responseObj as! [Any]
                   }
                   else if responseObj is [String:Any]
                   {
                       respDict = responseObj as! Dictionary<String,Any>
                       
                   }
                   
                   if reqType == RequestType.RequestTypeEnvironment
                   {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                  
                                
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }

                       else
                       {
                          AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                   }
                   else if reqType == RequestType.RequestAirSearch {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                self.mainairlocationArray.removeAll()
                                self.mainairlocationArray = (respDict["response"] as? [[String : Any]])!
//                                for i in 0..<arrItem.count
//                                  {
//
//                                    if let sectiontitle = arrItem[i]["type"]{
//                                       println_debug(sectiontitle)
//                                        self.locationSectionTitles.append(sectiontitle as! String )
//                                        if let val = arrItem[i]["item"] {
//                                            let itemDict:[[String:Any]] = val as!  [[String:Any]]
//                                            self.mainbuslocationArray.append(contentsOf: itemDict)
//                                        }
//                                    }
//
//                                }
//
                               
                                print(self.mainairlocationArray)
                                self.flightTableView.reloadData()
                                
                                // AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }
                       else
                       {
                           AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                       
                   }
                   else{
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                   }
               }
           }
       }
    
}


