//
//  ContactDetailsViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 12/23/19.
//  Copyright © 2019 E J ANTONY. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
class ContactDetailsViewController: UIViewController {

    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var firstnameTF: ACFloatingTextfield!
    
    @IBOutlet var lastnamrTF: ACFloatingTextfield!
    
    @IBOutlet var emailTF: ACFloatingTextfield!
    
    @IBOutlet var primaryContactTF: ACFloatingTextfield!
    
    @IBOutlet var homePhoneTF: ACFloatingTextfield!
    
    @IBOutlet var cityTF: ACFloatingTextfield!
    
    @IBOutlet var iternaryTF: ACFloatingTextfield!
    
    
    @IBOutlet var maleCheckButton: UIButton!
    
    @IBOutlet var maleButton: UIButton!
    
    @IBOutlet var femaleCheckButton: UIButton!
    
    @IBOutlet var femaleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
    self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        
        var homeVC: TravelDetailViewController?
               
               homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "TravelDetailViewController") as? TravelDetailViewController
               
               if let homeVC = homeVC {
                   navigationController?.pushViewController(homeVC, animated: true)
               }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
