//
//  FlightHomeViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 12/13/19.
//  Copyright © 2019 E J ANTONY. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import SCLAlertView

protocol FlightHomeViewControllerDelegate : class {
    func flightSelected(airportName: String, status:String)
}


@available(iOS 13.0, *)
class FlightHomeViewController: UIViewController, UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,ApiRequestProtocol,FlightHomeViewControllerDelegate {
 
    @IBOutlet var leavingTF: ACFloatingTextfield!
    
    @IBOutlet var goingTF: ACFloatingTextfield!
    
    @IBOutlet var departuredateTF: ACFloatingTextfield!
    
    @IBOutlet var returndateTF: ACFloatingTextfield!
    
   
    @IBOutlet var returndateHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var returndateView: UIView!
    
    
    @IBOutlet var triptypeSegement: UISegmentedControl!
    
    @IBOutlet var updownButton: UIButton!
    
    @IBOutlet var adultCountLabel: UILabel!
    
    @IBOutlet var adultStepper: UIStepper!
    
    @IBOutlet var childcountLabel: UILabel!
    
    @IBOutlet var childStepper: UIStepper!
    
    @IBOutlet var infantcountLabel: UILabel!
    
    @IBOutlet var infantStepper: UIStepper!
    
    @IBOutlet var advanceoptionSwitch: UISwitch!
    
    @IBOutlet var advancedOptionsView: UIView!
    
    @IBOutlet var anyTF: ACFloatingTextfield!
    
    @IBOutlet var searchButton: UIButton!
    
    var  pickerTextView = UITextView()
    
    var picker = UIDatePicker()
    
    var strTF = String()
    
    var typePicker = UIPickerView()
    
    var typearr = [String]()
    
     var flightListArray = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
         self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.backgroundColor = .red

        self.navigationController?.navigationBar.tintColor = .red
        
        self.navigationController?.navigationItem.title = "FLIGHT"
        
      // self.navigationController?.setNavigationBarHidden(false, animated: false)
    
        
        triptypeSegement.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)

        // color of other options
        triptypeSegement.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray], for: .normal)
        
//        let loadingView = RSLoadingView(effectType: RSLoadingView.Effect.twins)
//        loadingView.show(on: view)
     
        returndateHeightConstraint.constant = 0
        returndateView.isHidden = true
        
        advancedOptionsView.isHidden = true
        
        
        anyTF.rightViewMode = .always
        anyTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
        typePicker.delegate = self
        typePicker.dataSource = self
        
        typearr = ["Economy","Business","First Class","Premium Economy"]
        let colorTop =  UIColor(red: 255.0/255.0, green: 197.0/255.0, blue: 191.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 129.0/255.0, green: 20.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at:0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = true
        
//         let defaults = UserDefaults.standard
//
//        if defaults.object(forKey: "LeavingFrom") != nil
//            {
//                leavingTF.text = (defaults.object(forKey: "LeavingFrom")) as? String
//
//            }
//        else
//        {
//            leavingTF.text = ""
//        }
//
//        if defaults.object(forKey: "GoingTo") != nil
//            {
//
//                  goingTF.text = (defaults.object(forKey: "GoingTo")) as? String
//            }
//
//        else{
//            goingTF.text = ""
//        }
        
        
    }
    
    func apicall(){
        
       
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        
          DispatchQueue.main.async {
        
           apiReqObj.sendHttpRequest(requestUrl: URL(string:"")!, requestData: "", requestType: RequestType.RequestTypeFlight, httpMethodName: "GET")
        }
    }

    func flightSelected (airportName: String, status:String) {
        print(airportName,status)
        
        if status == "Leaving" {
        leavingTF.text = airportName
        } else {
        goingTF.text = airportName
        }
    }
    
  func textFieldDidBeginEditing(_ textField: UITextField) {
    //delegate method
    if textField == leavingTF
          {
              
            guard let homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: AirportListViewController.self)) as? AirportListViewController else { return }
                homeVC.leavingstr = "Leaving"
            homeVC.applybuttondelegate = self //as? FlightHomeViewControllerDelegate
                navigationController?.pushViewController(homeVC, animated: true)
           textField.resignFirstResponder()
    }
  
    if textField == goingTF
       {
           var homeVC: AirportListViewController?
           
           homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "AirportListViewController") as? AirportListViewController
            homeVC?.goStr = "Going"
        homeVC?.applybuttondelegate = self //as? FlightHomeViewControllerDelegate

           if let homeVC = homeVC {
               navigationController?.pushViewController(homeVC, animated: true)
           }
        textField.resignFirstResponder()
       }
    
    if textField == anyTF
    {
        anyTF.inputView = typePicker
        anyTF.tintColor = UIColor.clear
    }
    
    
    if textField == departuredateTF
    {
        
        strTF = "1"
        picker = UIDatePicker(frame: CGRect(x: 0, y: 210, width: 320, height: 200))

           picker.date = Date(timeIntervalSinceNow: TimeInterval(2))
           picker.minimumDate = Date(timeIntervalSinceNow: TimeInterval(0))

           picker.datePickerMode = .date
           picker.backgroundColor = UIColor.white
           picker.addTarget(self, action: #selector(updateTextField(_:)), for: .valueChanged)

           departuredateTF.inputView = picker

           let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
           numberToolbar.barStyle = .default
           numberToolbar.tintColor = UIColor.gray
           numberToolbar.items = [UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)), UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneWithNumberPad))]

           numberToolbar.sizeToFit()
           departuredateTF.inputAccessoryView = numberToolbar
    }
    else{
        
        strTF = "2"

        picker = UIDatePicker(frame: CGRect(x: 0, y: 210, width: 320, height: 200))

         picker.date = Date(timeIntervalSinceNow: TimeInterval(2))
         picker.minimumDate = Date(timeIntervalSinceNow: TimeInterval(0))

        picker.datePickerMode = .date
        picker.backgroundColor = UIColor.white
      
        picker.addTarget(self, action: #selector(updateTextField(_:)), for: .valueChanged)

        returndateTF.inputView = picker
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            numberToolbar.barStyle = .default
            numberToolbar.tintColor = UIColor.gray
            numberToolbar.items = [UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)), UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneWithNumberPad))]

            numberToolbar.sizeToFit()
           returndateTF.inputAccessoryView = numberToolbar
        
    }
   

     
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         
        
    }
    @objc func cancelNumberPad() {
        
        if strTF == "1"
        {
            let bool1 = departuredateTF.resignFirstResponder()
            if bool1 {
                departuredateTF.text = ""
            }
        }
        else
        {
            
           let bool1 =  returndateTF.resignFirstResponder()
            if bool1 {
                returndateTF.text = ""
            }
            
        }
        
        
    }

    @objc func doneWithNumberPad() {

        if strTF == "1"
        {
            let bool1 = departuredateTF.resignFirstResponder()
            
            print(bool1)
        }
        else
        {
            let bool1 =  returndateTF.resignFirstResponder()
             print(bool1)
        }

    }

    @objc func updateTextField(_ sender: Any?) {
        //    _Picker1 = (UIDatePicker*)self.dateOfBirthTF.inputView;
        //    self.dateOfBirthTF.text = [NSString stringWithFormat:@"%@",_Picker1.date];

        
        if strTF == "1"
        {
            let df = DateFormatter()
            // df.dateStyle = NSDateIntervalFormatterMediumStyle;

            df.dateFormat = "dd/MM/yyyy"
            departuredateTF.text = "\(df.string(from: picker.date))"
        }
        else
        {
            let df = DateFormatter()
            // df.dateStyle = NSDateIntervalFormatterMediumStyle;

            df.dateFormat = "dd/MM/yyyy"
            returndateTF.text = "\(df.string(from: picker.date))"
        }
        
    }
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
     }
     
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         
        return typearr.count
     }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return typearr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        anyTF.text =  typearr[row]
        anyTF.textColor = .red
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
  
        let pickerLabel = UILabel()
        
        pickerLabel.textAlignment = .center
        
        pickerLabel.text = typearr[row]
        
        return pickerLabel
    }
    
    
    @IBAction func updownButtonClick(_ sender: Any) {
        
      //   _ = SCLAlertView().showWarning("GAT", subTitle: "Fields Should not empty")
        
//        let appearance = SCLAlertView.SCLAppearance(
//            showCircularIcon: true
//        )
//
//        if leavingTF.text == ""
//        {
//
//                    let alertView = SCLAlertView(appearance: appearance)
//                    let alertViewIcon = UIImage(named: "flight") //Replace the IconImage text with the image name
//                    alertView.showInfo("GOAutoTicket", subTitle: "Enter Leaving From", circleIconImage: alertViewIcon)
//        }
//
//        else if goingTF.text == ""
//        {
//            let alertView = SCLAlertView(appearance: appearance)
//                               let alertViewIcon = UIImage(named: "flight") //Replace the IconImage text with the image name
//                               alertView.showInfo("GOAutoTicket", subTitle: "Enter Going To", circleIconImage: alertViewIcon)
//        }
  
        let value = leavingTF.text
        leavingTF.text = goingTF.text
        goingTF.text = value
       
    }
    
    @IBAction func triptypeSegementChanged(_ sender: Any) {
        
       // RSLoadingView.hide(from: view)
        
        if triptypeSegement.selectedSegmentIndex == 0{
          
            returndateHeightConstraint.constant = 0
            returndateView.isHidden = true
            
        }
        else
        {
            returndateHeightConstraint.constant = 50
            returndateView.isHidden = false
        }
        
    }
    
    @IBAction func adultClick(_ sender: UIStepper) {
        let count = Int(sender.value)
           adultCountLabel.text = String(count)
    }
    
    @IBAction func childClick(_ sender: UIStepper) {
        
        let count = Int(sender.value)
        childcountLabel.text = String(count)
    }
    
    @IBAction func infantClick(_ sender: UIStepper) {
        
        let count = Int(sender.value)
        infantcountLabel.text = String(count)
    }
    
    @IBAction func advancedoptionClick(_ sender: Any) {
        
        if advanceoptionSwitch.isOn
        {
             advancedOptionsView.isHidden = false
        }
        else
        {
             advancedOptionsView.isHidden = true
        }
    }
    @IBAction func backButtonClick(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func searchButtonClick(_ sender: Any) {
        
        if leavingTF.text == ""{
            AppUtility.alert(message: "Please Enter leaving from city".localized, title: "", controller: self)
        }
        else if goingTF.text == ""{
            AppUtility.alert(message: "Please Enter going to city".localized, title: "", controller: self)
        }
        else if leavingTF.text == goingTF.text{
           AppUtility.alert(message: "The From and To city cannot be same".localized, title: "", controller: self)
        }
        else if departuredateTF.text == ""{
            AppUtility.alert(message: "Please Select depart date".localized, title: "", controller: self)
        }
        if triptypeSegement.selectedSegmentIndex == 1{
            if returndateTF.text == ""{
                AppUtility.alert(message: "Please Select return date".localized, title: "", controller: self)
            }
        }
        else{
            var homeVC: FlightListViewController?
            
            homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "FlightListViewController") as? FlightListViewController
            
            if let homeVC = homeVC {
                navigationController?.pushViewController(homeVC, animated: true)
            }
        }
        
      
        
    }
    
    func httpResponse(responseObj: Any, reqType: RequestType)
    {
        DispatchQueue.main.async{
            
            
            if let respMsg = responseObj as? String
            {
                let responseAlert = UIAlertController(title: "Alert", message: respMsg, preferredStyle: UIAlertController.Style.alert)
                responseAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                UIApplication.shared.keyWindow?.rootViewController?.present(responseAlert, animated: true, completion: nil)
            }
            else
            {
                
                var respDict = [String:Any]()
                if responseObj is [Any]
                {
                  //  self.vendorsListArray = responseObj as! [Any]
                }
                else if responseObj is [String:Any]
                {
                    respDict = responseObj as! Dictionary<String,Any>
                }
                
                if reqType == RequestType.RequestTypeFlight
                {
                   if self.flightListArray.count > 0
                    {
//                        self.showEntrieslabel.isHidden = false
//                        self.entityTF.isHidden = false
//                        self.vendorSearchBar.isHidden = false
//
//                        UserDefaults.standard.set(self.vendorsListArray, forKey: "VendorsList")
//                        self.vendorsListTableView.reloadData()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "GAT", message: respDict["Message"] as? String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}
