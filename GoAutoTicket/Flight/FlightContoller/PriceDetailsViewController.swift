//
//  PriceDetailsViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 12/21/19.
//  Copyright © 2019 E J ANTONY. All rights reserved.
//

import UIKit

class PriceDetailsViewController: UIViewController {
    
    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var priceSegemnt: UISegmentedControl!
    
    @IBOutlet var basePriceLabel: UILabel!
    
    @IBOutlet var basePriceTotalLabel: UILabel!
    
    @IBOutlet var surchargeLabel: UILabel!
    
    @IBOutlet var surchargePriceLabel: UILabel!
    
    @IBOutlet var totalPriceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func priceSegmentClick(_ sender: Any) {
    }
    

}
