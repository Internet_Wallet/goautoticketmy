//
//  PaymentOptionsViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 1/7/20.
//  Copyright © 2020 E J ANTONY. All rights reserved.
//

import UIKit

class PaymentOptionsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var basePriceView: UIView!
    @IBOutlet var baseCountLabel: UILabel!
    @IBOutlet var baseTotalLabel: UILabel!
    
    @IBOutlet var surchargeView: UIView!
    @IBOutlet var surchargeCountLabel: UILabel!
    @IBOutlet var surchargeTotalLabel: UILabel!
    
    @IBOutlet var totalAmountLabel: UILabel!
    
    @IBOutlet var paymentoptionsTableView: UITableView!
    
    var expandedIndexPath: IndexPath = IndexPath(row: 0, section: 0)
    
     var categoryarr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let row = 1
        let section = 2
        self.expandedIndexPath = IndexPath(row: row, section: section)
        
        categoryarr = ["OkDollar","PaymentGateway","2C2P"]
        
        
        let footerView = UIView()
        footerView.frame = CGRect(x: 20, y: 0, width: paymentoptionsTableView.frame.size.width, height: 1)
        footerView.backgroundColor = paymentoptionsTableView.separatorColor
        paymentoptionsTableView.tableFooterView = footerView
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categoryarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! paymentoptionTableViewCell
        
        cell.selectionStyle = .none
     
        cell.paymentTypeLabel.text = categoryarr[indexPath.row]
        
        cell.checkButton.tag = indexPath.row
        cell.checkButton.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
        
     return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.beginUpdates()

        if (self.expandedIndexPath != nil) && indexPath.compare(self.expandedIndexPath) == .orderedSame {
            self.expandedIndexPath = IndexPath(row: 0, section: 0)
        } else {
            self.expandedIndexPath = indexPath
        }

        tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.compare(self.expandedIndexPath) == .orderedSame {
            return 130.0 // Expanded height
        }
        return 40.0 // Normal height
    }
    
    @objc func buttonSelected(_ checkButton: UIButton?) {
        let i: Int = checkButton!.tag
        _ = paymentoptionsTableView.cellForRow(at: IndexPath(row: i, section: 0)) as? paymentoptionTableViewCell
        
        print(checkButton?.tag as Any)
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    

}
