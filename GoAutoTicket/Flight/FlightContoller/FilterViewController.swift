//
//  FilterViewController.swift
//  GATFlightModule
//
//  Created by E J ANTONY on 12/17/19.
//  Copyright © 2019 E J ANTONY. All rights reserved.
//

import UIKit
import RangeSeekSlider
class FilterViewController: UIViewController,RangeSeekSliderDelegate, UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet var rangeSlider: RangeSeekSlider!
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var filterTableView: UITableView!
    
      var categoryarr = [String]()
     var  subcategoryarr = [String]()
     var categoryarrimg = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        rangeSlider.minValue = 524589
        rangeSlider.maxValue = 1050060
        
        rangeSlider.selectedMinValue = 524589
        rangeSlider.selectedMaxValue = 1050060
        
        rangeSlider.delegate = self
        
        categoryarr = ["Airline","Price Type","Currency","Stops"]
        
        subcategoryarr = ["Airindia,silkAir,Srilankan Airlines,Indigo,Thai Airways,Air China","Refundable,non-refundable","MMK","Non stop", "single stop"]
        
        categoryarrimg = ["airlines","pricetype","currency","stops"]
        
        
        let footerView = UIView()
        footerView.frame = CGRect(x: 20, y: 0, width: filterTableView.frame.size.width, height: 1)
        footerView.backgroundColor = filterTableView.separatorColor
        filterTableView.tableFooterView = footerView
    }

    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
          if slider === rangeSlider {
            
            let strMinValue = rangeSlider.numberFormatter.string(from: minValue as NSNumber)
            let strMaxValue = rangeSlider.numberFormatter.string(from: maxValue as NSNumber)

            print("Standard slider updated. Min Value: \(strMinValue ?? "") Max Value: \(strMaxValue ?? "")")
          }
      }

      func didStartTouches(in slider: RangeSeekSlider) {
          print("did start touches")
      }

      func didEndTouches(in slider: RangeSeekSlider) {
          print("did end touches")
      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FilterTableViewCell
        
        cell.selectionStyle = .none
               
        cell.categoryNameLabel.text = categoryarr[indexPath.row]

        cell.subcategoryNameLabel.text = subcategoryarr[indexPath.row];
        
        cell.filterImageView.image = UIImage(named: categoryarrimg[indexPath.row])
               return cell
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
