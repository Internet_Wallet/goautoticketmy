//
//  AppDelegate.swift
//  GoAutoTicket
//
//  Created by vamsi on 7/4/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON
import ObjectMapper
import JWT
@_exported import SkyFloatingLabelTextField
//@available(iOS 13.0, *)
@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var localBundle : Bundle?
    var currentLanguage = "my"
     var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        print("For test")
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        setNavigationBarProperties()
        if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
                       //println_debug("Very First Time")
                       currentLanguage = "en"
                       UserDefaults.standard.set("en", forKey: "currentLanguage")
                       if let path = Bundle.main.path(forResource: "en", ofType: "lproj") {
                           localBundle = Bundle(path: path)
                       }
                   }else {
                       
                       if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), let path =  Bundle.main.path(forResource: lang, ofType: "lproj") {
                           currentLanguage = lang
                           localBundle = Bundle(path: path)
                       }
                       //   println_debug(currentLanguage as? String)
                   }
        
           let userlogin = UserDefaults.standard.value(forKey: "UserLogedin") as? String
        
             if userlogin != ""
             {
                 self.dashboardNavigation()
             }
             else
             {
                 self.goToLoginPage()
             }
        
        return true
    }
    
    func setNavigationBarProperties(){
        UINavigationBar.appearance().barTintColor = UIColor(red: 203.0/255.0, green: 56.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    func dashboardNavigation(){
        
        var navigationController: UINavigationController? = nil
        if #available(iOS 13.0, *) {
            let storyboard = UIStoryboard(name: "BusMain", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
            if let homeViewController = homeViewController {
                navigationController = UINavigationController(rootViewController: homeViewController)
            }
        }
        else{
            //  var homeVC: HomeViewController?
            let  homeVC = UIStoryboard(name: "BusMain", bundle: nil)
            let dashbord = homeVC.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
            var objNav: UINavigationController? = nil
            if let dashbord = dashbord {
                objNav = UINavigationController(rootViewController: dashbord)
            }
            if objNav != nil {
                navigationController = UINavigationController(rootViewController: dashbord!)
            }
        }
        window = (UIApplication.shared.delegate?.window)!
        window?.rootViewController = navigationController
    }
    func goToLoginPage(){
        var navigationController: UINavigationController? = nil
        if #available(iOS 13.0, *){
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            
            if let homeViewController = homeViewController {
                navigationController = UINavigationController(rootViewController: homeViewController)
            }
        }
        else{
            let  homeVC = UIStoryboard(name: "Login", bundle: nil)
                let dashbord = homeVC.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                var objNav: UINavigationController? = nil
                if let dashbord = dashbord {
                    objNav = UINavigationController(rootViewController: dashbord)
                }
                if objNav != nil {
                    navigationController = UINavigationController(rootViewController: dashbord!)
                }
            }
            window = (UIApplication.shared.delegate?.window)!
            window?.rootViewController = navigationController
    }
  
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
     func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
         // Called when a new scene session is being created.
         // Use this method to select a configuration to create the new scene with.
         return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
     }

     @available(iOS 13.0, *)
     func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
         // Called when the user discards a scene session.
         // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
         // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
     }
    
    // MARK: Language Localization
          
          func setSeletedlocaLizationLanguage(language : String) {
              currentLanguage = language
              if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                  localBundle = Bundle(path: path)
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
              }
          }
          
          func getStringAccordingToLanguage(language : String, key: String) -> String {
              if let path = Bundle.main.path(forResource: language, ofType: "lproj") {
                  let bundle  = Bundle(path: path)
                  return NSLocalizedString(key, tableName: "Localization", bundle: bundle!, value: "", comment: "")
              }
              return key
          }
          
          func getSelectedLanguage() -> String {
              return currentLanguage
          }
          
    func getlocaLizationLanguage(key : String) -> String {
        if let bundl = localBundle {
            return NSLocalizedString(key, tableName: "Localization", bundle: bundl, value: "", comment: "")
        } else {
            return key
        }
    }


}

