//
//  IntialViewController.swift
//  GoAutoTicket
//
//  Created by vamsi on 7/8/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit
import CLWaterWaveView
import Alamofire
import SwiftyJSON
import ObjectMapper
import JWT
@available(iOS 13.0, *)
class IntialViewController: UIViewController,  ApiRequestProtocol {

    @IBOutlet var waveView: CLWaterWaveView!
    var timer: Timer?
    var tokenarray = [Any]()
    
    @IBOutlet var languageView: UIView!
    
    @IBOutlet var languageLabel: UILabel!
    
    @IBOutlet var languageTableView: UITableView!
    
    @IBOutlet var skipButton: UIButton!
    
    var LanguagesArray = [Any]()
    var Countrylogos = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   self.performSegue(withIdentifier: "loginController", sender: self)
        // Do any additional setup after loading the view.
      
        languageView.layer.cornerRadius = 8;
        languageView.layer.masksToBounds = true;
        
        LanguagesArray = ["English","Burmese Unicode","Burmese Zawgyi"]
        Countrylogos = ["uk","myanmar","myanmar"]
        
        languageView.isHidden = true
        
        waveView.amplitude = 32.0
        waveView.speed = 0.009
        waveView.angularVelocity = 0.40
        waveView.depth = 1.09
        waveView.startAnimation()
      //  self.startTimer()
        self.location()
        self.apicall()
        self.apicallEnvironment()
    }
    
    func location(){
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
            VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
                        //                        if let street = addressDic["street"]  {
                        //                            self.township = street
                        //                        }
                        //                        if let region = addressDic["region"]  {
                        //                            self.devision = region
                        //                        }
                        //                        self.googleAddress = "\(addressDic)"
                        
//                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
//                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                    }
                    
                }
            }
        }
        else{
            // showLocationDisabledpopUp()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = true
    }
    func startTimer() {
          timer = Timer.scheduledTimer(timeInterval: 15.0,
                                       target: self,
                                       selector: #selector(eventWith(timer:)),
                                       userInfo: [ "foo" : "bar" ],
                                       repeats: true)
      }

      // Timer expects @objc selector
      @objc func eventWith(timer: Timer!) {
          let info = timer.userInfo as Any
        
          waveView.stopAnimation()
          print(info)
        
      }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
          let touch = touches.first
         
          
          if touch?.view == self.languageView {
              languageView.isHidden = false
          }
          else
          {
               languageView.isHidden = true
          }
      }
    
    @IBAction func languageClick(_ sender: Any) {
         languageView.isHidden = false
        
    }
    
    @IBAction func skipButtonClick(_ sender: Any) {
                let storyBoard : UIStoryboard = UIStoryboard(name: "BusMain", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                if let isValidObj = nextViewController{
                    self.navigationController?.pushViewController(isValidObj, animated: true)
                }
//        
        
//        let story = UIStoryboard.init(name: "BusMain", bundle: nil)
//                       let vc = story.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
//                       let navController = UINavigationController(rootViewController: vc!)
//        navController.pushViewController(vc!, animated: true)

    }
    
    
    func apicallEnvironment(){
        AppUtility.showLoading(self.view)
                let apiReqObj = ApiRequestClass()
                apiReqObj.customDelegate = self
               
        let parameters = [:] as AnyObject
                
                  DispatchQueue.main.async {
                
                   apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/application/environment")!, requestData: parameters, requestType: RequestType.RequestTypeEnvironment, httpMethodName: "POST")
                }
    }
    
    func apicall(){
         
        AppUtility.showLoading(self.view)
         let apiReqObj = ApiRequestClass()
         apiReqObj.customDelegate = self
        
         let parameters = ["Request": ["ApplicationID": "f1040f0e7eb8ef7fb730f056a501e7b90c3adcd3dfd5366e286cb1af4f775400"]] as [String : Any]
         
           DispatchQueue.main.async {
         
            apiReqObj.sendHttpAuthenticationRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/auth")!, requestData: parameters, requestType: RequestType.RequestTypeAuthenticate, httpMethodName: "POST")
         }
     }
    
    
        func httpResponse(responseObj: Any, reqType: RequestType)
        {
            
            DispatchQueue.main.async{
                
                AppUtility.hideLoading(self.view)
                
                if let respMsg = responseObj as? String
                {
                    let responseAlert = UIAlertController(title: "Alert", message: respMsg, preferredStyle: UIAlertController.Style.alert)
                    responseAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    UIApplication.shared.keyWindow?.rootViewController?.present(responseAlert, animated: true, completion: nil)
                }
                else
                {
                    
                    var respDict = [String:Any]()
                    if responseObj is [Any]
                    {
                       // self.tokenarray = responseObj as! [Any]
                    }
                    else if responseObj is [String:Any]
                    {
                        respDict = responseObj as! Dictionary<String,Any>
                        
                    }
                    
                    if reqType == RequestType.RequestTypeAuthenticate
                    {
                        if respDict.count > 0
                        {
                        
                         let itemDict:[String:Any] = respDict["response"] as!  Dictionary<String,Any>
                           
                            
                            let authToken = itemDict["authToken"] as! String
                            print("@@@@",authToken)
                            let applicationID = itemDict["applicationID"] as! String
                             print("@@@@",applicationID)
                            
                              
                            UserDefaults.standard.set(authToken, forKey: "password")
                            UserDefaults.standard.set(applicationID, forKey: "username")
                            UserDefaults.standard.synchronize()
                            print("username:----",authToken)
                            print("passowrd:---",applicationID)
                            let loginString = "\(applicationID):\(authToken)"
                            let loginData = loginString.data(using: String.Encoding.utf8)!
                            let base64LoginString = loginData.base64EncodedString()
                            let authString = "Basic \(base64LoginString)"
                            
                             UserDefaults.standard.set(authString, forKey: "auth")
                            UserDefaults.standard.synchronize()
                        }
                        else
                        {
                            let alert = UIAlertController(title: "GAT", message: respDict["Message"] as? String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    } else if reqType == RequestType.RequestTypeEnvironment{
                        if respDict.count > 0
                        {
                        
                         let itemDict:[String:Any] = respDict["response"] as!  Dictionary<String,Any>
                           
                            
                            if let cobrandDetails : [[String : Any]] = itemDict["cobrandDetails"] as? [[String : Any]]{
                                
                                let SignupTerms = cobrandDetails.filter({ cobrandObject in
                                    return cobrandObject["shortDesc"] as? String == "SIGNUPTERMS"
                                })
                                
                                if SignupTerms.count > 0 {
                                    if let signUpTermsTextInHTML : String = SignupTerms[0]["value"] as? String{
                                        UserDefaults.standard.setValue(signUpTermsTextInHTML, forKey: userdefaultKeys.signupTerms)
                                    }
                                }
                                
                            }
//                            print(itemDict["portalType"])
//                            print(itemDict["cobrandDetails"])
//                            print(itemDict["portalCountry"])
//                            print(itemDict["documentTypes"])
//                            print(itemDict["availableCurrencies"])
//                            print(itemDict["portalName"])
//                            print(itemDict["isPhoneNumberForLoginEnabled"])
//                            print(itemDict["availableLanguages"])
//                            print(itemDict["isCoTravellerEnabled"])
//                            print(itemDict["socialNetworkIntegration"])
//                            print(itemDict["paymentGatewayInputInfo"])
//                            print(itemDict["preferredCountryCode"])
                            
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "GAT", message: respDict["Message"] as? String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }


}

@available(iOS 13.0, *)
extension IntialViewController : UITableViewDelegate,UITableViewDataSource {
  
   
    func numberOfSections(in tableView: UITableView) -> Int {
              return 1
       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LanguagesArray.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cellIdentifier: String = "LanguageCell"
           
           
                 
                 var cell:LanguageTableViewCell? = self.languageTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? LanguageTableViewCell
                 
               //  cell?.separatorInset = UIEdgeInsets.zero
                 
                 if cell == nil {
                     cell = LanguageTableViewCell(style: .default, reuseIdentifier: cellIdentifier)
                 }
                  //bankDetailsArray.remove(at: 1)
               
                 cell?.countryflagImage.image = UIImage(named: Countrylogos[indexPath.row] as! String)
                 cell?.languageNameLabel.text = String(describing: LanguagesArray[indexPath.row])
                
                 
                 return cell!
           
       }
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                languageView.isHidden = true
           
    }
    
    
}
