//
//  LanguageTableViewCell.swift
//  GoAutoTicket
//
//  Created by vamsi on 8/6/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet var countryflagImage: UIImageView!
    
    @IBOutlet var languageNameLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
