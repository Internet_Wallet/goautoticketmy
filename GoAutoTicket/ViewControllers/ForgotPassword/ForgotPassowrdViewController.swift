//
//  ForgotPassowrdViewController.swift
//  GoAutoTicket
//
//  Created by vamsi on 7/4/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit
import TextFieldEffects
@available(iOS 13.0, *)

class ForgotPassowrdViewController: UIViewController,CountryLeftViewDelegate, PhValidationProtocol,HelperFunctions {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var forgotSegmentController: UISegmentedControl!
    
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var ForgotTF: TxtAppTheme!
    
//    @IBOutlet var ForgotTF: NoPasteTextField!
    @IBOutlet var cancelButton: UIButton!
    
    @IBOutlet var submitButton: UIButton!
    
    var countryView   : PaytoViews?
    var country       : Country?
    let validObj = PayToValidations.init()
       
       public var currentSelectedCountry : countrySelected = .myanmarCountry
       enum countrySelected {
           case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
       }
    
    var segmentStr = ""
    var parameters:[String : Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.isHidden = true
       segmentStr = "First"
        // Do any additional setup after loading the view.
        if segmentStr == "First"{
            self.ForgotTF.leftViewMode = .never
            ForgotTF.keyboardType = .emailAddress
            ForgotTF.text = ""
            ForgotTF.becomeFirstResponder()
        }
        else{
            ForgotTF.keyboardType = .phonePad
            ForgotTF.text = ""
        }
        
    }
    
    func setUI(){
     //   dismissKey()
      //  headingLabel.setPropertiesForLabel()
      //  ForgotTF.setPropertiesForTextField()
        phNumValidationsFile = getDataFromJSONFile() ?? []
        self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
        countryView = PaytoViews.updateView()
        let countryCode = String(format: "(%@)", "+95")
        countryView?.wrapCountryViewData(img: "myanmarflag", str: countryCode)
        countryView?.delegate = self
        self.ForgotTF.leftViewMode = .always
        self.ForgotTF.leftView     = countryView
        
    }


    @IBAction func backButtonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitButtonClick(_ sender: Any) {
         let validate = Validation()
        if segmentStr == "First"{
            if ForgotTF.text == ""{
                AppUtility.alert(message: "Please Enter Email".localized, title: "", controller: self)
            }
            else if !validate.isEmailString(ForgotTF.text){
                AppUtility.alert(message: "Enter Valid Email".localized, title: "", controller: self)
            }
            else{
                // Api call
                parameters = ["Request": [
                 
                  "ContactInformation": ["Email": ForgotTF.text ?? ""]
                  
                ],
                
                ]
                
                print("$$$$$",parameters)
                
                //self.apirequestforgotpasswordcall()
            }
        }
        else{
            if ForgotTF.text == "09" || ForgotTF.text == "" {
                AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
            }
            else{
                // Api call
                parameters = ["Request": [
                  
                  "contactInformation": [
                    "phoneNumber": ForgotTF.text ?? "",
                    "phoneNumberCountryCode": "95"
                    ]
                ]
                
                ]
                print("$$$$$",parameters)
                //self.apirequestforgotpasswordcall()
                
            }
            
        }
        
    }
    @IBAction func forgotSegmentClick(_ sender: Any) {
        
        if forgotSegmentController.selectedSegmentIndex == 0{
            ForgotTF.resignFirstResponder()
            segmentStr = "First"
            ForgotTF.placeholder = "Email"
            self.ForgotTF.leftViewMode = .never
            ForgotTF.keyboardType = .emailAddress
            ForgotTF.text = ""
             ForgotTF.becomeFirstResponder()
            descriptionLabel.text = "Please enter your e-mail address & we will send you a confirmation mail to reset your password."
        }
        else{
            ForgotTF.resignFirstResponder()
            segmentStr = "Second"
            ForgotTF.placeholder = "Enter Mobile Number"
            ForgotTF.keyboardType = .phonePad
            ForgotTF.text = ""
             ForgotTF.becomeFirstResponder()
            descriptionLabel.text = "Please enter your mobile number & we will send you a confirmation SMS to reset your password."
            setUI()
        }
    }
    
}
