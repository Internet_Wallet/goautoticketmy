//
//  RegistrationVC_Extenstion.swift
//  GoAutoTicket
//
//  Created by iMac on 12/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension RegistrationViewController: CountryViewControllerDelegate{

    @available(iOS 13.0, *)
    func countryViewController(_ list: CountryViewController, country: Country) {
    self.country = country
        
        if countryselect == "Country"{
            let countryname = String.init(format: "", country.name)
            issuecountryView?.wrapCountryViewData(img: "", str: countryname)
            self.countryTF.text = self.country?.name
            self.countryTF.resignFirstResponder()
            countryselect = ""
        }
        else{
            self.mobilenumTF.leftView = nil
            
            let countryCode = String.init(format: "(%@)", country.dialCode)
            countryView?.wrapCountryViewData(img: country.code, str: countryCode)
            //self.mobileNumTF.leftView = countryView
            self.mobilenumTF.leftView = countryView
            self.mobilenumTF.text = (self.country?.dialCode == "+95") ? "09" : ""
            self.mobilenumTF.becomeFirstResponder()
        }
        
        
    
        
//    self.countryTF.leftView = nil
//    let countryname = String.init(format: "(%@)", country.name)
//    countryView?.wrapCountryViewData(img: country.code, str: countryname)
//           //self.mobileNumTF.leftView = countryView
//
//    self.countryTF.leftView = countryView
        
        
    list.dismiss(animated: true, completion: nil)
    
    switch self.country?.dialCode {
    case "+91":
        currentSelectedCountry = .indiaCountry
    case "+95":
        currentSelectedCountry = .myanmarCountry
    case "+66":
        currentSelectedCountry = .thaiCountry
    case "+86":
        currentSelectedCountry = .chinaCountry
    default:
        currentSelectedCountry  = .other
    }
    
//    self.countryTF.text = (self.country?.name == "myanmar") ? "myanmar" : ""
//    self.countryTF.becomeFirstResponder()
}

    @available(iOS 13.0, *)
    func countryViewControllerCloseAction(_ list: CountryViewController) {
    list.dismiss(animated: true, completion: nil)
}

    @available(iOS 13.0, *)
 func publicCountryView() {
    guard let countryVC = countryViewController(delegate: self) else { return }
    countryVC.modalPresentationStyle = .fullScreen
    self.present(countryVC, animated: true, completion: nil)
}
    
}
@available(iOS 13.0, *)
extension RegistrationViewController: ApiRequestProtocol {
   
    func apirequestregistercall(){
        AppUtility.showLoading(self.view)
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        DispatchQueue.main.async {
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/user/signup")!, requestData: self.registerparameters, requestType: RequestType.RequestTypeRegistration, httpMethodName: "POST")
        }
    }
    func apirequestotpcall(){
        AppUtility.showLoading(self.view)
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        DispatchQueue.main.async {
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/user/otp/generate")!, requestData: self.otpparameters, requestType: RequestType.RequestTypeotp, httpMethodName: "POST")
        }
    }
    
    func httpResponse(responseObj: Any, reqType: RequestType)
    {
        DispatchQueue.main.async{
            
            AppUtility.hideLoading(self.view)
            
            if let respMsg = responseObj as? String
            {
                AppUtility.alert(message: respMsg, title: "", controller: self)
            }
            else
            {
                
                var respDict = [String:Any]()
                if responseObj is [Any]
                {
                   // self.tokenarray = responseObj as! [Any]
                }
                else if responseObj is [String:Any]
                {
                    respDict = responseObj as! Dictionary<String,Any>
                    
                }
                
                if reqType == RequestType.RequestTypeEnvironment
                {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                               
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }

                    else
                    {
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                }
                else if reqType == RequestType.RequestTypeRegistration {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                               let responseDict:[String:Any] = respDict["response"] as!  Dictionary<String,Any>
                                
                                print("Signup Success", responseDict)
                                
                            AppUtility.alert(message: message ?? "", title: "", controller: self)
                        }
                        else
                        {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                        }
                       }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                      else
                      {
                     AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                }
                else if reqType == RequestType.RequestTypeotp {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                                
                                 self.showOtpScreen()
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                    else
                    {
                        AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                }
                else{
                    AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                }
            }
        }
    }
    
}
