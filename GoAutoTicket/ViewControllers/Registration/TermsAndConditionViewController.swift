//
//  TermsAndConditionViewController.swift
//  GoAutoTicket
//
//  Created by Govind Rakholiya on 22/05/21.
//  Copyright © 2021 vamsi. All rights reserved.
//

import UIKit


struct typeOfHTMLContent {
    static let privacyPolicyContent : String = "PrivacyPolicyContent"
    static let termsConditionContent : String = "PrivacyPolicyContent"
}

class TermsAndConditionViewController: UIViewController {

    @IBOutlet weak var txtContent: UITextView!
   
    @IBOutlet weak var displayView: UIView!
    var currentPageType : String = typeOfHTMLContent.privacyPolicyContent
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let termsHTMLData : String = UserDefaults.standard.value(forKey: userdefaultKeys.signupTerms) as? String{
            
            
//            let str = termsHTMLData.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil) ?? ""
//
//

            
//            let stringWithAttribute = NSAttributedString(string: str ,
//                                                                     attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0)])
            let stringWithAttribute = try? NSAttributedString(HTMLString: termsHTMLData, font: UIFont.systemFont(ofSize: 16, weight: .medium))

        
            txtContent.attributedText = stringWithAttribute
        }
        
        // Do any additional setup after loading the view.
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}




extension NSAttributedString {
    
    public convenience init?(HTMLString html: String, font: UIFont? = nil) throws {
        
        let options : [NSAttributedString.DocumentReadingOptionKey : Any] =
            [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
             NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue]

        guard let data = html.data(using: .utf8, allowLossyConversion: true) else {
            throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
        }

        if let font = font {
            guard let attr = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) else {
                throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
            }
            var attrs = attr.attributes(at: 0, effectiveRange: nil)
            attrs[NSAttributedString.Key.font] = font
            attrs[NSAttributedString.Key.foregroundColor] = UIColor.gray
            attr.setAttributes(attrs, range: NSRange(location: 0, length: attr.length))
            self.init(attributedString: attr)
        } else {
            try? self.init(data: data, options: options, documentAttributes: nil)
        }
        
    }

//    convenience init(htmlString html: String, font: UIFont? = nil, useDocumentFontSize: Bool = true) throws {
//        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
//            .documentType: NSAttributedString.DocumentType.html,
//            .characterEncoding: String.Encoding.utf8.rawValue
//        ]
//
//        let data = html.data(using: .utf8, allowLossyConversion: true)
//        guard (data != nil), let fontFamily = font?.familyName, let attr = try? NSMutableAttributedString(data: data!, options: options, documentAttributes: nil) else {
//            try self.init(data: data ?? Data(html.utf8), options: options, documentAttributes: nil)
//            return
//        }
//
//        let fontSize: CGFloat? = useDocumentFontSize ? nil : font!.pointSize
//        let range = NSRange(location: 0, length: attr.length)
//        attr.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
//            if let htmlFont = attrib as? UIFont {
//                let traits = htmlFont.fontDescriptor.symbolicTraits
//                var descrip = htmlFont.fontDescriptor.withFamily(fontFamily)
//
//                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitBold.rawValue) != 0 {
//                    descrip = descrip.withSymbolicTraits(.traitBold)!
//                }
//
//                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitItalic.rawValue) != 0 {
//                    descrip = descrip.withSymbolicTraits(.traitItalic)!
//                }
//
//                attr.addAttribute(.font, value: UIFont(descriptor: descrip, size: fontSize ?? htmlFont.pointSize), range: range)
//            }
//        }
//
//        self.init(attributedString: attr)
//    }

}
