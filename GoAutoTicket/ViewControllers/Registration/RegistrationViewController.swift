//
//  RegistrationViewController.swift
//  GoAutoTicket
//
//  Created by vamsi on 7/4/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class RegistrationViewController: UIViewController,CountryLeftViewDelegate,PhValidationProtocol,HelperFunctions  {
    @IBOutlet var registerScrollView: UIScrollView!
    
    @IBOutlet var contentView: UIView!
    // Navigation
    @IBOutlet var navigationView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    // profileImage
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var yellowLabel: UILabel!
    @IBOutlet var cameraButton: UIButton!
    
    // Name
    @IBOutlet var nameView: UIView!
    @IBOutlet var firstNameTF: NoPasteTextFieldWithFloatingLabel!
    @IBOutlet var lastNameTF: NoPasteTextFieldWithFloatingLabel!
    
    // Email
    @IBOutlet var emailView: UIView!
    @IBOutlet var emailTF: NoPasteTextFieldWithFloatingLabel!
    @IBOutlet var emailCheckButton: UIButton!
    @IBOutlet var emailIdLabelButton: UIButton!
    @IBOutlet var emailViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var emailtextfieldHeightConstraint: NSLayoutConstraint!
    
    // Mobile
    @IBOutlet var phonenumView: UIView!
    @IBOutlet var mobilenumTF: NoPasteTextField!
    
    // Password View
    @IBOutlet var passwordView: UIView!
    @IBOutlet var passwordTF: NoPasteTextFieldWithFloatingLabel!
    @IBOutlet var confirmPasswordTF: NoPasteTextFieldWithFloatingLabel!
    
    // CountryView
    @IBOutlet var countryselectView: UIView!
    @IBOutlet var countryTF: NoPasteTextFieldWithFloatingLabel!
    
    // Button View
    @IBOutlet var buttonView: UIView!
    @IBOutlet var termsCheckBoxButton: UIButton!
    @IBOutlet var termsandConditonButton: UIButton!
    @IBOutlet var privacyButton: UIButton!
    @IBOutlet var termsLabel: UILabel!
    @IBOutlet var andLabel: UILabel!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet weak var btnPasswordShowHide: UIButton!{
        didSet{
//            passwordTF.isSecureTextEntry = btnPasswordShowHide.isSelected ? false : true
        }
    }
    
    
    @IBOutlet weak var btnConfirmPasswordShowHide: UIButton!{
        didSet{
           // confirmPasswordTF.isSecureTextEntry = btnPasswordShowHide.isSelected ? false : true
        }
    }
    
    
    
    var registerparameters:[String : Any] = [:]
    var otpparameters:[String : Any] = [:]
    
    var imagebase64Str = ""
    var otpmessage = ""
    var smsotptxt = ""
    var randomotp = ""
    
    var unchecked = true
    
    var countryselect = ""
    
      var countryView   : PaytoViews?
      var country       : Country?
      let validObj = PayToValidations.init()
    
      var issuecountryView   : PaytoViews?
      var issuecountry       : Country?
      
      public var currentSelectedCountry : countrySelected = .myanmarCountry
      enum countrySelected {
          case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
      }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        signupButton.layer.cornerRadius = 4;
        signupButton.layer.masksToBounds = true;
        self.setUI()
        self.setcountrypick()
        self.countryTF.text = "Myanmar"
      //  self.countryTF.isUserInteractionEnabled = false
    }
    func setUI(){
        //   dismissKey()
         //  headingLabel.setPropertiesForLabel()
          // SigninTF.setPropertiesForTextField()
           phNumValidationsFile = getDataFromJSONFile() ?? []
           self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
           countryView = PaytoViews.updateView()
           let countryCode = String(format: "(%@)", "+95")
           countryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
           countryView?.delegate = self
           self.mobilenumTF.leftViewMode = .always
           self.mobilenumTF.leftView     = countryView
           self.mobilenumTF.placeholder = "                          Enter Phone Number"
           
       }
    
    func setcountrypick(){
           self.issuecountry = Country.init(name: "Myanmar", code: "", dialCode: "")
           issuecountryView = PaytoViews.countryupdateView()
           let countryCode = String(format: "", "myanmar")
           issuecountryView?.wrapCountryViewData(img: "", str: countryCode)
           issuecountryView?.delegate = self
           countryTF.inputView = issuecountryView
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func camerButtonClick(_ sender: Any) {
        
        self.showAlert()
        
    }
    @IBAction func onClickPasswordShowHide(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordTF.isSecureTextEntry = sender.isSelected ? false : true
    }
    
    @IBAction func onClickConfirmPasswordShowHide(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordTF.isSecureTextEntry = sender.isSelected ? false : true
    }
    
    @IBAction func emailCheckButtonClick(_ sender: Any) {
        if unchecked {
            (sender as AnyObject).setImage(UIImage(named:"check"), for: .normal)
            
            view.layoutIfNeeded() // force any pending operations to finish

            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.emailtextfieldHeightConstraint.constant = 0
                self.emailTF.isHidden = true
                self.emailViewHeightConstraint.constant = 120
                self.view.layoutIfNeeded()
            })
            
            
            unchecked = false
        }
        else {
            (sender as AnyObject).setImage( UIImage(named:"emptycheck"), for: .normal)
            view.layoutIfNeeded() // force any pending operations to finish

            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.emailtextfieldHeightConstraint.constant = 45
                self.emailTF.isHidden = false
                self.emailViewHeightConstraint.constant = 160
                self.view.layoutIfNeeded()
            })
            unchecked = true
        }
    }
    @IBAction func emailidlabelClick(_ sender: Any) {
        if unchecked {
             self.emailCheckButton.setImage(UIImage(named:"check"), for: .normal)
            view.layoutIfNeeded() // force any pending operations to finish
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.emailtextfieldHeightConstraint.constant = 0
                self.emailViewHeightConstraint.constant = 120
                self.view.layoutIfNeeded()
            })
            unchecked = false
        }
        else {
             self.emailCheckButton.setImage( UIImage(named:"emptycheck"), for: .normal)
            view.layoutIfNeeded() // force any pending operations to finish
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.emailtextfieldHeightConstraint.constant = 45
                self.emailViewHeightConstraint.constant = 160
                self.view.layoutIfNeeded()
            })
            unchecked = true
        }
    }
    
    @IBAction func termsCheckboxClick(_ sender: Any) {
        if unchecked {
            (sender as AnyObject).setImage(UIImage(named:"check"), for: .normal)
            
            unchecked = false
        }
        else {
            (sender as AnyObject).setImage( UIImage(named:"emptycheck"), for: .normal)
            
            unchecked = true
        }
        
    }
    @IBAction func termsconditionClick(_ sender: Any) {
        
            var termsVC: TermsAndConditionViewController?
            termsVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionViewController") as? TermsAndConditionViewController
            if let homeVC = termsVC {
                homeVC.modalPresentationStyle = .custom
                navigationController?.present(homeVC, animated: true, completion: nil)
            }
    }
    @IBAction func privacyClick(_ sender: Any) {
    }
    
    @IBAction func signUpClick(_ sender: Any) {
         let validate = Validation()
        if firstNameTF.text == ""{
            AppUtility.alert(message: "Please Enter Firstname".localized, title: "", controller: self)
        }
        else if lastNameTF.text == ""{
            AppUtility.alert(message: "Please Enter Lastname".localized, title: "", controller: self)
        }
        if unchecked == false
        {
           // no email
        }
        else if unchecked == true
        {
            if emailTF.text == ""{
                   AppUtility.alert(message: "Please Enter Email".localized, title: "", controller: self)
            }
            else if !validate.isEmailString(emailTF.text){
                AppUtility.alert(message: "Enter Valid Email".localized, title: "", controller: self)
            }
        }
        
        if  currentSelectedCountry == .myanmarCountry {
            if mobilenumTF.text == "09" || mobilenumTF.text == "" {
                AppUtility.alert(message: "Please Enter MM Mobile Number".localized, title: "", controller: self)
            }
        }
        else{
            if mobilenumTF.text == "" {
                AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
            }
        }
        if passwordTF.text == ""{
            AppUtility.alert(message: "Please Enter Password".localized, title: "", controller: self)
        }
        else if confirmPasswordTF.text == ""{
            AppUtility.alert(message: "Please Enter Confirm Password".localized, title: "", controller: self)
        }
        else if passwordTF.text != confirmPasswordTF.text{
            AppUtility.alert(message: "password and Confirm Password does not match".localized, title: "", controller: self)
        }
        else{
            otpparameters = ["Request": [
                "ContactInformation": [
                    "PhoneNumber":mobilenumTF.text ?? "",
                    "PhoneNumberCountryCode":"+95"
                ]
                ]]
            
            print("$$$",otpparameters)
            
            self.apirequestotpcall()
        }
        
        
        
    }
    
}
@available(iOS 13.0, *)
extension RegistrationViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    //Show alert to selected the media source type.
       private func showAlert() {

           let alert = UIAlertController(title: "Image Selection", message: "From where you want to pick this image?", preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
               self.getImage(fromSourceType: .camera)
           }))
           alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
               self.getImage(fromSourceType: .photoLibrary)
           }))
           alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }

       //get image from source type
       private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {

           //Check is source type available
           if UIImagePickerController.isSourceTypeAvailable(sourceType) {

               let imagePickerController = UIImagePickerController()
               imagePickerController.delegate = self
               imagePickerController.sourceType = sourceType
               self.present(imagePickerController, animated: true, completion: nil)
           }
       }

       //MARK:- UIImagePickerViewDelegate.
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

           self.dismiss(animated: true) { [weak self] in

               guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
               //Setting image to your image view
              
               self?.profileImageView.image = image
            let imageData:NSData = self!.profileImageView.image!.jpegData(compressionQuality: 1)! as NSData
            self?.imagebase64Str = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
           // print("RawData***",self?.imagebase64Str ?? "")

       
           }
                
       }

       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           picker.dismiss(animated: true, completion: nil)
       }
    
    func parameters(){

        registerparameters = ["Request": [
             "age": "0",
             "agentBalance": "0",
             "agentID": "0",
             "anniversaryDate": "Jan 1, 1 12:00:00 AM",
             "birthDate": "Jan 1, 1 12:00:00 AM",
             "canSendEmail": false,
             "cancelWaiveOffPercentage": "0",
             "ContactInformation": [
             "email":emailTF.text ?? "",
             "phoneNumber":mobilenumTF.text ?? "",
             "phoneNumberCountryCode":"95"
              ],
             "customerID": "0",
             "discountChangePercentage": "0",
             "entityID": "0",
             "firstName": firstNameTF.text ?? "",
             "gender": "19",
             "gmtTimeDifference": "0",
             "isCoPAX": false,
             "isEmployee": false,
             "isHeadOffice": false,
             "isNewsLetterSubscription": false,
             "isPortalAdmin": false,
             "isSelfSubscribed": false,
             "lastName": lastNameTF.text ?? "",
             "location":[
                "countryID": "Myanmar",
                "latitude": "0",
                "longitude": "0",
                "priority": "0"
             ],
//             "profilePicture":[
//             "rawData":imagebase64Str,
//             "url":AppUtility.gettransID() + ".png"
//              ],
             "loginName": emailTF.text ?? "",
             "openIDs": [:],
             "passportExpirationDate": "Jan 1, 1 12:00:00 AM",
             "password": passwordTF.text ?? "",
             "ratePercentage": "0",
             "seqNo": "0",
             "userID": "0"
          ],
           "smsOtp":otpmessage,
           "flags": [:],
           "info": [:]
       ]
        print ("Register",registerparameters)
    }

    
}

@available(iOS 13.0, *)
extension RegistrationViewController: OTPDelegate{
    
    
    func showOtpScreen() {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let oTPViewController  = sb.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
            oTPViewController.modalPresentationStyle = .overCurrentContext
            oTPViewController.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            oTPViewController.delegate = self
            self.present(oTPViewController, animated: false, completion: nil)
            self.smsotptxt = ""
        }
    }
    
    
    func selectedOTPNumber(otpNumber: String , vc:OTPViewController) {
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
         otpmessage = otpNumber
         print("otp number : \(otpmessage)")
          self.parameters()
          self.apirequestregistercall()
        
    }
}
