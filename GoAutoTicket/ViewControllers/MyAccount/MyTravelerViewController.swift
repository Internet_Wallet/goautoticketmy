//
//  MyTravelerViewController.swift
//  GoAutoTicket
//
//  Created by iMac on 20/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class MyTravelerViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var traveldiscriptionLabel: UILabel!
    
    @IBOutlet var travelerlistTableView: UITableView!
    
    var flotingButton  = UIButton()
    var parameters:[String : Any] = [:]
    var cotravelersArray = [[String : Any]]()
    var DeleteCoTravelerparameters:[String : Any] = [:]
    
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         self.travelerlistTableView.tableFooterView = UIView()
        flotingButton.frame = CGRect(x: SCREEN_WIDTH - 70, y: SCREEN_HEIGHT - 350, width: 60, height: 60)
        flotingButton.setImage(UIImage(named:"Plus"), for: .normal)
       // flotingButton.backgroundColor = UIColor.systemBlue
        flotingButton.clipsToBounds = true
        flotingButton.layer.cornerRadius = 30.0
      //  flotingButton.layer.borderColor = (UIColor.white as! CGColor)
      //  flotingButton.layer.borderWidth = 2.0
        flotingButton.addTarget(self, action: #selector(addcotraveler(_:)), for: .touchUpInside)
        self.travelerlistTableView.addSubview(flotingButton)
    
    }
    override func viewWillAppear(_ animated: Bool) {
        parameters = [:]
        self.apirequestcotravelerscall()

    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        let off = self.travelerlistTableView.contentOffset.y
        flotingButton.frame = CGRect(x: SCREEN_WIDTH - 70, y: off+SCREEN_HEIGHT - 350, width: flotingButton.frame.size.width, height: flotingButton.frame.size.height)
    }
    
    @objc func addcotraveler(_ button: UIButton) {
        var homeVC: AddCoTravelerViewController?
        
        homeVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "AddCoTravelerViewController") as? AddCoTravelerViewController
        
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
    }

}
@available(iOS 13.0, *)
extension MyTravelerViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cotravelersArray.count > 0{
            return cotravelersArray.count
        }
         return 0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //  let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyTravelerTableViewCell
        
        var cell:MyTravelerTableViewCell? = self.travelerlistTableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTravelerTableViewCell
        
        //  cell?.separatorInset = UIEdgeInsets.zero
        
        if cell == nil {
            cell = MyTravelerTableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        let sampleDict = self.cotravelersArray[indexPath.row] as? Dictionary<String,Any>
        cell?.usernameLabel.text = sampleDict?["userDisplayName"] as? String
        cell?.genderLabel.text = sampleDict?["genderDesc"] as? String
        let myString: String = sampleDict?["birthDate"] as? String ?? ""
        let myStringArr = myString.components(separatedBy: "T")//componentsSeparatedByString(" ")
        let date: String = myStringArr [0]
      //  let time: String = myStringArr [1]
        cell?.DOBLabel.text = date
        
        cell?.deletebutton.tag = indexPath.row
        cell?.editbutton.tag = indexPath.row
        
       cell?.deletebutton.addTarget(self, action: #selector(self.deleteButtonClicked(_:)), for: .touchUpInside)
       cell?.editbutton.addTarget(self, action: #selector(self.editButtonClicked(_:)), for: .touchUpInside)
        
        cell?.selectionStyle = .none
        return cell!
    }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
//        var homeVC: FlightDetailViewController?
//
//        homeVC = UIStoryboard(name: "FlightMain", bundle: nil).instantiateViewController(withIdentifier: "FlightDetailViewController") as? FlightDetailViewController
//
//        if let homeVC = homeVC {
//            navigationController?.pushViewController(homeVC, animated: true)
//        }
     }
    
        @objc func deleteButtonClicked(_ deleteButtonClicked: UIButton?) {
            
            let i: Int = deleteButtonClicked!.tag
            //let cell = travelerlistTableView.cellForRow(at: IndexPath(row: i, section: 0)) as? MyTravelerTableViewCell
            let sampleDict = self.cotravelersArray[i] as? Dictionary<String,Any>
            let locationdictTemp = sampleDict?["location"] as? Dictionary<String,Any>
            let contactdictTemp = sampleDict?["contactInformation"] as? Dictionary<String,Any>

            DeleteCoTravelerparameters = ["Request": [
                "CustomerID": sampleDict?["customerID"],
                "UserDisplayName": sampleDict?["userDisplayName"],
                "MemberNumber": sampleDict?["memberNumber"],
                "FirstName": sampleDict?["firstName"],
                "LastName": sampleDict?["lastName"],
                "Location": [
                    "CountryID": locationdictTemp?["countryID"],
                    "Country": locationdictTemp?["country"],
                    "Address": locationdictTemp?["address"],
                    "City": locationdictTemp?["city"],
                    "ZipCode": locationdictTemp?["zipCode"]
                       ],
                "ContactInformation": [
                    "PhoneNumber": contactdictTemp?["phoneNumber"],
                    "ActlFormatPhoneNumber": "95-\(contactdictTemp?["phoneNumber"] ?? "")",
                    "PhoneNumberCountryCode": "+95"
                ],
                "IssuingCountryCode": sampleDict?["issuingCountryCode"],
                "NationalityCode": sampleDict?["nationalityCode"],
                "GenderDesc": sampleDict?["genderDesc"],
                "Gender": sampleDict?["gender"],
                "ActlGender": sampleDict?["actlGender"],
                "BirthDate": sampleDict?["birthDate"],
                "PassportExpirationDate": sampleDict?["passportExpirationDate"],
                "DocumentType": sampleDict?["documentType"],
                "DocumentNumber": sampleDict?["documentNumber"],
                "SeqNo": sampleDict?["seqNo"]
                ],
               "Flags": [:]
            ]
            
            self.apirequestdeletecotravelercall()
        }
    @objc func editButtonClicked(_ editButtonClicked: UIButton?) {
        let i: Int = editButtonClicked!.tag
                   //let cell = travelerlistTableView.cellForRow(at: IndexPath(row: i, section: 0)) as? MyTravelerTableViewCell
        let sampleDict = self.cotravelersArray[i] as? Dictionary<String,Any>
        
        var homeVC: AddCoTravelerViewController?
            
            homeVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "AddCoTravelerViewController") as? AddCoTravelerViewController
             homeVC?.edittravelerdict = sampleDict
             homeVC?.FromScreen = "EDIT"
            if let homeVC = homeVC {
                navigationController?.pushViewController(homeVC, animated: true)
            }
    
    }
}
@available(iOS 13.0, *)
extension MyTravelerViewController: ApiRequestProtocol {
  func apirequestcotravelerscall(){
     AppUtility.showLoading(self.view)
     let apiReqObj = ApiRequestClass()
     apiReqObj.customDelegate = self
     DispatchQueue.main.async {
         
        apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/cotraveler/details")!, requestData:self.parameters, requestType: RequestType.Requestcotravelerdetials, httpMethodName: "POST")
     }
 }
    
     func apirequestdeletecotravelercall(){
        AppUtility.showLoading(self.view)
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        DispatchQueue.main.async {
            
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/cotraveler/delete")!, requestData:self.DeleteCoTravelerparameters, requestType: RequestType.RequestDeleteCoTraveler, httpMethodName: "POST")
        }
    }
    
    func httpResponse(responseObj: Any, reqType: RequestType)
    {
        DispatchQueue.main.async{
            
            AppUtility.hideLoading(self.view)
            
            if let respMsg = responseObj as? String
            {
                AppUtility.alert(message: respMsg, title: "", controller: self)
            }
            else
            {
                
                var respDict = [String:Any]()
                if responseObj is [Any]
                {
                   // self.tokenarray = responseObj as! [Any]
                }
                else if responseObj is [String:Any]
                {
                    respDict = responseObj as! Dictionary<String,Any>
                    
                }
                
                if reqType == RequestType.RequestTypeEnvironment
                {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                               
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }

                    else
                    {
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                }
                else if reqType == RequestType.Requestcotravelerdetials {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                                self.cotravelersArray = (respDict["response"] as? [[String : Any]])!
                                print("self.cotravelersArray", self.cotravelersArray)
                                self.travelerlistTableView.reloadData()

                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                    else
                    {
                        AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                }
                else if reqType == RequestType.RequestDeleteCoTraveler {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                               // self.cotravelersArray = (respDict["response"] as? [[String : Any]])!
                            //    print("self.cotravelersArray", self.cotravelersArray)
                                 print("Reload")
                                
                                  self.apirequestcotravelerscall()
                              //  self.travelerlistTableView.reloadData()
                                
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                    else
                    {
                        AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                }
                else{
                    AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                }
            }
        }
    }
    
}
