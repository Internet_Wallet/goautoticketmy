//
//  ProfileViewController.swift
//  GoAutoTicket
//
//  Created by iMac on 20/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ProfileViewController: UIViewController, ApiRequestProtocol {

    @IBOutlet var genderLabel: UILabel!
    
    @IBOutlet var namelabel: UILabel!
    
    @IBOutlet var surnamelabel: UILabel!
    
    @IBOutlet var birthLabel: UILabel!
    
    @IBOutlet var anniversaryLabel: UILabel!
    
    @IBOutlet var emailLabel: UILabel!
    
    @IBOutlet var cityLabel: UILabel!
    
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var zipLabel: UILabel!
    
    @IBOutlet var nationalityLabel: UILabel!
    
    @IBOutlet var phonelabel: UILabel!
    
     var parameters:[String : Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        parameters = [:]
        self.apirequestuserdetailscall()

    }
    
    @IBAction func editprofileClick(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController
        if let isValidObj = nextViewController{
            self.navigationController?.pushViewController(isValidObj, animated: true)
            
    }
  }
    func apirequestuserdetailscall(){
        AppUtility.showLoading(self.view)
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        DispatchQueue.main.async {
            
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/user/details")!, requestData:self.parameters, requestType: RequestType.RequestUserDetails, httpMethodName: "POST")
        }
    }
    
    func httpResponse(responseObj: Any, reqType: RequestType)
    {
        DispatchQueue.main.async{
            
            AppUtility.hideLoading(self.view)
            
            if let respMsg = responseObj as? String
            {
                AppUtility.alert(message: respMsg, title: "", controller: self)
            }
            else
            {
                
                var respDict = [String:Any]()
                if responseObj is [Any]
                {
                   // self.tokenarray = responseObj as! [Any]
                }
                else if responseObj is [String:Any]
                {
                    respDict = responseObj as! Dictionary<String,Any>
                    
                }
                
                if reqType == RequestType.RequestTypeEnvironment
                {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                                
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                    else
                    {
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                }
                else if reqType == RequestType.RequestUserDetails {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                                let responseDict:[String:Any] = respDict["response"] as!  Dictionary<String,Any>
                                
                                let contactDict:[String:Any] = responseDict["contactInformation"] as!  Dictionary<String,Any>
                                
                                let locationDict:[String:Any] = responseDict["location"] as!  Dictionary<String,Any>
                                
                                print("user Success", responseDict)
                                
                                self.genderLabel.text = responseDict["genderDesc"] .safelyWrappingString()
                                self.namelabel.text = responseDict["firstName"] .safelyWrappingString()
                                self.surnamelabel.text = responseDict["lastName"] .safelyWrappingString()
                                self.birthLabel.text = responseDict["birthDate"] .safelyWrappingString()
                                self.anniversaryLabel.text = responseDict["anniversaryDate"] .safelyWrappingString()
                                self.emailLabel.text = contactDict["email"] .safelyWrappingString()
                                self.cityLabel.text = locationDict["city"] .safelyWrappingString()
                                self.addressLabel.text = locationDict["address"] .safelyWrappingString()
                                self.zipLabel.text = locationDict["zipCode"] .safelyWrappingString()
                                self.nationalityLabel.text = responseDict["nationalityCode"] .safelyWrappingString()
                                self.phonelabel.text = contactDict["phoneNumber"] .safelyWrappingString()
                                
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                      else
                      {
                     AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                }
               else
                  {
                        AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                }
            }
        }
    }


}
