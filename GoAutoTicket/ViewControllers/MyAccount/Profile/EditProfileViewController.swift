//
//  EditProfileViewController.swift
//  GoAutoTicket
//
//  Created by iMac on 21/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

@available(iOS 13.0, *)
class EditProfileViewController: UIViewController,CountryLeftViewDelegate,PhValidationProtocol,HelperFunctions  {
    
    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var profileImageView: UIImageView!
    
    @IBOutlet var camerButton: UIButton!
    
    @IBOutlet var emailTF: NoPasteTextField!
    
    @IBOutlet var firstNameTF: NoPasteTextField!
    
    @IBOutlet var lastNameTF: NoPasteTextField!
    
    @IBOutlet var genderLabel: UILabel!
    @IBOutlet var maleradioButton: UIButton!
    
    @IBOutlet var maleButton: UIButton!
    
    @IBOutlet var femaleRadioButton: UIButton!
    
    @IBOutlet var femaleButton: UIButton!
    
    
    @IBOutlet var birthdayTF: NoPasteTextField!
    @IBOutlet var anniverasryTF: NoPasteTextField!

    @IBOutlet var phonenumTF: NoPasteTextField!
    @IBOutlet var alternativephonenumTF: NoPasteTextField!
    
    @IBOutlet var selectCountryTF: NoPasteTextField!
    @IBOutlet var cityTF: NoPasteTextField!
    @IBOutlet var addressTF: NoPasteTextField!
    @IBOutlet var zipCodeTF: NoPasteTextField!
    
    @IBOutlet var passportTF: NoPasteTextField!
    
    @IBOutlet var passwordCheckButton: UIButton!
    
    @IBOutlet var updatePasswordButton: UIButton!
    
    @IBOutlet var passportView: UIView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var submitButton: UIButton!
    
    @IBOutlet var documentinformationLabel: MarqueeLabel!
    @IBOutlet var documentTypeTF: ACFloatingTextfield!
    
    @IBOutlet var issuingCountryTF: ACFloatingTextfield!
    
    @IBOutlet var documentnumberTF: ACFloatingTextfield!
    
    @IBOutlet var expirydateTF: ACFloatingTextfield!
    
    @IBOutlet var passwordView: UIView!
    
    @IBOutlet var updatepasswordLabel: UILabel!
    
    @IBOutlet var passwordTF: ACFloatingTextfield!
    @IBOutlet var confirmPasswordTF: ACFloatingTextfield!
    
    @IBOutlet var passwordvisableButton: UIButton!
    @IBOutlet var confirmPasswordVisableButton: UIButton!
    
    
    @IBOutlet var passwordCancelButton: UIButton!
    @IBOutlet var okButton: UIButton!
    
    // if NRC Select
    
    @IBOutlet var NRCView: UIView!
    @IBOutlet var nrcStateTF: ACFloatingTextfield!
    @IBOutlet var nrcTownshipTF: ACFloatingTextfield!
    @IBOutlet var nrcNationalityTF: ACFloatingTextfield!
    @IBOutlet var nrcCodeTF: ACFloatingTextfield!
    
    
    
    var picker = UIDatePicker()
    var strTF = String()
    
    var  pickerTextView = UITextView()
    
    var typePicker = UIPickerView()
       
    var nrcstatepicker = UIPickerView()
    
    var townshippicker = UIPickerView()
    
    var nationalityidpicker = UIPickerView()
    
    var typearr = [String]()
    
    var nrcstatearr = [String]()
    
    var townshiparr = [String]()
    
    var natioanlityid = [String]()
    
    var countryselect = ""
    
    var imagebase64Str = ""
    
    var unchecked = true
    var radioCheck = true
    
    var selectedTFStr = ""
    
    var countryView   : PaytoViews?
    var country       : Country?
    var AltcountryView   : PaytoViews?
    var Altcountry       : Country?
    
    var issuecountryView   : PaytoViews?
    var issuecountry       : Country?
    
    let validObj = PayToValidations.init()
    
    public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    var updateparameters:[String : Any] = [:]
    var parameters:[String : Any] = [:]
    var entityid = ""
    var userid = ""
    var agentid = ""
    var customerid = ""
    var membernumber = ""
    var countryid = ""
    var gender = ""
    var actlgender = "19"
    var genderdesc = ""
    var nationalitycode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passportView.frame = self.view.frame
        self.view.addSubview(self.passportView)
        NRCView.isHidden = true
        NRCView.alpha = 0.0
        passportView.isHidden = true
        
        self.passwordView.frame = self.view.frame
        self.view.addSubview(self.passwordView)
        passwordView.isHidden = true
        
         parameters = [:]
        self.apirequestuserdetailscall()
        self.setphoneUI()
        self.setaltphoneUI()
        self.setcountrypick()
        self.selectcountrypick()
        typePicker.delegate = self
        typePicker.dataSource = self
         
        nrcstatepicker.delegate = self
        nrcstatepicker.dataSource = self
        
        townshippicker.delegate = self
        townshippicker.dataSource = self
        
        nationalityidpicker.delegate = self
        nationalityidpicker.dataSource = self
        
        typearr = ["Passport","NRC"]
        natioanlityid = ["N","P","E"]
        nrcstatearr = ["14/(Ayeyarwady Divsion)","7/(Bago Divison)","12/(Yangon Divison)","4/(Chin State)"]
        townshiparr = ["14/(BaKaLa)Bogale","7/(DaAuNa)Daik-U","12/(DaGaNa)Dagon","4/(MaTaNa)Mindat"]
        
        documentTypeTF.rightViewMode = .always
        documentTypeTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
        expirydateTF.rightViewMode = .always
        expirydateTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
        nrcStateTF.rightViewMode = .always
        nrcStateTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
               
        nrcNationalityTF.rightViewMode = .always
        nrcNationalityTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
        nrcTownshipTF.rightViewMode = .always
        nrcTownshipTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
          self.passwordTF.isSecureTextEntry = true
          self.confirmPasswordTF.isSecureTextEntry = true
          emailTF.isUserInteractionEnabled = false
        
        if gender == "M"{
            femaleRadioButton.setImage(UIImage(named:"circleempty"), for: .normal)
            maleradioButton.setImage( UIImage(named:"circlefill"), for: .normal)
        }
        else{
            femaleRadioButton.setImage(UIImage(named:"circlefill"), for: .normal)
            maleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
        }
        
    }
    
    func setphoneUI(){
          //   dismissKey()
           //  headingLabel.setPropertiesForLabel()
            // SigninTF.setPropertiesForTextField()
             phNumValidationsFile = getDataFromJSONFile() ?? []
             self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
             countryView = PaytoViews.updateView()
             let countryCode = String(format: "(%@)", "+95")
             countryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
             countryView?.delegate = self
             self.phonenumTF.leftViewMode = .always
             self.phonenumTF.leftView     = countryView
             
         }
    func setaltphoneUI(){
           //   dismissKey()
            //  headingLabel.setPropertiesForLabel()
             // SigninTF.setPropertiesForTextField()
              phNumValidationsFile = getDataFromJSONFile() ?? []
              self.Altcountry = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
              AltcountryView = PaytoViews.updateView()
              let countryCode = String(format: "(%@)", "+95")
              AltcountryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
              AltcountryView?.delegate = self
              self.alternativephonenumTF.leftViewMode = .always
              self.alternativephonenumTF.leftView     = AltcountryView
              
          }
    func setcountrypick(){
         self.issuecountry = Country.init(name: "Myanmar", code: "", dialCode: "")
         issuecountryView = PaytoViews.countryupdateView()
         let countryCode = String(format: "", "myanmar")
         issuecountryView?.wrapCountryViewData(img: "", str: countryCode)
         issuecountryView?.delegate = self
         issuingCountryTF.inputView = issuecountryView
     }
    
    func selectcountrypick(){
         self.issuecountry = Country.init(name: "Myanmar", code: "", dialCode: "")
         issuecountryView = PaytoViews.countryupdateView()
         let countryCode = String(format: "", "myanmar")
         issuecountryView?.wrapCountryViewData(img: "", str: countryCode)
         issuecountryView?.delegate = self
         selectCountryTF.inputView = issuecountryView
     }
//    func countryName(from countryCode: String) -> String {
//        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
//            // Country name was found
//            print ("Country Code", name)
//            return name
//        } else {
//            // Country name cannot be found
//            return countryCode
//        }
//    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func camerButtonClick(_ sender: Any) {
        self.showAlert()
        
    }
    
    @IBAction func maleradioClick(_ sender: Any) {
        
     //   if radioCheck {
            (sender as AnyObject).setImage(UIImage(named:"circlefill"), for: .normal)
             femaleRadioButton.setImage( UIImage(named:"circleempty"), for: .normal)
             gender = "M"
             genderdesc = "Male"
            radioCheck = false
//        }
//        else {
//            (sender as AnyObject).setImage( UIImage(named:"circleempty"), for: .normal)
//            radioCheck = true
//        }
    }
    
    @IBAction func maleButtonClick(_ sender: Any) {
     //   if radioCheck {
               maleradioButton.setImage(UIImage(named:"circlefill"), for: .normal)
                femaleRadioButton.setImage( UIImage(named:"circleempty"), for: .normal)
        
              gender = "M"
              genderdesc = "Male"
               radioCheck = false
//           }
//           else {
//               maleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
//               radioCheck = true
//           }
    }
    
    @IBAction func femaleradioClick(_ sender: Any) {
    //    if radioCheck {
             (sender as AnyObject).setImage(UIImage(named:"circlefill"), for: .normal)
              maleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
             gender = "F"
             genderdesc = "Female"
             radioCheck = false
//         }
//         else {
//             (sender as AnyObject).setImage( UIImage(named:"circleempty"), for: .normal)
//             radioCheck = true
//         }
        
    }
    
    @IBAction func femaleButtonClick(_ sender: Any) {
      //  if radioCheck {
            femaleRadioButton.setImage(UIImage(named:"circlefill"), for: .normal)
             maleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
            gender = "F"
            genderdesc = "Female"
            radioCheck = false
//        }
//        else {
//            femaleRadioButton.setImage( UIImage(named:"circleempty"), for: .normal)
//            radioCheck = true
//        }
        
    }
    
    @IBAction func passwordCheckClick(_ sender: Any) {
        
        if unchecked {
            (sender as AnyObject).setImage(UIImage(named:"check"), for: .normal)
            self.showpasswordView()
            unchecked = false
        }
        else {
            (sender as AnyObject).setImage( UIImage(named:"emptycheck"), for: .normal)
            self.passwordView.isHidden = true
            unchecked = true
        }
     
    }
    
    @IBAction func updatePasswordClick(_ sender: Any) {
            if unchecked {
                  passwordCheckButton.setImage(UIImage(named:"check"), for: .normal)
                  self.showpasswordView()
                  unchecked = false
              }
              else {
                  passwordCheckButton.setImage( UIImage(named:"emptycheck"), for: .normal)
                  self.passwordView.isHidden = true
                  unchecked = true
              }
    }
    
    @IBAction func saveButtonClick(_ sender: Any) {
        if firstNameTF.text == ""{
            AppUtility.alert(message: "Please Enter First Name".localized, title: "", controller: self)
        }
        else if lastNameTF.text == ""{
            AppUtility.alert(message: "Please Enter Last Name".localized, title: "", controller: self)
        }
        else if birthdayTF.text == ""{
            AppUtility.alert(message: "Please Select DOB".localized, title: "", controller: self)
        }
        else if anniverasryTF.text == ""{
            AppUtility.alert(message: "Please Select Anniversary".localized, title: "", controller: self)
        }
        if  currentSelectedCountry == .myanmarCountry {
            if phonenumTF.text == "09" || phonenumTF.text == "" {
                AppUtility.alert(message: "Please Enter MM Mobile Number".localized, title: "", controller: self)
            }
            else if alternativephonenumTF.text == "09" || alternativephonenumTF.text == "" {
                AppUtility.alert(message: "Please Enter Alternative MM Mobile Number".localized, title: "", controller: self)
            }
        }
        else{
            if phonenumTF.text == "" {
                AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
            }
            else if alternativephonenumTF.text == "" {
                AppUtility.alert(message: "Please Enter Alternative Mobile Number".localized, title: "", controller: self)
            }
        }
        if selectCountryTF.text == ""{
             AppUtility.alert(message: "Please Select Country".localized, title: "", controller: self)
        }
        else if cityTF.text == ""{
             AppUtility.alert(message: "Please Enter City".localized, title: "", controller: self)
        }
        else if addressTF.text == ""{
             AppUtility.alert(message: "Please Enter Address".localized, title: "", controller: self)
        }
        else if zipCodeTF.text == ""{
             AppUtility.alert(message: "Please Enter Zipcode".localized, title: "", controller: self)
        }
        else if passportTF.text == ""{
             AppUtility.alert(message: "Please Enter Documentnumber".localized, title: "", controller: self)
        }
        else if unchecked == true
        {
             passwordTF.text = ""
             confirmPasswordTF.text = ""
        }
        else{
            self.profileparamters()
            self.apirequestupdateprofilecall()
        }
        
       
        
    }
    
    @IBAction func submitClick(_ sender: Any) {
        if documentTypeTF.text == ""{
           AppUtility.alert(message: "Please Select DocumentType".localized, title: "", controller: self)
        }
        else if issuingCountryTF.text == ""{
            AppUtility.alert(message: "Please Select Country".localized, title: "", controller: self)
        }
        else if documentnumberTF.text == ""{
             AppUtility.alert(message: "Please Enter Documentnumber".localized, title: "", controller: self)
        }
        else if expirydateTF.text == ""{
             AppUtility.alert(message: "Please Select Expirydate".localized, title: "", controller: self)
        }
        else{
            self.passportView.isHidden = true
            passportTF.text = documentnumberTF.text ?? ""
        }
        
    }
    @IBAction func cancelClick(_ sender: Any) {
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut,
                               animations: {
                        self.passportView.isHidden = true
            })
    }
    
    @IBAction func passwordvisbaleButtonClick(_ sender: Any) {
        if (self.passwordTF.isSecureTextEntry == true) {
            self.passwordvisableButton.setImage(UIImage(named: "eyeopen"), for: .normal)
            self.passwordTF.isSecureTextEntry = false
        }else{
            self.passwordvisableButton.setImage(UIImage(named: "eyeclose"), for: .normal)
            self.passwordTF.isSecureTextEntry = true
        }
    }
    
    @IBAction func confirmPasswordVisableClick(_ sender: Any) {
        if (self.confirmPasswordTF.isSecureTextEntry == true) {
            self.confirmPasswordVisableButton.setImage(UIImage(named: "eyeopen"), for: .normal)
            self.confirmPasswordTF.isSecureTextEntry = false
        }else{
            self.confirmPasswordVisableButton.setImage(UIImage(named: "eyeclose"), for: .normal)
            self.confirmPasswordTF.isSecureTextEntry = true
        }
    }
    
    @IBAction func passwordCancelClick(_ sender: Any) {
        passwordCheckButton.setImage( UIImage(named:"emptycheck"), for: .normal)
        passwordTF.text = ""
        confirmPasswordTF.text = ""
        self.passwordView.isHidden = true
        unchecked = true
    }
    @IBAction func okClick(_ sender: Any) {
        
        if passwordTF.text == ""{
            AppUtility.alert(message: "Please Enter Password".localized, title: "", controller: self)
        }
        else if confirmPasswordTF.text == ""{
            AppUtility.alert(message: "Please Enter ConfirmPassword".localized, title: "", controller: self)
        }
        else if passwordTF.text != confirmPasswordTF.text{
            AppUtility.alert(message: "password and Confirm Password does not match".localized, title: "", controller: self)
        }
        else{
             self.passwordView.isHidden = true
        }
        
    }
    func showdocumentView(){
        
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseIn,
                       animations: {
                self.passportView.isHidden = false
        })
        
        
    }
    
    func showpasswordView(){
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseIn,
                       animations: {
            self.passwordView.isHidden = false
        })
    }
    
    func  showdatepicker(){
        picker = UIDatePicker(frame: CGRect(x: 0, y: 210, width: 320, height: 250))

        picker.date = Date(timeIntervalSinceNow: TimeInterval(2))
      //  picker.minimumDate = Date(timeIntervalSinceNow: TimeInterval(0))

        picker.datePickerMode = .date
        picker.backgroundColor = UIColor.white
        picker.addTarget(self, action: #selector(updateTextField(_:)), for: .valueChanged)

         
              if selectedTFStr == "Birthday"{
                //  selectedTFStr = ""
                  
                  birthdayTF.inputView = picker
              }
              else if selectedTFStr == "Anniversery"{
                 // selectedTFStr = ""
                  
                  anniverasryTF.inputView = picker
              }
              else{
                  
                  expirydateTF.inputView = picker
              }
//         let df = DateFormatter()
//         df.dateFormat = "dd/MM/yyyy"
//         expirydateTF.text = "\(df.string(from: picker.date))"

        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.tintColor = UIColor.gray
        numberToolbar.items = [UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)), UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneWithNumberPad))]

        numberToolbar.sizeToFit()
        if selectedTFStr == "Birthday"{
                //  selectedTFStr = ""
                
                birthdayTF.inputAccessoryView = numberToolbar
            }
            else if selectedTFStr == "Anniversery"{
                // selectedTFStr = ""
                
                anniverasryTF.inputAccessoryView = numberToolbar
            }
            else{
                
              expirydateTF.inputAccessoryView = numberToolbar
            }
        
    }
    
    @objc func cancelNumberPad() {
        
          if selectedTFStr == "Birthday"{
                selectedTFStr = ""
                birthdayTF.text = ""
                birthdayTF.resignFirstResponder()
            }
            else if selectedTFStr == "Anniversery"{
                selectedTFStr = ""
                anniverasryTF.text = ""
                anniverasryTF.resignFirstResponder()
            }
            else{
                expirydateTF.text = ""
                expirydateTF.resignFirstResponder()
            }
    }

    @objc func doneWithNumberPad() {

           if selectedTFStr == "Birthday"{
               selectedTFStr = ""
               
               birthdayTF.resignFirstResponder()
           }
           else if selectedTFStr == "Anniversery"{
               selectedTFStr = ""
               
               anniverasryTF.resignFirstResponder()
           }
           else{
               
               expirydateTF.resignFirstResponder()
           }

    }

    @objc func updateTextField(_ sender: Any?) {
                 let df = DateFormatter()
            // df.dateStyle = NSDateIntervalFormatterMediumStyle;

            df.dateFormat = "yyyy-MM-dd"
        
        if selectedTFStr == "Birthday"{
            birthdayTF.text = "\(df.string(from: picker.date))"
        }
        else if selectedTFStr == "Anniversery"{
            anniverasryTF.text = "\(df.string(from: picker.date))"
        }
        else{
           expirydateTF.text = "\(df.string(from: picker.date))"
        }
    }
    
    func profileparamters (){
        
        let passportexpdata = expirydateTF.text ?? ""
        let documenttypedata = documentTypeTF.text ?? ""
        let documentnumdata = documentnumberTF.text ?? ""
        let countrydata = selectCountryTF.text ?? ""
        let addressdata = addressTF.text ?? ""
        let citydata = cityTF.text ?? ""
        let zipcodedata = zipCodeTF.text ?? ""
        let passwordData = confirmPasswordTF.text ?? ""
        let issuecountryCodeData = issuingCountryTF.text ?? ""
        updateparameters = ["Request": [
            "EntityID": entityid,
            "UserID": userid,
            "AgentID": agentid,
            "CustomerID": customerid,
            "ContactInformation": [
                "PhoneNumber": phonenumTF.text ?? "",
                "ActlFormatPhoneNumber": "95-\(alternativephonenumTF.text ?? "")",
                "PhoneNumberCountryCode": "+95",
                "HomePhoneNumberCountryCode": "+95",
                "Email": emailTF.text ?? ""
            ],
            "MemberNumber": membernumber,
            "FirstName": firstNameTF.text ?? "",
            "LastName": lastNameTF.text ?? "",
            "Password": passwordData,
            "location":[
                "CountryID": "My",
                "Country": countrydata,
                "Address": addressdata,
                "City": citydata,
                "ZipCode": zipcodedata
            ],
            "profilePicture":[
                "UpdatedDate":"",
                "url":""
            ],
            "IssuingCountryCode": issuecountryCodeData,
            "CanSendEmail": true,
            "NationalityCode": nationalitycode,
            "GenderDesc": genderdesc,
            "Gender": gender,
            "ActlGender": actlgender,
            "BirthDate": birthdayTF.text ?? "",
            "AnniversaryDate": anniverasryTF.text ?? "",
            "PassportExpirationDate": passportexpdata,
            "DocumentType": documenttypedata,
            "DocumentNumber": documentnumdata
            ],
            "flags": [:]
        ]
        
        print("Update params",updateparameters)
    }
    
}

@available(iOS 13.0, *)
extension EditProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    //Show alert to selected the media source type.
       private func showAlert() {

           let alert = UIAlertController(title: "Image Selection", message: "From where you want to pick this image?", preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
               self.getImage(fromSourceType: .camera)
           }))
           alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
               self.getImage(fromSourceType: .photoLibrary)
           }))
           alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }

       //get image from source type
       private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {

           //Check is source type available
           if UIImagePickerController.isSourceTypeAvailable(sourceType) {

               let imagePickerController = UIImagePickerController()
               imagePickerController.delegate = self
               imagePickerController.sourceType = sourceType
               self.present(imagePickerController, animated: true, completion: nil)
           }
       }

       //MARK:- UIImagePickerViewDelegate.
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

           self.dismiss(animated: true) { [weak self] in

               guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
               //Setting image to your image view
              
               self?.profileImageView.image = image
            let imageData:NSData = self!.profileImageView.image!.jpegData(compressionQuality: 1)! as NSData
            self?.imagebase64Str = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
           // print("RawData***",self?.imagebase64Str ?? "")
           }
                
       }

       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           picker.dismiss(animated: true, completion: nil)
       }
}

@available(iOS 13.0, *)
extension EditProfileViewController : UIPickerViewDelegate,UIPickerViewDataSource{
    
      func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
       
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           var rows: Int = 0
        if pickerView == nrcstatepicker {
            rows = nrcstatearr.count
        }
        if pickerView == typePicker{
            rows = typearr.count
        }
        
        if pickerView == townshippicker {
            rows = townshiparr.count
        }
        if pickerView == nationalityidpicker{
            rows = natioanlityid.count
        }
        
          return rows
       }
      
      func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var returnString = String()
        if pickerView == typePicker
        {
            returnString = typearr[row]
        }
        if pickerView == nrcstatepicker
        {
            returnString = nrcstatearr[row]
        }
        if pickerView == townshippicker
        {
            returnString = townshiparr[row]
        }
        if pickerView == nationalityidpicker{
            returnString = natioanlityid[row]
        }
        return returnString
      }
      
      func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == typePicker {
            documentTypeTF.text =  typearr[row]
            documentTypeTF.textColor = .black
        }
        if pickerView == nrcstatepicker {
            nrcStateTF.text =  nrcstatearr[row]
            nrcStateTF.textColor = .black
        }
        if pickerView == townshippicker {
            nrcTownshipTF.text =  townshiparr[row]
            nrcTownshipTF.textColor = .black
        }
        if pickerView == nationalityidpicker {
            nrcNationalityTF.text =  natioanlityid[row]
            nrcNationalityTF.textColor = .black
        }

          
      }
      
      func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    
        let pickerLabel = UILabel()
        
        pickerLabel.textAlignment = .center
        if pickerView == typePicker {
            pickerLabel.text = typearr[row]
        }
        if pickerView == nrcstatepicker {
            pickerLabel.text = nrcstatearr[row]
        }
        if pickerView == townshippicker {
            pickerLabel.text = townshiparr[row]
        }
        if pickerView == nationalityidpicker {
            pickerLabel.text = natioanlityid[row]
        }
        
          return pickerLabel
      }
}
