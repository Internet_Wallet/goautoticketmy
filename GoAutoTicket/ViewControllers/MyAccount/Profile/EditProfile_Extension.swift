//
//  EditProfile_Extension.swift
//  GoAutoTicket
//
//  Created by iMac on 21/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation
@available(iOS 13.0, *)
extension EditProfileViewController: ApiRequestProtocol {
   
    func apirequestupdateprofilecall(){
        AppUtility.showLoading(self.view)
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        DispatchQueue.main.async {
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/user/update")!, requestData: self.updateparameters, requestType: RequestType.RequestupdateProfile, httpMethodName: "POST")
        }
    }
  func apirequestuserdetailscall(){
     AppUtility.showLoading(self.view)
     let apiReqObj = ApiRequestClass()
     apiReqObj.customDelegate = self
     DispatchQueue.main.async {
         
         apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/user/details")!, requestData:self.parameters, requestType: RequestType.RequestUserDetails, httpMethodName: "POST")
     }
 }
    
    func httpResponse(responseObj: Any, reqType: RequestType)
    {
        DispatchQueue.main.async{
            
            AppUtility.hideLoading(self.view)
            
            if let respMsg = responseObj as? String
            {
                AppUtility.alert(message: respMsg, title: "", controller: self)
            }
            else
            {
                
                var respDict = [String:Any]()
                if responseObj is [Any]
                {
                   // self.tokenarray = responseObj as! [Any]
                }
                else if responseObj is [String:Any]
                {
                    respDict = responseObj as! Dictionary<String,Any>
                    
                }
                
                if reqType == RequestType.RequestTypeEnvironment
                {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                               
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }

                    else
                    {
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                }
                else if reqType == RequestType.RequestUserDetails {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                                let responseDict:[String:Any] = respDict["response"] as!  Dictionary<String,Any>
                                
                                let contactDict:[String:Any] = responseDict["contactInformation"] as!  Dictionary<String,Any>
                                
                                let locationDict:[String:Any] = responseDict["location"] as!  Dictionary<String,Any>
                                
                                print("user Success", responseDict)
                                
                                self.emailTF.text = contactDict["email"] .safelyWrappingString()
                                self.firstNameTF.text = responseDict["firstName"] .safelyWrappingString()
                                self.lastNameTF .text = responseDict["lastName"] .safelyWrappingString()
                                self.gender = responseDict["gender"] .safelyWrappingString()
                                self.genderdesc = responseDict["genderDesc"] .safelyWrappingString()
                                self.phonenumTF.text = contactDict["phoneNumber"] .safelyWrappingString()
                                self.alternativephonenumTF.text = contactDict["phoneNumber"] .safelyWrappingString()
                                self.selectCountryTF.text = locationDict["CountryID"] .safelyWrappingString()
                                self.cityTF.text = locationDict["city"] .safelyWrappingString()
                                self.addressTF.text = locationDict["address"] .safelyWrappingString()
                                self.zipCodeTF.text = locationDict["zipCode"] .safelyWrappingString()
                                self.passportTF.text = responseDict["documentNumber"] .safelyWrappingString()
                                self.documentTypeTF.text = responseDict["documentType"] .safelyWrappingString()
                                self.issuingCountryTF.text = responseDict["issuingCountryCode"] .safelyWrappingString()
                                self.documentnumberTF.text = responseDict["documentNumber"] .safelyWrappingString()
                                
                                self.customerid = responseDict["customerID"] .safelyWrappingString()
                                self.membernumber = responseDict["memberNumber"] .safelyWrappingString()
                                self.agentid = responseDict["agentID"] .safelyWrappingString()
                                self.userid = responseDict["userID"] .safelyWrappingString()
                                self.entityid = responseDict["entityID"] .safelyWrappingString()
                                self.nationalitycode = responseDict["nationalityCode"] .safelyWrappingString()
                                
                                
                                let birtdateserverformate = responseDict["birthDate"] .safelyWrappingString()
                                let myStringArr = birtdateserverformate.components(separatedBy: "T")//componentsSeparatedByString(" ")
                                let birthdate: String = myStringArr [0]
                                self.birthdayTF.text = birthdate
                                
                                let anniverserydateserverformate = responseDict["anniversaryDate"] .safelyWrappingString()
                                let anniveresryStringArr = anniverserydateserverformate.components(separatedBy: "T")//componentsSeparatedByString(" ")
                                let anniverserydate: String = anniveresryStringArr[0]
                                self.anniverasryTF.text = anniverserydate
                                
                                let passportdateserverformate = responseDict["passportExpirationDate"] .safelyWrappingString()
                                let passportStrinArr = passportdateserverformate.components(separatedBy: "T")
                                let passportdate: String = passportStrinArr [0]
                                self.expirydateTF.text = passportdate
                               
                                
                                if self.gender == "M"{
                                    
                                  self.femaleRadioButton.setImage(#imageLiteral(resourceName: "circleempty"), for: .normal)
                                  self.maleradioButton.setImage(#imageLiteral(resourceName: "circlefill"), for: .normal)
                                }
                                else{
                                    self.maleradioButton.setImage(#imageLiteral(resourceName: "circleempty"), for: .normal)
                                    self.femaleRadioButton.setImage(#imageLiteral(resourceName: "circlefill"), for: .normal)
                                }
                            
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                    else
                    {
                        AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                }
                else if reqType == RequestType.RequestupdateProfile {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                               let responseDict:[String:Any] = respDict["response"] as!  Dictionary<String,Any>
                                
                                
                                print("Update Success", responseDict)
                                
                                self.navigationController?.popViewController(animated: true)
                                
                            AppUtility.alert(message: message ?? "", title: "", controller: self)
                        }
                        else
                        {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                        }
                       }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                      else
                      {
                     AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                }
                else{
                    AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                }
            }
        }
    }
    
}
