//
//  EditProfile_TextField.swift
//  GoAutoTicket
//
//  Created by iMac on 21/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation
import UIKit

//MARK:- TextField Delegate & Functions
@available(iOS 13.0, *)
extension EditProfileViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        textField.text =  (self.country?.dialCode == "+95") ? "09" : ""
//         self.clearButton.isHidden = true
//         self.loginButton.backgroundColor = .lightGray
        
      if textField == phonenumTF{
            if  currentSelectedCountry == .myanmarCountry  {
            if let text = textField.text, text.count < 3 {
              textField.text = "09"
               //  self.clearButton.isHidden = true
               //  self.loginButton.backgroundColor = .lightGray
               //   self.loginButton.isEnabled = false
                //   loginButton.setTitleColor(.white, for: .normal)
              }
          }
       
        else{
           if let text = textField.text{
               textField.text = text
            }
        }
    }
        if textField == alternativephonenumTF{
            if  currentSelectedCountry == .myanmarCountry  {
                if let text = textField.text, text.count < 3 {
                    textField.text = "09"
                    //  self.clearButton.isHidden = true
                    //  self.loginButton.backgroundColor = .lightGray
                    //   self.loginButton.isEnabled = false
                    //   loginButton.setTitleColor(.white, for: .normal)
                }
            }
                
            else{
                if let text = textField.text{
                    textField.text = text
                }
            }
        }
        
//       else if textField == passwordTF{
//          if let text = textField.text{
//               textField.text = text
//            }
//        }
//
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
           if textField == passportTF{
            passportTF.resignFirstResponder()
            self.showdocumentView()
            passportTF.inputView = nil
            passportTF.tintColor = UIColor.clear
      
        }
        if textField == birthdayTF{
            selectedTFStr = "Birthday"
            self.showdatepicker()
            birthdayTF.tintColor = UIColor.clear
        }
        if textField == anniverasryTF{
            selectedTFStr = "Anniversery"
            self.showdatepicker()
            anniverasryTF.tintColor = UIColor.clear
        }
        if textField == expirydateTF{
            self.showdatepicker()
            expirydateTF.tintColor = UIColor.clear
        }
        if textField == documentTypeTF{
            documentTypeTF.inputView = typePicker
            documentTypeTF.tintColor = UIColor.clear
        }
        if textField == nrcStateTF{
            nrcStateTF.inputView = nrcstatepicker
            nrcStateTF.tintColor = UIColor.clear
        }
        if textField == nrcTownshipTF{
            nrcTownshipTF.inputView = townshippicker
            nrcTownshipTF.tintColor = UIColor.clear
        }
        if textField == nrcNationalityTF{
            nrcNationalityTF.inputView = nationalityidpicker
            nrcNationalityTF.tintColor = UIColor.clear
        }
        if textField == issuingCountryTF{
            countryselect = "Country"
            self.publicCountryView()
        }
        
        if textField == selectCountryTF{
            countryselect = "SelectCountry"
            self.publicCountryView()
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == documentTypeTF{
            if documentTypeTF.text == "NRC"{
                self.NRCView.isHidden = false
                self.NRCView.alpha = 1.0
            }
            else{
                self.NRCView.isHidden = true
                self.NRCView.alpha = 0.0
            }
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if self.country?.dialCode == "+95" {
            textField.text = "09"
        } else {
            textField.text = ""
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      //  self.loginButton.isUserInteractionEnabled = true
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        if textField == phonenumTF{
           
            
        if currentSelectedCountry == .myanmarCountry {
            if range.location <= 1 {
                let newPosition = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                return false
            }
        }
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
        switch currentSelectedCountry {
        case .indiaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
         //   clearButtonShowHide(count: textCount)
            if textField.text!.count >= 10 && range.length == 0 {
                return false
            }
            if textCount >= 10 {
                 textField.text = text
               // loginButton.backgroundColor = .orange
              //  loginButton.setTitleColor(.white, for: .normal)
              //  self.loginButton.sendActions(for: .touchUpInside)
              //  self.loginButton.isEnabled = true
                 self.phonenumTF.resignFirstResponder()
                return false
            } else {
             //   loginButton.setTitleColor(.white, for: .normal)
             ///   loginButton.backgroundColor = UIColor.lightGray
             //   self.loginButton.isEnabled = false
                return true
            }
        case .chinaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
           // clearButtonShowHide(count: textCount)
            if textField.text!.count >= 11 && range.length == 0 {
                return false
            }
            
            if textCount >= 11  {
                 textField.text = text
              //  loginButton.setTitleColor(.white, for: .normal)
             //   loginButton.backgroundColor = .orange
              //   self.loginButton.sendActions(for: .touchUpInside)
             //    self.loginButton.isEnabled = true
                 self.phonenumTF.resignFirstResponder()
                return false
            } else {
              //  loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = UIColor.lightGray
              //   self.loginButton.isEnabled = false
                return true
            }
        case .myanmarCountry:
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
            {
                return false
            }
            
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
              //  self.clearButton.isHidden = true
                return false
            }
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            if textCount > 2 {
              //  self.clearButton.isHidden = false
            } else {
              //  self.clearButton.isHidden = true
            }
            
            let object = validObj.getNumberRangeValidation(text)
            // let validCount = object.min
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if textField.text == "09" {
                    textField.text = "09"
                 //   self.clearButton.isHidden = true
                    return false
                }
            }
            
            if object.isRejected == true {
                textField.text = "09"
             //   self.clearButton.isHidden = true
                textField.resignFirstResponder()
                
                let text = "Invalid Mobile Number".localized
                
                AppUtility.alert(message: text, title: "", controller: self)
                //  self.showAlert(text, image: nil, simChanges: false)
                return false
            }
            if textCount < object.min {
              //  self.loginButton.isEnabled = false
            //    self.loginButton.backgroundColor = .lightGray
            } else if (textCount >= object.min && textCount < object.max) {
              //  self.loginButton.isEnabled = true
              //  loginButton.setTitleColor(.white, for: .normal)
              //  self.loginButton.backgroundColor = .orange
            } else if (textCount == object.max) {
                textField.text = text
              //  loginButton.setTitleColor(.white, for: .normal)
              //  self.loginButton.backgroundColor = .orange
              //  self.loginButton.sendActions(for: .touchUpInside)
              //  self.loginButton.isEnabled = true
                self.phonenumTF.resignFirstResponder()
                return false
            } else if textCount > object.max {
                return false
            }
        case .thaiCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
           // clearButtonShowHide(count: textCount)
            if textField.text!.count >= 9 && range.length == 0 {
                return false
            }
            
            if textCount >= 9  {
                textField.text = text
              //  loginButton.setTitleColor(.white, for: .normal)
               // loginButton.backgroundColor = .orange
               //  self.loginButton.sendActions(for: .touchUpInside)
              //   self.loginButton.isEnabled = true
                 self.phonenumTF.resignFirstResponder()
                return false
            }  else {
              //  loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = .lightGray
              //   self.loginButton.isEnabled = false
                return true
            }
            
        case .other:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
           // clearButtonShowHide(count: textCount)
            
            if textField.text!.count >= 13 && range.length == 0 {
                return false
            }
            
            
            if textCount >= 4 {
                textField.text = text
               // loginButton.setTitleColor(.white, for: .normal)
               // loginButton.backgroundColor = .orange
              //  self.loginButton.isEnabled = true
              if textCount == 13 {
                textField.text = text
              //   self.loginButton.sendActions(for: .touchUpInside)
                 self.phonenumTF.resignFirstResponder()
              }
                return false
            }
            
            if textCount > 13 {
                textField.text = text
              //  loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = .orange
              //   self.loginButton.isEnabled = true
              //   self.loginButton.sendActions(for: .touchUpInside)
                 self.phonenumTF.resignFirstResponder()
                return false
            }
            else {
              //  loginButton.setTitleColor(.white, for: .normal)
             //   loginButton.backgroundColor = .lightGray
             //    self.mobilenumTF.isEnabled = false
                return true
            }
            
//            if textCount > 13 {
//                loginButton.setTitleColor(.white, for: .normal)
//                loginButton.backgroundColor = .orange
//                //self.registerButton.sendActions(for: .touchUpInside)
//                return false
//            }
//
//            if textCount > 3  {
//                self.mobileNumTF.text = text
//                self.registerButton.isEnabled = true
//                loginButton.setTitleColor(.white, for: .normal)
//                self.loginButton.backgroundColor = .orange
//                //self.submitBtn.sendActions(for: .touchUpInside)
//                return false
//            }
//            else {
//                self.loginButton.isEnabled = false
//                self.loginButton.backgroundColor = .lightGray
//                return true
//            }
      }
    }
        if textField == alternativephonenumTF{
            
            
            if currentSelectedCountry == .myanmarCountry {
                if range.location <= 1 {
                    let newPosition = textField.endOfDocument
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                    return false
                }
            }
            
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
            switch currentSelectedCountry {
            case .indiaCountry:
                
                let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount  = text.count
                //   clearButtonShowHide(count: textCount)
                if textField.text!.count >= 10 && range.length == 0 {
                    return false
                }
                if textCount >= 10 {
                    textField.text = text
                    // loginButton.backgroundColor = .orange
                    //  loginButton.setTitleColor(.white, for: .normal)
                    //  self.loginButton.sendActions(for: .touchUpInside)
                    //  self.loginButton.isEnabled = true
                    self.alternativephonenumTF.resignFirstResponder()
                    return false
                } else {
                    //   loginButton.setTitleColor(.white, for: .normal)
                    ///   loginButton.backgroundColor = UIColor.lightGray
                    //   self.loginButton.isEnabled = false
                    return true
                }
            case .chinaCountry:
                
                let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount  = text.count
                // clearButtonShowHide(count: textCount)
                if textField.text!.count >= 11 && range.length == 0 {
                    return false
                }
                
                if textCount >= 11  {
                    textField.text = text
                    //  loginButton.setTitleColor(.white, for: .normal)
                    //   loginButton.backgroundColor = .orange
                    //   self.loginButton.sendActions(for: .touchUpInside)
                    //    self.loginButton.isEnabled = true
                    self.alternativephonenumTF.resignFirstResponder()
                    return false
                } else {
                    //  loginButton.setTitleColor(.white, for: .normal)
                    //  loginButton.backgroundColor = UIColor.lightGray
                    //   self.loginButton.isEnabled = false
                    return true
                }
            case .myanmarCountry:
                if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
                {
                    return false
                }
                
                if range.location == 0 && range.length > 1 {
                    textField.text = "09"
                    //  self.clearButton.isHidden = true
                    return false
                }
                
                let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount  = text.count
                if textCount > 2 {
                    //  self.clearButton.isHidden = false
                } else {
                    //  self.clearButton.isHidden = true
                }
                
                let object = validObj.getNumberRangeValidation(text)
                // let validCount = object.min
                
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                
                if (isBackSpace == -92) {
                    if textField.text == "09" {
                        textField.text = "09"
                        //   self.clearButton.isHidden = true
                        return false
                    }
                }
                
                if object.isRejected == true {
                    textField.text = "09"
                    //   self.clearButton.isHidden = true
                    textField.resignFirstResponder()
                    
                    let text = "Invalid Mobile Number".localized
                    
                    AppUtility.alert(message: text, title: "", controller: self)
                    //  self.showAlert(text, image: nil, simChanges: false)
                    return false
                }
                if textCount < object.min {
                    //  self.loginButton.isEnabled = false
                    //    self.loginButton.backgroundColor = .lightGray
                } else if (textCount >= object.min && textCount < object.max) {
                    //  self.loginButton.isEnabled = true
                    //  loginButton.setTitleColor(.white, for: .normal)
                    //  self.loginButton.backgroundColor = .orange
                } else if (textCount == object.max) {
                    textField.text = text
                    //  loginButton.setTitleColor(.white, for: .normal)
                    //  self.loginButton.backgroundColor = .orange
                    //  self.loginButton.sendActions(for: .touchUpInside)
                    //  self.loginButton.isEnabled = true
                    self.alternativephonenumTF.resignFirstResponder()
                    return false
                } else if textCount > object.max {
                    return false
                }
            case .thaiCountry:
                
                let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount  = text.count
                // clearButtonShowHide(count: textCount)
                if textField.text!.count >= 9 && range.length == 0 {
                    return false
                }
                
                if textCount >= 9  {
                    textField.text = text
                    //  loginButton.setTitleColor(.white, for: .normal)
                    // loginButton.backgroundColor = .orange
                    //  self.loginButton.sendActions(for: .touchUpInside)
                    //   self.loginButton.isEnabled = true
                    self.alternativephonenumTF.resignFirstResponder()
                    return false
                }  else {
                    //  loginButton.setTitleColor(.white, for: .normal)
                    //  loginButton.backgroundColor = .lightGray
                    //   self.loginButton.isEnabled = false
                    return true
                }
                
            case .other:
                
                let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount  = text.count
                // clearButtonShowHide(count: textCount)
                
                if textField.text!.count >= 13 && range.length == 0 {
                    return false
                }
                
                
                if textCount >= 4 {
                    textField.text = text
                    // loginButton.setTitleColor(.white, for: .normal)
                    // loginButton.backgroundColor = .orange
                    //  self.loginButton.isEnabled = true
                    if textCount == 13 {
                        textField.text = text
                        //   self.loginButton.sendActions(for: .touchUpInside)
                        self.alternativephonenumTF.resignFirstResponder()
                    }
                    return false
                }
                
                if textCount > 13 {
                    textField.text = text
                    //  loginButton.setTitleColor(.white, for: .normal)
                    //  loginButton.backgroundColor = .orange
                    //   self.loginButton.isEnabled = true
                    //   self.loginButton.sendActions(for: .touchUpInside)
                    self.alternativephonenumTF.resignFirstResponder()
                    return false
                }
                else {
                    //  loginButton.setTitleColor(.white, for: .normal)
                    //   loginButton.backgroundColor = .lightGray
                    //    self.mobilenumTF.isEnabled = false
                    return true
                }
                
                //            if textCount > 13 {
                //                loginButton.setTitleColor(.white, for: .normal)
                //                loginButton.backgroundColor = .orange
                //                //self.registerButton.sendActions(for: .touchUpInside)
                //                return false
                //            }
                //
                //            if textCount > 3  {
                //                self.mobileNumTF.text = text
                //                self.registerButton.isEnabled = true
                //                loginButton.setTitleColor(.white, for: .normal)
                //                self.loginButton.backgroundColor = .orange
                //                //self.submitBtn.sendActions(for: .touchUpInside)
                //                return false
                //            }
                //            else {
                //                self.loginButton.isEnabled = false
                //                self.loginButton.backgroundColor = .lightGray
                //                return true
                //            }
            }
        }
//     if textField ==  {
//       if string == " " {
//          return false
//        }
//       }
//    if textField == confirmPasswordTF {
//            if string == " " {
//                return false
//            }
//    }
   if textField == emailTF {
          if string == " " {
          return false
        }
    }
//   if textField == passportTF{
//    textField.resignFirstResponder()
//    return false
//    }
            
       else {
            // password TF
       }
        
        return true
        
    }
    
}
@available(iOS 13.0, *)
extension EditProfileViewController: CountryViewControllerDelegate{

    @available(iOS 13.0, *)
    func countryViewController(_ list: CountryViewController, country: Country) {
    self.country = country
        
        
        if countryselect == "Country"{
            let countryname = String.init(format: "", country.name)
            issuecountryView?.wrapCountryViewData(img: "", str: countryname)
            self.issuingCountryTF.text = self.country?.name
            self.issuingCountryTF.resignFirstResponder()
            countryselect = ""
        }
        else if  countryselect == "SelectCountry" {
           let countryname = String.init(format: "", country.name)
         //   let countrycode = String.init(format: "", country.dialCode)
            issuecountryView?.wrapCountryViewData(img: "", str: countryname)
            self.selectCountryTF.text = self.country?.name
      //      let varData = self.countryName(from: "95")
            self.selectCountryTF.resignFirstResponder()
            countryselect = ""
        }
        else{
            self.phonenumTF.leftView = nil
            
            let countryCode = String.init(format: "(%@)", country.dialCode)
            countryView?.wrapCountryViewData(img: country.code, str: countryCode)
            //self.mobileNumTF.leftView = countryView
            self.phonenumTF.leftView = countryView
            
            
            self.Altcountry = country
            self.alternativephonenumTF.leftView = nil
            let altcountryCode = String.init(format: "(%@)", country.dialCode)
            AltcountryView?.wrapCountryViewData(img: country.code, str: altcountryCode)
            //self.mobileNumTF.leftView = countryView
            self.alternativephonenumTF.leftView = AltcountryView
            
            self.phonenumTF.text = (self.country?.dialCode == "+95") ? "09" : ""
            self.phonenumTF.becomeFirstResponder()
              
            self.alternativephonenumTF.text = (self.Altcountry?.dialCode == "+95") ? "09" : ""
            self.alternativephonenumTF.becomeFirstResponder()
        }
    
       
      
        
//    self.countryTF.leftView = nil
//    let countryname = String.init(format: "(%@)", country.name)
//    countryView?.wrapCountryViewData(img: country.code, str: countryname)
//           //self.mobileNumTF.leftView = countryView
//
//    self.countryTF.leftView = countryView
        
        
    list.dismiss(animated: true, completion: nil)
    
    switch self.country?.dialCode {
    case "+91":
        currentSelectedCountry = .indiaCountry
    case "+95":
        currentSelectedCountry = .myanmarCountry
    case "+66":
        currentSelectedCountry = .thaiCountry
    case "+86":
        currentSelectedCountry = .chinaCountry
    default:
        currentSelectedCountry  = .other
    }
    
//    self.countryTF.text = (self.country?.name == "myanmar") ? "myanmar" : ""
//    self.countryTF.becomeFirstResponder()
}

    @available(iOS 13.0, *)
    func countryViewControllerCloseAction(_ list: CountryViewController) {
    list.dismiss(animated: true, completion: nil)
}

    @available(iOS 13.0, *)
 func publicCountryView() {
    guard let countryVC = countryViewController(delegate: self) else { return }
    countryVC.modalPresentationStyle = .fullScreen
    self.present(countryVC, animated: true, completion: nil)
}
    
}
