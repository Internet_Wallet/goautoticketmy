//
//  MyAccountViewController.swift
//  GoAutoTicket
//
//  Created by iMac on 20/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

class MyAccountViewController: UIViewController {
    
    @IBOutlet weak var navigationView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var myaccountSegmentController: UISegmentedControl!
    
    @IBOutlet var profileView: UIView!
    
    @IBOutlet var travelerView: UIView!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        profileView.alpha = 1.0
        travelerView.isHidden = true
        travelerView.alpha = 0.0
       myaccountSegmentController.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func myaccountsegmentClick(_ sender: Any) {
        if myaccountSegmentController.selectedSegmentIndex == 0{
            profileView.alpha = 1.0
            travelerView.isHidden = true
            travelerView.alpha = 0.0
        }
        else
        {
            profileView.alpha = 0.0
            travelerView.isHidden = false
            travelerView.alpha = 1.0
        }
        
    }

    
}
