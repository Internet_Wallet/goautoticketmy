//
//  MyTravelerTableViewCell.swift
//  GoAutoTicket
//
//  Created by iMac on 24/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

class MyTravelerTableViewCell: UITableViewCell {

    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var genderLabel: UILabel!
    @IBOutlet var DOBLabel: UILabel!
    
    @IBOutlet var deletebutton: UIButton!
    @IBOutlet var editbutton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
