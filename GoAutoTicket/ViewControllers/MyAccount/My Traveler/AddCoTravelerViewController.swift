//
//  AddCoTravelerViewController.swift
//  GoAutoTicket
//
//  Created by iMac on 24/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
@available(iOS 13.0, *)
class AddCoTravelerViewController: UIViewController,CountryLeftViewDelegate,PhValidationProtocol,HelperFunctions  {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    
    // mainview
    
    @IBOutlet var nameView: UIView!
    
    @IBOutlet var firstnameTF: NoPasteTextField!
    @IBOutlet var lastnameTF: NoPasteTextField!
    @IBOutlet var genderLabel: UILabel!
    @IBOutlet var maleradioButton: UIButton!
    @IBOutlet var maleButton: UIButton!
    @IBOutlet var femaleradioButton: UIButton!
    @IBOutlet var femaleButton: UIButton!
    
    
    @IBOutlet var birthView: UIView!
    @IBOutlet var birthdayTF: NoPasteTextField!
    
    
    @IBOutlet var anniverseryView: UIView!
    @IBOutlet var anniveresryTF: NoPasteTextField!
    
    
    @IBOutlet var phonenumView: UIView!
    @IBOutlet var phonenumTF: NoPasteTextField!
    @IBOutlet var altrphonenumTF: NoPasteTextField!
    
    
    @IBOutlet var usercountryView: UIView!
    @IBOutlet var selectcountryTF: NoPasteTextField!
    @IBOutlet var cityTF: NoPasteTextField!
    @IBOutlet var addressTF: NoPasteTextField!
    @IBOutlet var zipcodeTF: NoPasteTextField!
    
    @IBOutlet var passportView: UIView!
    @IBOutlet var enterdocumentTF: NoPasteTextField!
    
    @IBOutlet var buttonView: UIView!
    @IBOutlet var saveButton: UIButton!
    
    
    // Document View
    
    @IBOutlet var documentView: UIView!
    @IBOutlet var enterdocumentLabel: MarqueeLabel!
    
    @IBOutlet var documentTypeTF: ACFloatingTextfield!
    @IBOutlet var issuingTF: ACFloatingTextfield!
    @IBOutlet var documentNumberTF: ACFloatingTextfield!
    @IBOutlet var expirydateTF: ACFloatingTextfield!
    
    // NRC View
    
    @IBOutlet var nrcView: UIView!
    @IBOutlet var nrcstateTF: ACFloatingTextfield!
    @IBOutlet var townshipTF: ACFloatingTextfield!
    @IBOutlet var nationalityTF: ACFloatingTextfield!
    @IBOutlet var sixdigitcodeTF: ACFloatingTextfield!
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var submitButton: UIButton!
    
    var cotravelerparameters:[String : Any] = [:]
    var updatecotravelerparameters:[String : Any] = [:]
    
    var picker = UIDatePicker()
    var strTF = String()
    
    var  pickerTextView = UITextView()
    
    var typePicker = UIPickerView()
    
    var nrcstatepicker = UIPickerView()
    
    var townshippicker = UIPickerView()
    
    var nationalityidpicker = UIPickerView()
       
    var typearr = [String]()
    
    var nrcstatearr = [String]()
    
    var townshiparr = [String]()
    
    var natioanlityid = [String]()
    
    var countryselect = ""
    
    var imagebase64Str = ""
    
    var unchecked = true
    var radioCheck = true
    
    var gender = ""
    var genderdesc = ""
    
    var customerid = ""
    var membernum = ""
    var UserDisplayName = ""
    var issuingcountrycode = ""
    var nationalitycode = ""
    var ActlGender = ""
    var documenttype = ""
    var documentnum = ""
    var seqnum = ""
    
    var selectedTFStr = ""
    var dateformate = ""
    
    var edittravelerdict: [String : Any]?
    var FromScreen = ""
    
    var countryView   : PaytoViews?
    var country       : Country?
    var AltcountryView   : PaytoViews?
    var Altcountry       : Country?
    
    var issuecountryView   : PaytoViews?
    var issuecountry       : Country?
    
    let validObj = PayToValidations.init()
    
    public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.documentView.frame = self.view.frame
        self.view.addSubview(self.documentView)
        nrcView.isHidden = true
        nrcView.alpha = 0.0
        documentView.isHidden = true
        self.setphoneUI()
        self.setaltphoneUI()
        self.setcountrypick()
        self.selectcountrypick()
        typePicker.delegate = self
        typePicker.dataSource = self
        
        nrcstatepicker.delegate = self
        nrcstatepicker.dataSource = self
        
        townshippicker.delegate = self
        townshippicker.dataSource = self
        
        nationalityidpicker.delegate = self
        nationalityidpicker.dataSource = self
        
        typearr = ["Passport","NRC"]
        natioanlityid = ["N","P","E"]
        nrcstatearr = ["14/(Ayeyarwady Divsion)","7/(Bago Divison)","12/(Yangon Divison)","4/(Chin State)"]
        townshiparr = ["14/(BaKaLa)Bogale","7/(DaAuNa)Daik-U","12/(DaGaNa)Dagon","4/(MaTaNa)Mindat"]
        
        documentTypeTF.rightViewMode = .always
        documentTypeTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
        expirydateTF.rightViewMode = .always
        expirydateTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
        nrcstateTF.rightViewMode = .always
        nrcstateTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
               
        nationalityTF.rightViewMode = .always
        nationalityTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
        townshipTF.rightViewMode = .always
        townshipTF.rightView = UIImageView(image: UIImage(named: "graydownArrow"))
        
        if FromScreen == "EDIT"{
            let locationdictTemp = edittravelerdict?["location"] as? Dictionary<String,Any>
            let contactdictTemp = edittravelerdict?["contactInformation"] as? Dictionary<String,Any>
            
            firstnameTF.text = edittravelerdict?["firstName"] as? String
            lastnameTF.text = edittravelerdict?["lastName"] as? String
           
           
            phonenumTF.text = contactdictTemp?["phoneNumber"] as? String
            altrphonenumTF.text = contactdictTemp?["phoneNumber"] as? String
            selectcountryTF.text = locationdictTemp?["country"] as? String
            cityTF.text = locationdictTemp?["city"] as? String
            addressTF.text = locationdictTemp?["address"] as? String
            zipcodeTF.text = locationdictTemp?["zipCode"] as? String
            enterdocumentTF.text = edittravelerdict?["documentType"] as? String
            issuingcountrycode =  edittravelerdict?["issuingCountryCode"] as? String ?? ""
            nationalitycode =  edittravelerdict?["nationalityCode"] as? String ?? ""
            customerid =  edittravelerdict?["customerID"] as? String ?? ""
            membernum =  edittravelerdict?["memberNumber"] as? String ?? ""
            UserDisplayName =  edittravelerdict?["userDisplayName"] as? String ?? ""
            ActlGender = edittravelerdict?["actlGender"] as? String ?? ""
            seqnum = edittravelerdict?["seqNo"] as? String ?? ""
            documentnum = edittravelerdict?["documentNumber"] as? String ?? ""
            gender = edittravelerdict?["gender"] as? String ?? ""
            genderdesc = edittravelerdict?["genderDesc"] as? String ?? ""
            
            documentTypeTF.text = edittravelerdict?["documentType"] as? String
            issuingTF.text = edittravelerdict?["issuingCountryCode"] as? String
            documentNumberTF.text = edittravelerdict?["documentNumber"] as? String
           
            
            let birtdateserverformate = edittravelerdict?["birthDate"] as? String
            let myStringArr = birtdateserverformate?.components(separatedBy: "T")//componentsSeparatedByString(" ")
            let birthdate: String = myStringArr? [0] ?? ""
            birthdayTF.text = birthdate
            
            let anniverserydateserverformate = edittravelerdict?["anniversaryDate"] as? String
            let anniveresryStringArr = anniverserydateserverformate?.components(separatedBy: "T")//componentsSeparatedByString(" ")
            let anniverserydate: String = anniveresryStringArr? [0] ?? ""
            anniveresryTF.text = anniverserydate
            
            let passportdateserverformate = edittravelerdict?["passportExpirationDate"] as? String
            let passportStrinArr = passportdateserverformate?.components(separatedBy: "T")
            let passportdate: String = passportStrinArr? [0] ?? ""
            expirydateTF.text = passportdate
            
            
            
            if gender == "M"{
                 femaleradioButton.setImage(UIImage(named:"circleempty"), for: .normal)
                 maleradioButton.setImage( UIImage(named:"circlefill"), for: .normal)
            }
            else{
                femaleradioButton.setImage(UIImage(named:"circlefill"), for: .normal)
                maleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
            }
        
            
        }
        else{
            //firstnameTF.text = ""
            gender = "M"
            genderdesc = "Male"

        }
        
        
    }
    
    func setphoneUI(){
          //   dismissKey()
           //  headingLabel.setPropertiesForLabel()
            // SigninTF.setPropertiesForTextField()
             phNumValidationsFile = getDataFromJSONFile() ?? []
             self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
             countryView = PaytoViews.updateView()
             let countryCode = String(format: "(%@)", "+95")
             countryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
             countryView?.delegate = self
             self.phonenumTF.leftViewMode = .always
             self.phonenumTF.leftView     = countryView
             
         }
    func setaltphoneUI(){
           //   dismissKey()
            //  headingLabel.setPropertiesForLabel()
             // SigninTF.setPropertiesForTextField()
              phNumValidationsFile = getDataFromJSONFile() ?? []
              self.Altcountry = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
              AltcountryView = PaytoViews.updateView()
              let countryCode = String(format: "(%@)", "+95")
              AltcountryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
              AltcountryView?.delegate = self
              self.altrphonenumTF.leftViewMode = .always
              self.altrphonenumTF.leftView     = AltcountryView
              
          }
    func setcountrypick(){
         self.issuecountry = Country.init(name: "Myanmar", code: "", dialCode: "")
         issuecountryView = PaytoViews.countryupdateView()
         let countryCode = String(format: "", "myanmar")
         issuecountryView?.wrapCountryViewData(img: "", str: countryCode)
         issuecountryView?.delegate = self
         issuingTF.inputView = issuecountryView
     }
    
    func selectcountrypick(){
         self.issuecountry = Country.init(name: "Myanmar", code: "", dialCode: "")
         issuecountryView = PaytoViews.countryupdateView()
         let countryCode = String(format: "", "myanmar")
         issuecountryView?.wrapCountryViewData(img: "", str: countryCode)
         issuecountryView?.delegate = self
         selectcountryTF.inputView = issuecountryView
     }
    
    func showdocumentView(){
        
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseIn,
                       animations: {
                self.documentView.isHidden = false
        })
        
        
    }
    
    func  showdatepicker(){
        picker = UIDatePicker(frame: CGRect(x: 0, y: 210, width: 320, height: 250))

        picker.date = Date(timeIntervalSinceNow: TimeInterval(2))
      //  picker.minimumDate = Date(timeIntervalSinceNow: TimeInterval(0))

        picker.datePickerMode = .date
        picker.backgroundColor = UIColor.white
        picker.addTarget(self, action: #selector(updateTextField(_:)), for: .valueChanged)

       
        if selectedTFStr == "Birthday"{
          //  selectedTFStr = ""
            
            birthdayTF.inputView = picker
        }
        else if selectedTFStr == "Anniversery"{
           // selectedTFStr = ""
            
            anniveresryTF.inputView = picker
        }
        else{
            
            expirydateTF.inputView = picker
        }
        
        
        
       //  let df = DateFormatter()
      //   df.dateFormat = "dd/MM/yyyy"
      //   expirydateTF.text = "\(df.string(from: picker.date))"

        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.tintColor = UIColor.gray
        numberToolbar.items = [UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)), UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneWithNumberPad))]

        numberToolbar.sizeToFit()
        if selectedTFStr == "Birthday"{
            //  selectedTFStr = ""
            
            birthdayTF.inputAccessoryView = numberToolbar
        }
        else if selectedTFStr == "Anniversery"{
            // selectedTFStr = ""
            
            anniveresryTF.inputAccessoryView = numberToolbar
        }
        else{
            
            expirydateTF.inputAccessoryView = numberToolbar
        }
        
        
    }
    
    @objc func cancelNumberPad() {
        if selectedTFStr == "Birthday"{
            selectedTFStr = ""
            birthdayTF.text = ""
            birthdayTF.resignFirstResponder()
        }
        else if selectedTFStr == "Anniversery"{
            selectedTFStr = ""
            anniveresryTF.text = ""
            anniveresryTF.resignFirstResponder()
        }
        else{
            expirydateTF.text = ""
            expirydateTF.resignFirstResponder()
        }
       
    }

    @objc func doneWithNumberPad() {

        if selectedTFStr == "Birthday"{
            selectedTFStr = ""
            
            birthdayTF.resignFirstResponder()
        }
        else if selectedTFStr == "Anniversery"{
            selectedTFStr = ""
            
            anniveresryTF.resignFirstResponder()
        }
        else{
            
            expirydateTF.resignFirstResponder()
        }

    }

    @objc func updateTextField(_ sender: Any?) {
        //    _Picker1 = (UIDatePicker*)self.dateOfBirthTF.inputView;
        //    self.dateOfBirthTF.text = [NSString stringWithFormat:@"%@",_Picker1.date];

            let df = DateFormatter()
            // df.dateStyle = NSDateIntervalFormatterMediumStyle;

            df.dateFormat = "yyyy-MM-dd"
        
        if selectedTFStr == "Birthday"{
            birthdayTF.text = "\(df.string(from: picker.date))"
        }
        else if selectedTFStr == "Anniversery"{
            anniveresryTF.text = "\(df.string(from: picker.date))"
        }
        else{
           expirydateTF.text = "\(df.string(from: picker.date))"
        }
        
           
    }
    @IBAction func backButtonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func maleradioButtonClick(_ sender: Any) {
        (sender as AnyObject).setImage(UIImage(named:"circlefill"), for: .normal)
        femaleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
        gender = "M"
        genderdesc = "Male"
        radioCheck = false
    }
    
    @IBAction func femaleradioButtonClick(_ sender: Any) {
        (sender as AnyObject).setImage(UIImage(named:"circlefill"), for: .normal)
         maleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
        gender = "F"
        genderdesc = "Female"
        radioCheck = false
    }
    
    @IBAction func maleButtonClick(_ sender: Any) {
        maleradioButton.setImage(UIImage(named:"circlefill"), for: .normal)
        femaleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
        gender = "M"
        genderdesc = "Male"
        radioCheck = false
    }
    @IBAction func femaleButonClick(_ sender: Any) {
        femaleradioButton.setImage(UIImage(named:"circlefill"), for: .normal)
        maleradioButton.setImage( UIImage(named:"circleempty"), for: .normal)
        gender = "F"
        genderdesc = "Female"
        radioCheck = false
        
    }
    @IBAction func cancelButtonClick(_ sender: Any) {
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut,
                       animations: {
                self.documentView.isHidden = true
        })
        
    }
    @IBAction func submitButtonClick(_ sender: Any) {
        if documentTypeTF.text == ""{
            AppUtility.alert(message: "Please Select DocumentType".localized, title: "", controller: self)
        }
        else if issuingTF.text == ""{
            AppUtility.alert(message: "Please Select Country".localized, title: "", controller: self)
        }
        else if documentNumberTF.text == ""{
            AppUtility.alert(message: "Please Enter Documentnumber".localized, title: "", controller: self)
        }
        else if expirydateTF.text == ""{
            AppUtility.alert(message: "Please Select Expirydate".localized, title: "", controller: self)
        }
        else{
            self.documentView.isHidden = true
            enterdocumentTF.text = documentNumberTF.text ?? ""
        }
       
    }
    @IBAction func saveButtonClick(_ sender: Any) {
        if FromScreen == "EDIT"{
            if firstnameTF.text == ""{
                AppUtility.alert(message: "Please Enter First Name".localized, title: "", controller: self)
            }
            else if lastnameTF.text == ""{
                AppUtility.alert(message: "Please Enter Last Name".localized, title: "", controller: self)
            }
            else if birthdayTF.text == ""{
                AppUtility.alert(message: "Please Select DOB".localized, title: "", controller: self)
            }
            else if anniveresryTF.text == ""{
                AppUtility.alert(message: "Please Select Anniversary".localized, title: "", controller: self)
            }
            if  currentSelectedCountry == .myanmarCountry {
                if phonenumTF.text == "09" || phonenumTF.text == "" {
                    AppUtility.alert(message: "Please Enter MM Mobile Number".localized, title: "", controller: self)
                }
                else if altrphonenumTF.text == "09" || altrphonenumTF.text == "" {
                    AppUtility.alert(message: "Please Enter Alternative MM Mobile Number".localized, title: "", controller: self)
                }
            }
            else{
                if phonenumTF.text == "" {
                    AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
                }
                else if altrphonenumTF.text == "" {
                    AppUtility.alert(message: "Please Enter Alternative Mobile Number".localized, title: "", controller: self)
                }
            }
            if selectcountryTF.text == ""{
                 AppUtility.alert(message: "Please Select Country".localized, title: "", controller: self)
            }
            else if cityTF.text == ""{
                 AppUtility.alert(message: "Please Enter City".localized, title: "", controller: self)
            }
            else if addressTF.text == ""{
                 AppUtility.alert(message: "Please Enter Address".localized, title: "", controller: self)
            }
            else if zipcodeTF.text == ""{
                 AppUtility.alert(message: "Please Enter Zipcode".localized, title: "", controller: self)
            }
            else if enterdocumentTF.text == ""{
                 AppUtility.alert(message: "Please Enter Documentnumber".localized, title: "", controller: self)
            }
            else{
                self.updateCoTravelerparams()
                self.apirequestupdatecotravelercall()
            }
           
        }
        else{
            if firstnameTF.text == ""{
                AppUtility.alert(message: "Please Enter First Name".localized, title: "", controller: self)
            }
            else if lastnameTF.text == ""{
                AppUtility.alert(message: "Please Enter Last Name".localized, title: "", controller: self)
            }
            else if birthdayTF.text == ""{
                AppUtility.alert(message: "Please Select DOB".localized, title: "", controller: self)
            }
            else if anniveresryTF.text == ""{
                AppUtility.alert(message: "Please Select Anniversary".localized, title: "", controller: self)
            }
            if  currentSelectedCountry == .myanmarCountry {
                if phonenumTF.text == "09" || phonenumTF.text == "" {
                    AppUtility.alert(message: "Please Enter MM Mobile Number".localized, title: "", controller: self)
                }
                else if altrphonenumTF.text == "09" || altrphonenumTF.text == "" {
                    AppUtility.alert(message: "Please Enter Alternative MM Mobile Number".localized, title: "", controller: self)
                }
            }
            else{
                if phonenumTF.text == "" {
                    AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
                }
                else if altrphonenumTF.text == "" {
                    AppUtility.alert(message: "Please Enter Alternative Mobile Number".localized, title: "", controller: self)
                }
            }
            if selectcountryTF.text == ""{
                 AppUtility.alert(message: "Please Select Country".localized, title: "", controller: self)
            }
            else if cityTF.text == ""{
                 AppUtility.alert(message: "Please Enter City".localized, title: "", controller: self)
            }
            else if addressTF.text == ""{
                 AppUtility.alert(message: "Please Enter Address".localized, title: "", controller: self)
            }
            else if zipcodeTF.text == ""{
                 AppUtility.alert(message: "Please Enter Zipcode".localized, title: "", controller: self)
            }
            else if enterdocumentTF.text == ""{
                 AppUtility.alert(message: "Please Enter Documentnumber".localized, title: "", controller: self)
            }
            else{
                self.createCoTravelerparamters()
                self.apirequestcreatecotravelercall()
            }
        }
       
    }
    
    func createCoTravelerparamters (){
        
        let txtData1 = documentTypeTF.text ?? ""
        let txtData2 = documentNumberTF.text ?? ""
        let countryData = selectcountryTF.text ?? ""
        let zipcodeData = zipcodeTF.text ?? ""
        cotravelerparameters = ["Request": [
            "EntityID": "",
            "UserID": "",
            "AgentID": "",
            "CustomerID": "",
            "FirstName": firstnameTF.text ?? "",
            "LastName": lastnameTF.text ?? "",
            "ContactInformation": [
                "PhoneNumber": phonenumTF.text ?? "",
                "ActlFormatPhoneNumber": "95-\(altrphonenumTF.text ?? "")",
                "HomePhoneNumber": "09",
                "PhoneNumberCountryCode": "+95",
                "HomePhoneNumberCountryCode": "+95",
                "ActlFormatHomePhoneNumber": "95-09"
            ],
            "Location":[
                "Country": countryData,
                "Address": addressTF.text ?? "",
                "City": cityTF.text ?? "",
                "ZipCode": zipcodeData
            ],
            "OpenIDs": [:],
            "GenderDesc": genderdesc,
            "Gender": gender,
            "ActlGender": "19",
            "BirthDate": birthdayTF.text ?? "",
            "AnniversaryDate": anniveresryTF.text ?? "",
            "PassportExpirationDate": expirydateTF.text ?? "",
            "DocumentType": txtData1,
            "DocumentNumber":txtData2
            ],
            "flags": [:]
        ]
        
       //  print("#####",cotravelerparameters)
    }
    
    func updateCoTravelerparams (){
        
        let txtData1 = documentTypeTF.text ?? ""
        let txtData2 = documentNumberTF.text ?? ""
        let countryData = selectcountryTF.text ?? ""
        let zipcodeData = zipcodeTF.text ?? ""
        
        let contactInfoTotal = ["PhoneNumber": phonenumTF.text ?? "","ActlFormatPhoneNumber": "95-\(altrphonenumTF.text ?? "")","HomePhoneNumber": "09","PhoneNumberCountryCode": "+95","HomePhoneNumberCountryCode": "+95","ActlFormatHomePhoneNumber": "95-09"]
        let requestDictionary = ["CustomerID": customerid,"MemberNumber":membernum,"UserDisplayName": UserDisplayName,"FirstName": firstnameTF.text ?? "","LastName": lastnameTF.text ?? "", "ContactInformation": contactInfoTotal , "Location":["CountryID": "mm", "Country": countryData,"Address": addressTF.text ?? "","City": cityTF.text ?? "","ZipCode": zipcodeData],"IssuingCountryCode": issuingTF.text ?? "","NationalityCode":"mm" ,"GenderDesc":genderdesc ,"Gender": gender,"ActlGender": "19","BirthDate": birthdayTF.text ?? "","AnniversaryDate": anniveresryTF.text ?? "","PassportExpirationDate": expirydateTF.text ?? "","DocumentType": txtData1,"DocumentNumber": txtData2,"SeqNo": seqnum] as [String : Any]
        updatecotravelerparameters = ["Request":requestDictionary,"flags":[:]]
    }
    
}
@available(iOS 13.0, *)
extension AddCoTravelerViewController : UIPickerViewDelegate,UIPickerViewDataSource{
    
      func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
       
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           var rows: Int = 0
        if pickerView == nrcstatepicker {
            rows = nrcstatearr.count
        }
        if pickerView == typePicker{
            rows = typearr.count
        }
        
        if pickerView == townshippicker {
            rows = townshiparr.count
        }
        if pickerView == nationalityidpicker{
            rows = natioanlityid.count
        }
        
          return rows
       }
      
      func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var returnString = String()
        if pickerView == typePicker
        {
            returnString = typearr[row]
        }
        if pickerView == nrcstatepicker
        {
            returnString = nrcstatearr[row]
        }
        if pickerView == townshippicker
        {
            returnString = townshiparr[row]
        }
        if pickerView == nationalityidpicker{
            returnString = natioanlityid[row]
        }
        return returnString
      }
      
      func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == typePicker {
            documentTypeTF.text =  typearr[row]
            documentTypeTF.textColor = .black
        }
        if pickerView == nrcstatepicker {
            nrcstateTF.text =  nrcstatearr[row]
            nrcstateTF.textColor = .black
        }
        if pickerView == townshippicker {
            townshipTF.text =  townshiparr[row]
            townshipTF.textColor = .black
        }
        if pickerView == nationalityidpicker {
            nationalityTF.text =  natioanlityid[row]
            nationalityTF.textColor = .black
        }

          
      }
      
      func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    
        let pickerLabel = UILabel()
        
        pickerLabel.textAlignment = .center
        if pickerView == typePicker {
            pickerLabel.text = typearr[row]
        }
        if pickerView == nrcstatepicker {
            pickerLabel.text = nrcstatearr[row]
        }
        if pickerView == townshippicker {
            pickerLabel.text = townshiparr[row]
        }
        if pickerView == nationalityidpicker {
            pickerLabel.text = natioanlityid[row]
        }
        
          return pickerLabel
      }
}

@available(iOS 13.0, *)
extension AddCoTravelerViewController : ApiRequestProtocol{
    
    func apirequestcreatecotravelercall(){
        AppUtility.showLoading(self.view)
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        DispatchQueue.main.async {
            
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/cotraveler/create")!, requestData:self.cotravelerparameters, requestType: RequestType.Requestcreatecotraveler, httpMethodName: "POST")
        }
    }
    
    func apirequestupdatecotravelercall(){
           AppUtility.showLoading(self.view)
           let apiReqObj = ApiRequestClass()
           apiReqObj.customDelegate = self
           DispatchQueue.main.async {
               
               apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/cotraveler/update")!, requestData:self.updatecotravelerparameters, requestType: RequestType.Requestupdatecotraveler, httpMethodName: "POST")
           }
       }
       
       func httpResponse(responseObj: Any, reqType: RequestType)
       {
           DispatchQueue.main.async{
               
               AppUtility.hideLoading(self.view)
               
               if let respMsg = responseObj as? String
               {
                   AppUtility.alert(message: respMsg, title: "", controller: self)
               }
               else
               {
                   
                   var respDict = [String:Any]()
                   if responseObj is [Any]
                   {
                      // self.tokenarray = responseObj as! [Any]
                   }
                   else if responseObj is [String:Any]
                   {
                       respDict = responseObj as! Dictionary<String,Any>
                       
                   }
                   
                   if reqType == RequestType.RequestTypeEnvironment
                   {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                  
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }

                       else
                       {
                          AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                   }
                   else if reqType == RequestType.Requestcreatecotraveler {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                   
                                  self.navigationController?.popViewController(animated: true)
                                 AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }
                       else
                       {
                           AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                       
                   }
                   else if reqType == RequestType.Requestupdatecotraveler {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                                
                                 self.navigationController?.popViewController(animated: true)
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                    else
                    {
                        AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                   }
                   else{
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                   }
               }
           }
       }
       
     
}


