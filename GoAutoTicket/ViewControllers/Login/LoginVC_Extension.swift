//
//  Login_LeftView.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension LoginViewController: CountryViewControllerDelegate{

    @available(iOS 13.0, *)
    func countryViewController(_ list: CountryViewController, country: Country) {
    self.country = country
    self.SigninTF.leftView = nil
    
    let countryCode = String.init(format: "(%@)", country.dialCode)
    countryView?.wrapCountryViewData(img: country.code, str: countryCode)
    //self.mobileNumTF.leftView = countryView
        if segmentStr == "Second"{
           self.SigninTF.leftView = countryView
        }
        else{
            //
         }
        
    list.dismiss(animated: true, completion: nil)
    
    switch self.country?.dialCode {
    case "+91":
        currentSelectedCountry = .indiaCountry
    case "+95":
        currentSelectedCountry = .myanmarCountry
    case "+66":
        currentSelectedCountry = .thaiCountry
    case "+86":
        currentSelectedCountry = .chinaCountry
    default:
        currentSelectedCountry  = .other
    }
        
    if segmentStr == "Second"{
      self.SigninTF.text = (self.country?.dialCode == "+95") ? "09" : ""
      self.SigninTF.becomeFirstResponder()
     }
     else{
        //
    }
    
  //  self.mobileNumTF.text = (self.country?.dialCode == "+95") ? "09" : ""
  //  self.mobileNumTF.becomeFirstResponder()
}

    @available(iOS 13.0, *)
    func countryViewControllerCloseAction(_ list: CountryViewController) {
    list.dismiss(animated: true, completion: nil)
}

    @available(iOS 13.0, *)
    func publicCountryView() {
    guard let countryVC = countryViewController(delegate: self) else { return }
    countryVC.modalPresentationStyle = .fullScreen
    self.present(countryVC, animated: true, completion: nil)
}
    
}
@available(iOS 13.0, *)
extension LoginViewController: ApiRequestProtocol {
   
    func apirequestcall(){
           AppUtility.showLoading(self.view)
                   let apiReqObj = ApiRequestClass()
                   apiReqObj.customDelegate = self
                     DispatchQueue.main.async {
                   
                        apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/user/login")!, requestData: self.parameters, requestType: RequestType.RequestTypeLogin, httpMethodName: "POST")
                   }
       }
    
    func httpResponse(responseObj: Any, reqType: RequestType)
    {
        DispatchQueue.main.async{
            
            AppUtility.hideLoading(self.view)
            
            if let respMsg = responseObj as? String
            {
                AppUtility.alert(message: respMsg, title: "", controller: self)
            }
            else
            {
                
                var respDict = [String:Any]()
                if responseObj is [Any]
                {
                   // self.tokenarray = responseObj as! [Any]
                }
                else if responseObj is [String:Any]
                {
                    respDict = responseObj as! Dictionary<String,Any>
                    
                }
                
                if reqType == RequestType.RequestTypeEnvironment
                {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                                
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                    else
                    {
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                }
                else if reqType == RequestType.RequestTypeLogin {
                    if respDict.count > 0
                    {
                        
                        let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                        let code = itemDict["code"] as? Int
                        let message = itemDict["message"] as? String
                        
                        print("#####",code as Any)
                        if let isValidCode = code {
                            if isValidCode == 0 {
                                let responseDict:[String:Any] = respDict["response"] as!  Dictionary<String,Any>
                                
                                UserDefaults.standard.set("Login Success", forKey: "UserLogedin")
                                UserDefaults.standard.synchronize()
                                
                                print("Login Success", responseDict)
                                self.navigateDashBoard()
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                            else
                            {
                                AppUtility.alert(message: message ?? "", title: "", controller: self)
                            }
                        }
                        else
                        {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                        }
                        
                        
                    }
                      else
                      {
                     AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                    }
                    
                }
            }
        }
    }
    
}
