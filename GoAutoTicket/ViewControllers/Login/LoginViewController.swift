//
//  LoginViewController.swift
//  GoAutoTicket
//
//  Created by vamsi on 7/4/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import SwiftyJSON
import ObjectMapper
import JWT

@available(iOS 13.0, *)
class LoginViewController: UIViewController, CountryLeftViewDelegate, PhValidationProtocol,HelperFunctions {
   
    @IBOutlet var signinScrollView: UIScrollView!
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var signinLabel: UILabel!
    
    @IBOutlet var signinSubLabel: UILabel!
    
    @IBOutlet var signinButtonsView: UIView!
    @IBOutlet var signinSegment: UISegmentedControl!
    
    @IBOutlet var SigninTF: TxtAppTheme!
    
    @IBOutlet var passwordTF: TxtAppTheme!
    
//    @IBOutlet var SigninTF: NoPasteTextField!
    
//    @IBOutlet var passwordTF: NoPasteTextField!
    
    @IBOutlet var passwordviewButton: UIButton!
    
    @IBOutlet var signinButton: UIButton!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var forgotButton: UIButton!
    
    @IBOutlet var imgEmail : UIImageView!
    
    var countryView   : PaytoViews?
    var country       : Country?
    let validObj = PayToValidations.init()
    
    public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    
    var segmentStr = ""
    var parameters:[String : Any] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        
        signinButton.layer.cornerRadius = 4;
        signinButton.layer.masksToBounds = true;
         self.passwordTF.isSecureTextEntry = true
        segmentStr = "First"
               // Do any additional setup after loading the view.
               if segmentStr == "First"{
                   self.SigninTF.leftViewMode = .never
                   SigninTF.keyboardType = .emailAddress
                   SigninTF.text = ""
                   
               }
               else{
                   SigninTF.keyboardType = .phonePad
                   SigninTF.text = ""
               }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.environment()
        self.apicall()
    }
    
    func setUI(){
       //   dismissKey()
        //  headingLabel.setPropertiesForLabel()
         // SigninTF.setPropertiesForTextField()
          phNumValidationsFile = getDataFromJSONFile() ?? []
          self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
          countryView = PaytoViews.updateView()
          let countryCode = String(format: "(%@)", "+95")
          countryView?.wrapCountryViewData(img: "myanmarflag", str: countryCode)
          countryView?.delegate = self
          self.SigninTF.leftViewMode = .always
          self.SigninTF.leftView     = countryView
          
      }
    
    
//        func environment(){
//
//            let username = UserDefaults.standard.value(forKey: "username")
//            let password = UserDefaults.standard.value(forKey: "password")
//           // let loginString = String(format: "%@:%@", username as! CVarArg, password as! CVarArg)
//            let loginString = "\(username ?? ""):\(password ?? "")"
//            let loginData = loginString.data(using: String.Encoding.utf8)!
//            let base64LoginString = loginData.base64EncodedString()
//
//             let authString = "Basic \(base64LoginString)"
//            let headers = [
//              "Content-Type": "application/json; charset=utf-8"
//
//            ]
//
//
//            let parameters = [:] as AnyObject
//
//            let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
//
//            let request = NSMutableURLRequest(url: NSURL(string: "https://preprod-coreapi.goautoticket.com/api/v1/application/environment")! as URL,
//                                                    cachePolicy: .useProtocolCachePolicy,
//                                                timeoutInterval: 30.0)
//            request.httpMethod = "POST"
//            request.setValue(authString, forHTTPHeaderField: "Authorization")
//
//
//           // request.addValue(authString, forHTTPHeaderField: "Authorization")
//            request.allHTTPHeaderFields = headers
//            request.httpBody = postData! as Data
//
//            let session = URLSession.shared
//            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//              if (error != nil) {
//                print(error)
//              } else {
//    //            let httpResponse = response as? HTTPURLResponse
//    //            print(httpResponse)
//                let json = JSON(data ?? "")
//                let responseDic = json.dictionaryValue
//                let  response = responseDic["response"]
//                if responseDic.count>0{
//                    print("@@@@",response as Any)
//              }
//
//              }
//            })
//
//            dataTask.resume()
//        }
    
    
    func apicall(){
        AppUtility.showLoading(self.view)
                let apiReqObj = ApiRequestClass()
                apiReqObj.customDelegate = self
               
        let parameters = [:] as AnyObject
                
                  DispatchQueue.main.async {
                
                   apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/application/environment")!, requestData: parameters, requestType: RequestType.RequestTypeEnvironment, httpMethodName: "POST")
                }
    }
//    func httpResponse(responseObj: Any, reqType: RequestType)
//    {
//        DispatchQueue.main.async{
//            
//            AppUtility.hideLoading(self.view)
//            
//            if let respMsg = responseObj as? String
//            {
//                let responseAlert = UIAlertController(title: "Alert", message: respMsg, preferredStyle: UIAlertController.Style.alert)
//                responseAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                UIApplication.shared.keyWindow?.rootViewController?.present(responseAlert, animated: true, completion: nil)
//            }
//            else
//            {
//                
//                var respDict = [String:Any]()
//                if responseObj is [Any]
//                {
//                   // self.tokenarray = responseObj as! [Any]
//                }
//                else if responseObj is [String:Any]
//                {
//                    respDict = responseObj as! Dictionary<String,Any>
//                    
//                }
//                
//                if reqType == RequestType.RequestTypeEnvironment
//                {
//                    if respDict.count > 0
//                    {
//                    
//                     let itemDict:[String:Any] = respDict["response"] as!  Dictionary<String,Any>
//                       print( "#####",itemDict)
//                        
//                    }
//                    else
//                    {
//                        let alert = UIAlertController(title: "GAT", message: respDict["Message"] as? String, preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                    }
//                }
//            }
//        }
//    }
    
    func navigateDashBoard(){

        let storyBoard : UIStoryboard = UIStoryboard(name: "BusMain", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardNewVC") as? DashboardNewVC
        if let isValidObj = nextViewController{
            self.navigationController?.pushViewController(isValidObj, animated: true)
        }
    }
    
    
    @IBAction func signinsegmentClick(_ sender: Any) {
        if signinSegment.selectedSegmentIndex == 0{
              SigninTF.resignFirstResponder()
             segmentStr = "First"
             SigninTF.placeholder = "Email"
             self.SigninTF.leftViewMode = .never
             SigninTF.keyboardType = .emailAddress
             SigninTF.text = ""
             passwordTF.text = ""
             SigninTF.becomeFirstResponder()
             imgEmail.image = UIImage(named: "email")
         }
         else{
             SigninTF.resignFirstResponder()
             segmentStr = "Second"
             SigninTF.placeholder = ""
             SigninTF.keyboardType = .phonePad
             SigninTF.text = ""
             passwordTF.text = ""
             SigninTF.becomeFirstResponder()
             imgEmail.image = UIImage(named: "phone")
             setUI()
         }
        
    }
    @IBAction func passwordViewClick(_ sender: Any) {
        if (self.passwordTF.isSecureTextEntry == true) {
            self.passwordviewButton.setImage(UIImage(named: "eyeopen"), for: .normal)
               self.passwordTF.isSecureTextEntry = false
           }else{
               self.passwordviewButton.setImage(UIImage(named: "eyeclose"), for: .normal)
               self.passwordTF.isSecureTextEntry = true
           }
        
    }
    
    @IBAction func signinButtonClick(_ sender: Any) {
          let validate = Validation()
        if segmentStr == "First"{
            if SigninTF.text == ""{
                AppUtility.alert(message: "Please Enter Email".localized, title: "", controller: self)
            }
            else if !validate.isEmailString(SigninTF.text){
                AppUtility.alert(message: "Enter Valid Email".localized, title: "", controller: self)
            }
            else if passwordTF.text == ""{
                AppUtility.alert(message: "Please Enter Password".localized, title: "", controller: self)
            }
            else{
                // Api call
                parameters = ["Request": [
                  "age": "0",
                  "agentBalance": "0",
                  "agentID": "0",
                  "anniversaryDate": "Jan 1, 1 12:00:00 AM",
                  "birthDate": "Jan 1, 1 12:00:00 AM",
                  "canSendEmail": false,
                  "cancelWaiveOffPercentage": "0",
                  "contactInformation": ["email": SigninTF.text ?? ""],
                  "customerID": "0",
                  "discountChangePercentage": "0",
                  "entityID": "0",
                  "gender": "M",
                  "gmtTimeDifference": "0",
                  "isCoPAX": false,
                  "isEmployee": false,
                  "isHeadOffice": false,
                  "isNewsLetterSubscription": false,
                  "isPortalAdmin": false,
                  "isSelfSubscribed": false,
                  "loginName": SigninTF.text ?? "",
                  "openIDs": [:],
                  "passportExpirationDate": "Jan 1, 1 12:00:00 AM",
                  "password": passwordTF.text ?? "",
                  "ratePercentage": "0",
                  "seqNo": "0",
                  "userID": "0"
                ],
                 "flags": [:],
                 "info": [:]
                ]
                
                print("$$$$$",parameters)
                
                self.apirequestcall()
            }
        }
        else{
            if SigninTF.text == "09" || SigninTF.text == "" {
                AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
            }
           else if passwordTF.text == ""{
                AppUtility.alert(message: "Please Enter Password".localized, title: "", controller: self)
            }
            else{
                // Api call
                parameters = ["Request": [
                  "age": "0",
                  "agentBalance": "0",
                  "agentID": "0",
                  "anniversaryDate": "Jan 1, 1 12:00:00 AM",
                  "birthDate": "Jan 1, 1 12:00:00 AM",
                  "canSendEmail": false,
                  "cancelWaiveOffPercentage": "0",
                  "contactInformation": [
                    "phoneNumber": SigninTF.text ?? "",
                    "phoneNumberCountryCode": "95"
                    ],
                  "customerID": "0",
                  "discountChangePercentage": "0",
                  "entityID": "0",
                  "gender": "M",
                  "gmtTimeDifference": "0",
                  "isCoPAX": false,
                  "isEmployee": false,
                  "isHeadOffice": false,
                  "isNewsLetterSubscription": false,
                  "isPortalAdmin": false,
                  "isSelfSubscribed": false,
                  "loginName": SigninTF.text ?? "",
                  "openIDs": [:],
                  "passportExpirationDate": "Jan 1, 1 12:00:00 AM",
                  "password": passwordTF.text ?? "",
                  "ratePercentage": "0",
                  "seqNo": "0",
                  "userID": "0"
                ],
                 "flags": [:],
                 "info": [:]
                ]
                print("$$$$$",parameters)
                self.apirequestcall()
                
            }
            
        }
        
        
    }
    
    @IBAction func signupButtonClick(_ sender: Any) {
           var homeVC: RegistrationViewController?
               homeVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController
               if let homeVC = homeVC {
                   navigationController?.pushViewController(homeVC, animated: true)
               }
    }
    
    @IBAction func forgotButtonClick(_ sender: Any) {
        
        var homeVC: ForgotPassowrdViewController?
               homeVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "ForgotPassowrdViewController") as? ForgotPassowrdViewController
               if let homeVC = homeVC {
                   navigationController?.pushViewController(homeVC, animated: true)
               }
    }
    
    @IBAction func backbuttonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
