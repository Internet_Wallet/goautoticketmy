//
//  AuthenticateClientModel.swift
//  GATFlightModule
//
//  Created by iMac on 22/01/2020.
//  Copyright © 2020 E J ANTONY. All rights reserved.
//


//Response Fromat for this Model class
//{
//    "status": {
//        "code": 0,
//        "message": "Success"
//    },
//    "info": {
//        "id": "authenticate-client",
//        "token": "637152646832477240"
//    },
//    "response": {
//        "applicationID": "dbee2f5c580793ed8452121b16fc94d7721700b3eb6f8d3d91e6d853abc3216c",
//        "authToken": "aeccfa45d53ecd31cbceee160038c38f56c4f896df4854d3abddefa4946d0d64"
//    },
//    "flags": {}
//}


import Foundation
import SwiftyJSON

struct AuthenticateClientModel {
   
    var statusMessage: String?
    var infoToken: String?
    var authToken: String?
    var applicationID: String?
    
    public init(data: JSON){
        if data.exists(){
            let dictionary = data.dictionaryValue
            if dictionary.count>0{
                let statusDic = data["status"].dictionaryValue
                if statusDic.count>0{
                    
                    self.statusMessage = Utility.validateNull(Value: statusDic["message"]?.stringValue ?? "")
                }
                
                if self.statusMessage == HotelServerMessage.kSuccess{
                    let infoDic = data["info"].dictionaryValue
                    if infoDic.count>0{
                      self.infoToken = Utility.validateNull(Value: infoDic["token"]?.stringValue ?? "")
                    }
                    let responseDic = data["response"].dictionaryValue
                    if responseDic.count>0{
                      self.authToken = Utility.validateNull(Value: responseDic["authToken"]?.stringValue ?? "")
                      self.applicationID = Utility.validateNull(Value: responseDic["applicationID"]?.stringValue ?? "")
                    }
                }
            }
        }
    }
}
