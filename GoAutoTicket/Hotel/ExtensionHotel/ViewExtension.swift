//
//  ViewExtension.swift
//  LC-Travel
//
//  Created by Tushar Lama on 06/10/18.
//  Copyright © 2018 Tushar Lama. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func curveBorder(borderColor: UIColor){
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.borderWidth = 2.0
        self.clipsToBounds = true
        self.layer.borderColor = borderColor.cgColor
    }
    
    func setBorderPropertiesForView(color: UIColor){
        self.layer.cornerRadius = 10.0
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = 10.0
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
    }
    
    
    
}
