//
//  UIViewExtension.swift
//  LC-Travel
//
//  Created by Dhairya Uniyal on 06/10/18.
//  Copyright © 2018 Tushar Lama. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

class RoundUIView: UIView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}

extension UIView{
    
    func addCornerAndShadow(cornerRadius: CGFloat,shadowColor: UIColor){
                
        // corner radius
        self.layer.cornerRadius = cornerRadius

        // border
       // self.layer.borderWidth = 1.0
      //  self.layer.borderColor = UIColor.black.cgColor

        // shadow
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
    }

    
       func makeRounded() {
           let radius = self.frame.width/2.0
           self.layer.cornerRadius = radius
           self.layer.masksToBounds = true
       }
    
}

