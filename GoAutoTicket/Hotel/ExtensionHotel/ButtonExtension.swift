//
//  ButtonExtension.swift
//  LC-Travel
//
//  Created by Tushar Lama on 30/09/18.
//  Copyright © 2018 Tushar Lama. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
   
    func curveBorder(){
        self.layer.cornerRadius = 10.0
        self.layer.borderWidth = 2.0
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.clear.cgColor
    }
    func circleButton(){
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
    }
    
    
}
