//
//  TextFieldExtension.swift
//  LC-Travel
//
//  Created by Tushar Lama on 30/09/18.
//  Copyright © 2018 Tushar Lama. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    
    func curveBorder(color:UIColor){
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.borderWidth = 2.0
        self.clipsToBounds = true
        self.layer.borderColor = color.cgColor
    }
    
}
