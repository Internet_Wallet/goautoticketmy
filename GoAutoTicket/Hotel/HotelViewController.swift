//
//  HotelViewController.swift
//  GoAutoTicket
//
//  Created by Tushar Lama on 07/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift


protocol HotelSearchViewControllerDelegate : class {
    func HotelSelected(LocationName: String, status:String)
}

class HotelViewController: UIViewController, HotelSearchViewControllerDelegate {
    
    @IBOutlet weak var roomTableHeight: NSLayoutConstraint!
    @IBOutlet weak var checkOutView: UIView!
    @IBOutlet weak var hotelTableView: UITableView!
    var rowCount = 1
    var adultCount = 1
    var childCount = 0
    @IBOutlet weak var hotelTableHeight: NSLayoutConstraint!
    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBOutlet weak var checkInView: UIView!
   // let datePicker = UIDatePicker()
    @IBOutlet var checkinTF: UITextField!
    @IBOutlet var CheckoutTF: UITextField!
    
    @IBOutlet var checkinDayLabel: UILabel!
    @IBOutlet var checkinMonthLabel: UILabel!
    
    @IBOutlet var checkoutDayLabel: UILabel!
    @IBOutlet var checkoutMonthLabel: UILabel!
    
    @IBOutlet var guestroomView: UIView!
    @IBOutlet var guestttitleLabel: UILabel!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var adultCountLabel: UILabel!
    @IBOutlet var childCountLabel: UILabel!
    
    @IBOutlet var adultView: UIView!
    @IBOutlet var childView: UIView!
    @IBOutlet var guestView: UIView!
    
    @IBOutlet var guestViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var child1HeightConstraint: NSLayoutConstraint!
    @IBOutlet var child2HeightConstraint: NSLayoutConstraint!
    @IBOutlet var child3HeightConstraint: NSLayoutConstraint!
    @IBOutlet var child4HeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var child1TF: ACFloatingTextfield!
    @IBOutlet var child2TF: ACFloatingTextfield!
    @IBOutlet var child3TF: ACFloatingTextfield!
    @IBOutlet var child4TF: ACFloatingTextfield!
    
    var picker = UIDatePicker()
    
     var strTF = ""
    
    @IBOutlet var hotelLocationTF: NoPasteTextField!
    
    let dateFormatter = DateFormatter()
    let locale = NSLocale.current
    var datePicker : UIDatePicker!
    let toolBar = UIToolbar()
    
    @IBOutlet weak var roomCountLabel: UILabel!
    
    //var myArray:[[String : String]] = [[:]]
    var myArray:[(Room:String,Adult:String,Child:String)] = []
    
    var selectedIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Hotel"
        self.navigationController?.isNavigationBarHidden = false
        self.guestroomView.frame = self.view.frame
        self.view.addSubview(self.guestroomView)
        guestroomView.isHidden = true
        guestViewHeightConstraint.constant = 250
        child1HeightConstraint.constant = 0
        child2HeightConstraint.constant = 0
        child3HeightConstraint.constant = 0
        child4HeightConstraint.constant = 0
        
        child1TF.isHidden = true
        child2TF.isHidden = true
        child3TF.isHidden = true
        child4TF.isHidden = true
        myArray.append((Room: "\(rowCount)", Adult: "\(adultCount)", Child: "\(childCount)"))
        self.checkinTF.tintColor = UIColor.clear
        self.CheckoutTF.tintColor = UIColor.clear
        setUI()
        print(roomTableHeight.constant)
    }
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = false
    }
    
    func HotelSelected(LocationName: String, status: String) {
         print(LocationName,status)
    
       //  leavingFromTxtField.text = LocationName
        hotelLocationTF.text = LocationName
     }
    
    @objc func updateTextFieldForDOB(_ sender: UIDatePicker,textField : UITextField) {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd"
         //  birthDateField.text = dateFormatter.string(from: sender.date)
          
       }
    
    @IBAction func onClickSearchButton(_ sender: Any) {
        if #available(iOS 13.0, *) {
            if let isValidObj = self.storyboard?.instantiateViewController(identifier: "HotelLocationListController") as? HotelLocationListController{
                //  self.present(isValidObj, animated: false, completion: nil)
                self.navigationController?.pushViewController(isValidObj, animated: true)
            }
        }
        else {
            if let isValidObj = self.storyboard?.instantiateViewController(withIdentifier: "HotelLocationListController") as? HotelLocationListController {
                self.navigationController?.pushViewController(isValidObj, animated: true)
            }
            // Fallback on earlier versions
        }
        
    }
    
    @IBAction func onClickIncreaseRoomCount(_ sender: Any) {
        if rowCount < 4{
             rowCount = rowCount + 1
            roomTableHeight.constant = CGFloat(60 * rowCount)
            myArray.append((Room: "\(rowCount)", Adult: "1", Child: "0"))
        }
        roomCountLabel.text = "\(rowCount)"
        hotelTableView.reloadData()
    }
    
    @IBAction func onClickDecreaseRoomCount(_ sender: Any) {
       
        if rowCount > 1{
            rowCount = rowCount - 1
            roomTableHeight.constant = roomTableHeight.constant - 60
           myArray.append((Room: "\(rowCount)", Adult: "\(adultCount)", Child: "\(childCount)"))
            
        }else{
            
        }
         roomCountLabel.text = "\(rowCount)"
        hotelTableView.reloadData()
    }
    
    @IBAction func onClickIncreaseAdultCount(_ sender: Any) {
        let adultnum =  Int(myArray[selectedIndex].Adult) ?? 0
        adultCount = adultnum + 1
        if adultCount <= 4{
            myArray[selectedIndex] = ((Room: "\(myArray[selectedIndex].Room)", Adult: "\(adultCount)", Child: "\(myArray[selectedIndex].Child)"))
        }
        adultCountLabel.text = "\(myArray[selectedIndex].Adult)"
    }
    @IBAction func onClickDecreaseAdultCount(_ sender: Any) {
        let adultnum =  Int(myArray[selectedIndex].Adult) ?? 0
        adultCount = adultnum - 1
        if adultCount >= 1{
            myArray[selectedIndex] = ((Room: "\(myArray[selectedIndex].Room)", Adult: "\(adultCount)", Child: "\(myArray[selectedIndex].Child)"))
        }else{
            
        }
        adultCountLabel.text = "\(myArray[selectedIndex].Adult)"

    }
    @IBAction func onClickIncreaseChildCount(_ sender: Any) {
        
        let childnum =  Int(myArray[selectedIndex].Child) ?? 0
        childCount = childnum + 1
        if childCount <= 4{
    
            if childCount == 1{
                child1TF.isHidden = false
                child1HeightConstraint.constant = 45
                guestViewHeightConstraint.constant = 350
            }
            else if childCount == 2{
                child1TF.isHidden = false
                child1HeightConstraint.constant = 45
                child2TF.isHidden = false
                child2HeightConstraint.constant = 45
                guestViewHeightConstraint.constant = 350
            }
            else if childCount == 3{
                child1TF.isHidden = false
                child1HeightConstraint.constant = 45
                child2TF.isHidden = false
                child2HeightConstraint.constant = 45
                child3TF.isHidden = false
                child3HeightConstraint.constant = 45
                guestViewHeightConstraint.constant = 450
            }
            else if childCount == 4{
                child1TF.isHidden = false
                child1HeightConstraint.constant = 45
                child2TF.isHidden = false
                child2HeightConstraint.constant = 45
                child3TF.isHidden = false
                child3HeightConstraint.constant = 45
                child4TF.isHidden = false
                child4HeightConstraint.constant = 45
                guestViewHeightConstraint.constant = 450
            }
            myArray[selectedIndex] = ((Room: "\(myArray[selectedIndex].Room)", Adult: "\(myArray[selectedIndex].Adult)", Child: "\(childCount)"))
        }
        childCountLabel.text = "\(myArray[selectedIndex].Child)"
    }
    @IBAction func onClickDecreaseChildCount(_ sender: Any) {
        let childnum =  Int(myArray[selectedIndex].Child) ?? 0
        childCount = childnum - 1
        if childCount >= 0{
            if childCount == 1{
                child1TF.isHidden = false
                child1HeightConstraint.constant = 45
                child2TF.isHidden = true
                child2HeightConstraint.constant = 0
                child3TF.isHidden = true
                child3HeightConstraint.constant = 0
                child4TF.isHidden = true
                child4HeightConstraint.constant = 0
                guestViewHeightConstraint.constant = 350
            }
            else if childCount == 2{
                child1TF.isHidden = false
                child1HeightConstraint.constant = 45
                child2TF.isHidden = false
                child2HeightConstraint.constant = 45
                child3TF.isHidden = true
                child3HeightConstraint.constant = 0
                child4TF.isHidden = true
                child4HeightConstraint.constant = 0
                guestViewHeightConstraint.constant = 350
            }
            else if childCount == 3{
                child1TF.isHidden = false
                child1HeightConstraint.constant = 45
                child2TF.isHidden = false
                child2HeightConstraint.constant = 45
                child3TF.isHidden = false
                child3HeightConstraint.constant = 45
                child4TF.isHidden = true
                child4HeightConstraint.constant = 0
                guestViewHeightConstraint.constant = 450
            }
           else{
                child1TF.isHidden = true
                child1HeightConstraint.constant = 0
                child2TF.isHidden = true
                child2HeightConstraint.constant = 0
                child3TF.isHidden = true
                child3HeightConstraint.constant = 0
                child4TF.isHidden = true
                child4HeightConstraint.constant = 0
                guestViewHeightConstraint.constant = 250
            }
           myArray[selectedIndex] = ((Room: "\(myArray[selectedIndex].Room)", Adult: "\(myArray[selectedIndex].Adult)", Child: "\(childCount)"))
            
        }else{
            
        }
        childCountLabel.text = "\(myArray[selectedIndex].Child)"
       
    }
    
    @IBAction func doneButtonClick(_ sender: Any) {
        self.HideguestroomView()
        hotelTableView.reloadData()
    }
    
    func showguestroomView(){
        
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseIn,
                       animations: {

                 self.childFieldsMange()
                self.guestroomView.isHidden = false
        })
        
        
    }
    
    func childFieldsMange(){
        if self.childCountLabel.text == "1"{
             self.child1TF.isHidden = false
             self.child1HeightConstraint.constant = 45
             self.child2TF.isHidden = true
             self.child2HeightConstraint.constant = 0
             self.child3TF.isHidden = true
             self.child3HeightConstraint.constant = 0
             self.child4TF.isHidden = true
             self.child4HeightConstraint.constant = 0
             self.guestViewHeightConstraint.constant = 350
         }
         else if self.childCountLabel.text == "2"{
             self.child1TF.isHidden = false
             self.child1HeightConstraint.constant = 45
             self.child2TF.isHidden = false
             self.child2HeightConstraint.constant = 45
             self.child3TF.isHidden = true
             self.child3HeightConstraint.constant = 0
             self.child4TF.isHidden = true
             self.child4HeightConstraint.constant = 0
             self.guestViewHeightConstraint.constant = 350
         }
         else if self.childCountLabel.text == "3"{
             self.child1TF.isHidden = false
             self.child1HeightConstraint.constant = 45
             self.child2TF.isHidden = false
             self.child2HeightConstraint.constant = 45
             self.child3TF.isHidden = false
             self.child3HeightConstraint.constant = 45
             self.child4TF.isHidden = true
             self.child4HeightConstraint.constant = 0
             self.guestViewHeightConstraint.constant = 450
         }
        else if self.childCountLabel.text == "4"{
            self.child1TF.isHidden = false
            self.child1HeightConstraint.constant = 45
            self.child2TF.isHidden = false
            self.child2HeightConstraint.constant = 45
            self.child3TF.isHidden = false
            self.child3HeightConstraint.constant = 45
            self.child4TF.isHidden = false
            self.child4HeightConstraint.constant = 45
            self.guestViewHeightConstraint.constant = 450
        }
        else{
            self.child1TF.isHidden = true
            self.child1HeightConstraint.constant = 0
            self.child2TF.isHidden = true
            self.child2HeightConstraint.constant = 0
            self.child3TF.isHidden = true
            self.child3HeightConstraint.constant = 0
            self.child4TF.isHidden = true
            self.child4HeightConstraint.constant = 0
            self.guestViewHeightConstraint.constant = 250
        }
    }
    
    
    func HideguestroomView(){
         
         
         UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseIn,
                        animations: {
                 self.guestroomView.isHidden = true
         })
         
         
     }
    
}

extension HotelViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == hotelLocationTF{
            guard let homeVC = UIStoryboard(name: "HotelMain", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: HotelLocationListController.self)) as? HotelLocationListController else { return }
                    //  homeVC.leavingstr = "Leaving"
                        homeVC.applybuttondelegate = self
                      navigationController?.pushViewController(homeVC, animated: true)
                      textField.resignFirstResponder()
        }
        if textField == checkinTF{
            strTF = "1"
            picker = UIDatePicker(frame: CGRect(x: 0, y: 210, width: 320, height: 200))
            
            picker.date = Date(timeIntervalSinceNow: TimeInterval(2))
            picker.minimumDate = Date(timeIntervalSinceNow: TimeInterval(0))
            
            picker.datePickerMode = .date
            picker.backgroundColor = UIColor.white
            picker.addTarget(self, action: #selector(updateTextField(_:)), for: .valueChanged)
            
            checkinTF.inputView = picker
            
            let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            numberToolbar.barStyle = .default
            numberToolbar.tintColor = UIColor.gray
            numberToolbar.items = [UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)), UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneWithNumberPad))]
            
            numberToolbar.sizeToFit()
            checkinTF.inputAccessoryView = numberToolbar
        }
        else{
            strTF = "2"
            picker = UIDatePicker(frame: CGRect(x: 0, y: 210, width: 320, height: 200))
            
            picker.date = Date(timeIntervalSinceNow: TimeInterval(2))
            picker.minimumDate = Date(timeIntervalSinceNow: TimeInterval(0))
            
            picker.datePickerMode = .date
            picker.backgroundColor = UIColor.white
            picker.addTarget(self, action: #selector(updateTextField(_:)), for: .valueChanged)
            
            CheckoutTF.inputView = picker
            
            let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            numberToolbar.barStyle = .default
            numberToolbar.tintColor = UIColor.gray
            numberToolbar.items = [UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)), UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneWithNumberPad))]
            
            numberToolbar.sizeToFit()
            CheckoutTF.inputAccessoryView = numberToolbar
        }

    }
}


extension HotelViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  return rowCount
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelTableViewCell") as? HotelTableViewCell else  {
            return UITableViewCell()
        }
        cell.layer.cornerRadius = 5.0
        cell.selectionStyle = .none
        
        cell.roomLabel.text =  self.myArray[indexPath.row].Room
        cell.adultCountLabel.text = self.myArray[indexPath.row].Adult
        cell.childCountLabel.text = self.myArray[indexPath.row].Child
        
      //  cell.roomLabel.text = "\(indexPath.row + 1)"
    
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         selectedIndex = indexPath.row
         adultCountLabel.text = "\(myArray[selectedIndex].Adult)"
         childCountLabel.text = "\(myArray[selectedIndex].Child)"
        if indexPath.row == 0 {
            println_debug("1")
            self.guestttitleLabel.text = "Guest in Room 1"
            self.showguestroomView()
        }
        else if indexPath.row == 1{
             println_debug("2")
             self.guestttitleLabel.text = "Guest in Room 2"
             self.showguestroomView()
            
        }
        else if indexPath.row == 2{
             println_debug("3")
             self.guestttitleLabel.text = "Guest in Room 3"
             self.showguestroomView()
        }
        else {
             println_debug("4")
             self.guestttitleLabel.text = "Guest in Room 4"
             self.showguestroomView()
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
}

extension HotelViewController{
    
    fileprivate func setUI(){
        hotelTableHeight.constant = 60.0
        hotelTableView.layer.cornerRadius = 5.0
        searchButtonOutlet.layer.cornerRadius = 5.0
        let colorTop =  UIColor(red: 255.0/255.0, green: 197.0/255.0, blue: 191.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 129.0/255.0, green: 20.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at:0)
        hotelTableView.delegate = self
        hotelTableView.dataSource = self
        hotelTableView.reloadData()
        roomCountLabel.text = "1"
        adultCountLabel.text = "1"
        childCountLabel.text = "0"
        
        let gestureCheckIN = UITapGestureRecognizer(target: self, action:  #selector(self.checkActionCheckIn))
        self.checkInView.addGestureRecognizer(gestureCheckIN)
        
        let gestureCheckOut = UITapGestureRecognizer(target: self, action:  #selector(self.checkActionCheckOut))
        self.checkOutView.addGestureRecognizer(gestureCheckOut)
        
    }
    
    @objc func checkActionCheckIn(sender : UITapGestureRecognizer) {
        // Do what you want
        //self.check
        
        print("vamsiiiii")
       
        
    }
    
    @objc func checkActionCheckOut(sender : UITapGestureRecognizer) {
        // Do what you want
       print("mohannnnnn")
      
    }
    
//    func doDatePicker(){
//        // DatePicker
//        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: self.view.frame.size.height  - 150, width: self.view.frame.size.width, height: 150))
//        self.datePicker?.backgroundColor = UIColor.white
//        self.datePicker?.datePickerMode = UIDatePicker.Mode.date
//        self.datePicker?.minimumDate = Date()
//       // datePicker.center = view.center
//        view.addSubview(self.datePicker)
//
//        // ToolBar
//
//        toolBar.frame = CGRect(x: 0, y: datePicker.frame.origin.y - 10 , width: self.view.frame.size.width, height: 40)
//        toolBar.barStyle = .default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
//        toolBar.sizeToFit()
//
//        // Adding Button ToolBar
//        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
//        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
//        toolBar.isUserInteractionEnabled = true
//        self.view.addSubview(toolBar)
//        self.toolBar.isHidden = false
//    }
    
    @objc func updateTextField(_ sender: Any?) {
        //    _Picker1 = (UIDatePicker*)self.dateOfBirthTF.inputView;
        //    self.dateOfBirthTF.text = [NSString stringWithFormat:@"%@",_Picker1.date];

        
        if strTF == "1"
        {
            let df = DateFormatter()
            // df.dateStyle = NSDateIntervalFormatterMediumStyle;

            df.dateFormat = "yyyy-MM-dd"
          //  checkinTF.text = "\(df.string(from: picker.date))"
            
            let currentDate = dateFormatter.date(from: "\(df.string(from: picker.date))")
            dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
            let finalDate = dateFormatter.string(from: currentDate ?? picker.date)
            println_debug(finalDate)
            
            let fullNameArr = finalDate.components(separatedBy: " ")

            let firstName: String = fullNameArr[0]
            let lastName: String = fullNameArr[1]
            
            let fordate = lastName.components(separatedBy: "-")
            
            let exactdate : String = fordate[0]
            let exactmonth: String = fordate[1]
            
            checkinDayLabel.text = NSString(string: (firstName.replacingOccurrences(of: ",", with: ""))) as String
            checkinTF.text = exactdate
            checkinMonthLabel.text = exactmonth
        }
        else
        {
            let df = DateFormatter()
            // df.dateStyle = NSDateIntervalFormatterMediumStyle;

            df.dateFormat = "dd/MM/yyyy"
           // CheckoutTF.text = "\(df.string(from: picker.date))"
            let currentDate = dateFormatter.date(from: "\(df.string(from: picker.date))")
            dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
            let finalDate = dateFormatter.string(from: currentDate ?? picker.date)
            println_debug(finalDate)
            
            let fullNameArr = finalDate.components(separatedBy: " ")

            let firstName: String = fullNameArr[0]
            let lastName: String = fullNameArr[1]
            
            let fordate = lastName.components(separatedBy: "-")
            
            let exactdate : String = fordate[0]
            let exactmonth: String = fordate[1]
            
            checkoutDayLabel.text = NSString(string: (firstName.replacingOccurrences(of: ",", with: ""))) as String
            
            CheckoutTF.text = exactdate
            checkoutMonthLabel.text = exactmonth
            
        }
        
    }

    @objc func cancelNumberPad() {
         
         if strTF == "1"
         {
             let bool1 = checkinTF.resignFirstResponder()
             if bool1 {
                  let df = DateFormatter()
                 // df.dateStyle = NSDateIntervalFormatterMediumStyle;

                 df.dateFormat = "dd/MM/yyyy"
                // CheckoutTF.text = "\(df.string(from: picker.date))"
                 let currentDate = dateFormatter.date(from: "\(df.string(from: picker.date))")
                 dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
                 let finalDate = dateFormatter.string(from: currentDate ?? Date())
                 println_debug(finalDate)
                let fullNameArr = finalDate.components(separatedBy: " ")
                
                let firstName: String = fullNameArr[0]
                let lastName: String = fullNameArr[1]
                
                let fordate = lastName.components(separatedBy: "-")
                
                let exactdate : String = fordate[0]
                let exactmonth: String = fordate[1]
                
                checkinDayLabel.text = NSString(string: (firstName.replacingOccurrences(of: ",", with: ""))) as String
                
                checkinTF.text = exactdate
                checkinMonthLabel.text = exactmonth
             }
         }
         else
         {
             
            let bool1 =  CheckoutTF.resignFirstResponder()
             if bool1 {
                let df = DateFormatter()
                // df.dateStyle = NSDateIntervalFormatterMediumStyle;
                
                df.dateFormat = "dd/MM/yyyy"
                // CheckoutTF.text = "\(df.string(from: picker.date))"
                let currentDate = dateFormatter.date(from: "\(df.string(from: picker.date))")
                dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
                let finalDate = dateFormatter.string(from: currentDate ?? Date())
                println_debug(finalDate)
                
                let fullNameArr = finalDate.components(separatedBy: " ")
                
                let firstName: String = fullNameArr[0]
                let lastName: String = fullNameArr[1]
                
                let fordate = lastName.components(separatedBy: "-")
                
                let exactdate : String = fordate[0]
                let exactmonth: String = fordate[1]
                
                checkoutDayLabel.text = NSString(string: (firstName.replacingOccurrences(of: ",", with: ""))) as String
                
                CheckoutTF.text = exactdate
                checkoutMonthLabel.text = exactmonth
            }
             
         }
         
         
     }

     @objc func doneWithNumberPad() {

         if strTF == "1"
         {
             let bool1 = checkinTF.resignFirstResponder()
             
             print(bool1)
         }
         else
         {
             let bool1 =  CheckoutTF.resignFirstResponder()
              print(bool1)
         }

     }
    
}

extension HotelViewController : ApiRequestProtocol {
func apirequesthotelsearchcall(){
        //   AppUtility.showLoading(self.view)
           let apiReqObj = ApiRequestClass()
           apiReqObj.customDelegate = self
           DispatchQueue.main.async {
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/hotel/search")!, requestData:"", requestType: RequestType.RequestHotelSearch, httpMethodName: "POST")
           }
       }
       
       func httpResponse(responseObj: Any, reqType: RequestType)
       {
           DispatchQueue.main.async{
               
               AppUtility.hideLoading(self.view)
               
               if let respMsg = responseObj as? String
               {
                   AppUtility.alert(message: respMsg, title: "", controller: self)
               }
               else
               {
                   
                var respDict = [String:Any]()
                if let resposneDataFromServer = responseObj as? [String : Any] {
                    respDict = resposneDataFromServer
                }
                
                   if reqType == RequestType.RequestTypeEnvironment
                   {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                  
                                
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }

                       else
                       {
                          AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                   }
                   else if reqType == RequestType.RequestHotelSearch {
                       if respDict.count > 0
                       {
                           if let itemDict:[String:Any] = respDict["status"] as? [String : Any] {
                          //  if  let innerdict:[String:Any] = itemDict["status"] as? [String : Any]{
                                let code = itemDict["code"] as? Int
                                let message = itemDict["message"] as? String
                                print("#####",code as Any)
                                if let isValidCode = code {
                                    if isValidCode == 0 {
                                        AppUtility.alert(message: message ?? "", title: "", controller: self)
                                    }
                                    else
                                    {
                                        AppUtility.alert(message: message ?? "", title: "", controller: self)
                                    }
                                }
                                else
                                {
                                    AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                                }
                          //  }
                          }
                        }
                       else
                       {
                        AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                       }
                       else
                       {
                           AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                       
                   }
               }
           }
       }
