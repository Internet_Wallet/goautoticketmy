//
//  HotelRoomDetail.swift
//  GoHotelTicket
//
//  Created by iMac on 03/01/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class HotelRoomDetail: UIViewController {

    @IBOutlet var footerView: UIView!
    var collectionArray = ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg"]
    var facilityArray = ["NO Large Pets Allowed over 5kg","NO Small Pets Allowed over 5kg","NO Garage","No WheelChair - accessible","NO Smoking Rooms", "NO Disability- friendly bathroom"]
    var isFacilityOn = false
    
    @IBOutlet weak var hotelRoomTableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    @IBAction func onClickSegmented(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1{
            collectionView.isHidden = false
            hotelRoomTableView.isHidden = true
        } else if sender.selectedSegmentIndex == 2{
            collectionView.isHidden = true
            hotelRoomTableView.isHidden = false
            isFacilityOn = true
            hotelRoomTableView.reloadData()
        }else{
            collectionView.isHidden = true
            hotelRoomTableView.isHidden = false
            isFacilityOn = false
            hotelRoomTableView.reloadData()
        }
    }
    
}

extension HotelRoomDetail: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFacilityOn{
            return 1
        }else{
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFacilityOn{
            return facilityArray.count
        }else{
            switch  section {
            case 1,2,3:
                return 1
            case 4:
                return 5
            default:
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFacilityOn{
            return 70.0
        }else{
            switch indexPath.section {
            case 1:
                return 299.0
                
            case 2:
                return 152.0
            case 3:
               return 40.0
            case 4:
                return 201.0
            default:
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isFacilityOn{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelDetailFacilityCell) as? HotelDetailFacilityCell else{
                return UITableViewCell()
            }
            cell.facilityLabel.text = facilityArray[indexPath.row]
            return cell
        }else{
            switch indexPath.section {
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelDetailCellRoom) as? HotelDetailCellRoom else{
                    return UITableViewCell()
                }
                return cell
            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelAboutHotelCell) as? HotelAboutHotelCell else{
                    return UITableViewCell()
                }
                return cell
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelRoomDetailCell) as? HotelRoomDetailCell else{
                    return UITableViewCell()
                }
                return cell
            case 4:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelDetailRoomCell) as? HotelDetailRoomCell else{
                    return UITableViewCell()
                }
                return cell
            default:
                return UITableViewCell()
            }
        }
    }
    
}


extension HotelRoomDetail: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotelListCollectionViewCell", for: indexPath) as? HotelListCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.hotelImageImageView.image =  UIImage(named: collectionArray[indexPath.row])
        
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: collectionView.bounds.width/3, height: collectionView.bounds.height/2)
//    }
    
    
}

extension HotelRoomDetail{
    fileprivate func setUI(){
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.title = "Hotel"
        collectionView.isHidden = true
        hotelRoomTableView.delegate = self
        hotelRoomTableView.dataSource = self
        hotelRoomTableView.separatorColor = .clear
        hotelRoomTableView.reloadData()
    }
}
