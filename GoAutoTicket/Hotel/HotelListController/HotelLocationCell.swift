//
//  HotelLocationCell.swift
//  GoHotelTicket
//
//  Created by iMac on 23/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class HotelLocationCell: UITableViewCell {

    @IBOutlet weak var hotelNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
