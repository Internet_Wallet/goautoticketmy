//
//  HotelLocationListController.swift
//  GoHotelTicket
//
//  Created by iMac on 23/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class HotelLocationListController: UIViewController {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var hotelSearchBar: UISearchBar!
    @IBOutlet weak var hotelLocationTableView: UITableView!
    
       var leavingstr: String = ""
       var goStr : String = ""
       var locationLat : String = ""
       var locationLong : String = ""
       
       var  hotellocationArray = [String]()
       var  mainhotellocationArray = [[String : Any]]()
       
       var locationSectionTitles = [String]()
       
       var hotelsearchparams:[String : Any] = [:]
       
       weak var applybuttondelegate : HotelSearchViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        self.title = "Hotel"
        
        self.navigationController?.navigationBar.isHidden = true
        self.location()
        hotelSearchBar.delegate = self
        hotelSearchBar.showsCancelButton = false
        if #available(iOS 13.0, *) {
            hotelSearchBar.searchTextField.textColor = .white
        } else {
            // Fallback on earlier versions
        }
        
      //  hotelLocationTableView.separatorColor = .clear
      //  hotelLocationTableView.layer.cornerRadius = 10.0
     //   hotelLocationTableView.layer.masksToBounds = true
        let colorTop =  UIColor(red: 255.0/255.0, green: 197.0/255.0, blue: 191.0/255.0, alpha: 1.0).cgColor
               let colorBottom = UIColor(red: 129.0/255.0, green: 20.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
               let gradientLayer = CAGradientLayer()
               gradientLayer.colors = [colorTop, colorBottom]
               gradientLayer.locations = [0.0, 1.0]
               gradientLayer.frame = self.view.bounds
               self.view.layer.insertSublayer(gradientLayer, at:0)
     //   hotelLocationTableView.delegate = self
     //   hotelLocationTableView.dataSource = self
     //   hotelLocationTableView.reloadData()
       
        
    }
    
    func locationparamsapi(){
        
        var locationparams:[String : Any] = [:]
        
        locationparams = [
            "Location": [
                "Latitude": locationLat,
                "Longitude": locationLong
            ],
            "Request": ""
        ]
        print("Location Params", locationparams)
        self.apirequestHotelsearchcall(parameter:locationparams)
    }
    
    func location(){
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
            VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
                        //                        if let street = addressDic["street"]  {
                        //                            self.township = street
                        //                        }
                        //                        if let region = addressDic["region"]  {
                        //                            self.devision = region
                        //                        }
                        //                        self.googleAddress = "\(addressDic)"
                        
                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                        self.locationparamsapi()
                    }
                    
                }
            }
        }
        else{
            // showLocationDisabledpopUp()
        }
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
       self.navigationController?.popViewController(animated: false)
    }
    
}
extension HotelLocationListController : UISearchBarDelegate{
    // Search Bar Delegate
     
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
      {
        //  UnApprovedvendorListArray = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
          var tempArray = [String]()
          if searchText.count == 0
          {
              hotelLocationTableView.reloadData()
              return
          }
          if searchText.count >= 3{
            hotelsearchparams = [

            "Location": [
                "Latitude": locationLat,
                "Longitude": locationLong
            ],
            "Request": searchText
            ]
            self.apirequestHotelsearchcall(parameter: hotelsearchparams)
          }
          for i in 0..<mainhotellocationArray.count
          {
            let vendorDict:[[String:Any]] = [mainhotellocationArray[i]]
             let vendorId: String = String(describing: vendorDict)
             if substring(searchText, existsIn: vendorId)
              {
                  tempArray.append(vendorId)
              }
          }
          hotellocationArray = tempArray
        //  hotelLocationTableView.reloadData()
      }
      
      func substring(_ substr: String, existsIn str: String) -> Bool
      {
          if str.range(of: substr) != nil
          {
              return true
          }
          return false
      }
      
      func searchBarTextDidBeginEditing(_ templeSearchBar: UISearchBar)
      {
          hotelSearchBar.setShowsCancelButton(true, animated: true)
          hotelLocationTableView.allowsSelection = false
          hotelLocationTableView.isScrollEnabled = true
          var searchBarTextField: UITextField? = nil
          for mainview: UIView in hotelSearchBar.subviews {
              for subview: UIView in mainview.subviews {
                  if (subview is UITextField) {
                      searchBarTextField?.backgroundColor = UIColor.clear
                      searchBarTextField = (subview as? UITextField)
                      break
                  }
              }
          }
          searchBarTextField?.enablesReturnKeyAutomatically = false
      }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
         hotelLocationTableView.allowsSelection = true
    }
      
      func updateSearchResults(for searchController: UISearchController)
         {
             let searchText: String? = hotelSearchBar.text
             if searchText?.count == 0
             {
                // airportname = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
                 hotelLocationTableView.reloadData()
                 return
             }
         }
      
      func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
      {
          hotelSearchBar.showsCancelButton = false
          hotelSearchBar.resignFirstResponder()
       //   flightSearchBar = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
          hotelLocationTableView.reloadData()
      }
      
      func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
      {
          hotelSearchBar.showsCancelButton = false
          hotelSearchBar.resignFirstResponder()
          // Do the search...
      }
    
}


extension HotelLocationListController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.mainhotellocationArray.count > 0{
           return self.mainhotellocationArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                return self.mainhotellocationArray[section]["type"] as? String
            }
        case 1:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                 return self.mainhotellocationArray[section]["type"] as? String
            }
        case 2:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                return self.mainhotellocationArray[section]["type"] as? String
            }
        case 3:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                return self.mainhotellocationArray[section]["type"] as? String
            }
        case 4:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                return self.mainhotellocationArray[section]["type"] as? String
            }
        default:
            return nil // when return nil no header will be shown
        }
        return nil
      
        
       
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if let val = mainhotellocationArray[section]["item"] {
            return (val as AnyObject).count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        guard let headerView = view as? UITableViewHeaderFooterView else { return }
        headerView.textLabel?.textColor = .red
        headerView.backgroundView?.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // var cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BusLocationSearchTableViewCell
         var cell:HotelLocationCell? = self.hotelLocationTableView.dequeueReusableCell(withIdentifier: "Cell") as? HotelLocationCell
        if cell == nil {
           cell = HotelLocationCell(style: .default, reuseIdentifier: "Cell")
        }
  
         if let val = self.mainhotellocationArray[indexPath.section]["item"] {
            let itemDict:[[String:Any]] = val as!  [[String:Any]]
            cell?.hotelNameLabel.text = itemDict[indexPath.row]["name"] as? String
        }
            
        cell?.selectionStyle = .none
        
         self.hotelLocationTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:HotelLocationCell? = self.hotelLocationTableView.dequeueReusableCell(withIdentifier: "Cell") as? HotelLocationCell
         let itemDict:[[String:Any]]
        if let val = self.mainhotellocationArray[indexPath.section]["item"] {
            itemDict = val as!  [[String:Any]]
            cell?.hotelNameLabel.text = itemDict[indexPath.row]["name"] as? String
        }
    
        self.applybuttondelegate?.HotelSelected(LocationName:  cell?.hotelNameLabel.text ?? "", status: "Leaving")
    
        self.navigationController?.popViewController(animated: false)
    }
}
extension HotelLocationListController: ApiRequestProtocol{
    
    
    func apirequestHotelsearchcall(parameter:Any){
        //   AppUtility.showLoading(self.view)
           let apiReqObj = ApiRequestClass()
           apiReqObj.customDelegate = self
           DispatchQueue.main.async {
               print("****",parameter)
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/hotel/search/location")!, requestData:parameter, requestType: RequestType.RequestHotelLocationSearch, httpMethodName: "POST")
           }
       }
       
       func httpResponse(responseObj: Any, reqType: RequestType)
       {
           DispatchQueue.main.async{
               
               AppUtility.hideLoading(self.view)
               
               if let respMsg = responseObj as? String
               {
                   AppUtility.alert(message: respMsg, title: "", controller: self)
               }
               else
               {
                   
                   var respDict = [String:Any]()
                   if responseObj is [Any]
                   {
                      // self.tokenarray = responseObj as! [Any]
                   }
                   else if responseObj is [String:Any]
                   {
                       respDict = responseObj as! Dictionary<String,Any>
                       
                   }
                   
                   if reqType == RequestType.RequestTypeEnvironment
                   {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                  
                                
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }

                       else
                       {
                          AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                   }
                   else if reqType == RequestType.RequestHotelLocationSearch {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                self.mainhotellocationArray.removeAll()
                                self.mainhotellocationArray = (respDict["response"] as? [[String : Any]])!
//                                for i in 0..<arrItem.count
//                                  {
//
//                                    if let sectiontitle = arrItem[i]["type"]{
//                                       println_debug(sectiontitle)
//                                        self.locationSectionTitles.append(sectiontitle as! String )
//                                        if let val = arrItem[i]["item"] {
//                                            let itemDict:[[String:Any]] = val as!  [[String:Any]]
//                                            self.mainbuslocationArray.append(contentsOf: itemDict)
//                                        }
//                                    }
//
//                                }
//
                               
                                print(self.mainhotellocationArray)
                                self.hotelLocationTableView.reloadData()
                                
                                // AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }
                       else
                       {
                           AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                       
                   }
                   else{
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                   }
               }
           }
       }
    
}


