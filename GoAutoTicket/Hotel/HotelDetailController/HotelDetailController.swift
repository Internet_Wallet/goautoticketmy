//
//  HotelDetailController.swift
//  GoHotelTicket
//
//  Created by iMac on 23/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//



import UIKit

class HotelDetailController: UIViewController {

    @IBOutlet weak var segmentedBar: UISegmentedControl!
    @IBOutlet weak var hotelDetailTableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var collectionArray = ["Hotel Name","Rate","Star","Promotion"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
}


extension HotelDetailController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "HotelDetailCell") as? HotelDetailCell else {
            return UITableViewCell()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if #available(iOS 13.0, *) {
            if let isValidObj = self.storyboard?.instantiateViewController(identifier: "HotelRoomDetail") as? HotelRoomDetail{
                self.navigationController?.pushViewController(isValidObj, animated: true)
            }
        } else {
             if let isValidObj = self.storyboard?.instantiateViewController(withIdentifier: "HotelRoomDetail") as? HotelRoomDetail{
                           self.navigationController?.pushViewController(isValidObj, animated: true)
                       }
        }
    }
}



//extension HotelDetailController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return collectionArray.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotelListCollectionViewCell", for: indexPath) as? HotelListCollectionViewCell else{
//            return UICollectionViewCell()
//        }
//        cell.infoLabel.text = collectionArray[indexPath.row]
//        cell.infoLabel.textAlignment = .center
//        
//        if indexPath.row == 0 {
//            cell.topArrowImageView.isHidden = false
//        }else{
//           cell.topArrowImageView.isHidden = true
//        }
//        
//        return cell
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: collectionView.bounds.width/4.5, height: collectionView.bounds.height)
//    }
//    
//    
//    
//}

extension HotelDetailController{
    
    fileprivate func setUI(){
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.title = "Hotel"
        let font = UIFont.systemFont(ofSize: 12)
        segmentedBar.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
        hotelDetailTableView.delegate = self
        hotelDetailTableView.dataSource = self
        hotelDetailTableView.reloadData()
    }
    
}



