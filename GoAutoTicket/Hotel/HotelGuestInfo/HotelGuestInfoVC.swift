//
//  HotelGuestInfoVC.swift
//  GoHotelTicket
//
//  Created by iMac on 06/01/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class HotelGuestInfoVC: UIViewController {
    @IBOutlet weak var guestInfoTableView: UITableView!
    var sectionArray = ["Passenger Details","Guests"]
    var nextCount = 0
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBOutlet weak var nextArrowImageView: UIImageView!
    @IBOutlet weak var backArrowImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        if nextCount<2{
            backButtonOutlet.isHidden = false
            backArrowImageView.isHidden = false
            nextCount = nextCount + 1
        }
        guestInfoTableView.reloadData()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        if nextCount <= 2{
             nextCount = nextCount - 1
        }else{
            backButtonOutlet.isHidden = true
            backArrowImageView.isHidden = true
        }
        guestInfoTableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HotelGuestInfoVC{
    
    fileprivate func setUI(){
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.title = "Hotel"
        backButtonOutlet.isHidden = true
        backArrowImageView.isHidden = true
        guestInfoTableView.delegate = self
        guestInfoTableView.dataSource = self
        guestInfoTableView.reloadData()
    }
}

extension HotelGuestInfoVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if nextCount == 1{
            return 1
        }else if nextCount == 2{
            return 1
        }else{
          return 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if nextCount == 2{
            return 4
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray
       
        let label = UILabel(frame: CGRect(x: 20, y: 10, width:
        30, height: 30))
        label.backgroundColor = .white
        label.textColor = .lightGray
        label.textAlignment = .center
        
        let headerLabel = UILabel(frame: CGRect(x: 60, y: 10, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "Verdana", size: 20)
        headerLabel.textColor = UIColor.white
        if nextCount == 1{
            
           headerLabel.text = "Contact Details"
        }else if nextCount == 2{
            headerLabel.text = "Travel"
        }else {
            headerLabel.text = sectionArray[section]
        }
        label.layer.cornerRadius =  label.frame.width/2
        label.layer.masksToBounds = true
        label.text = "\(section + 1)"
        headerLabel.sizeToFit()
        headerView.addSubview(label)
        headerView.addSubview(headerLabel)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if nextCount == 1{
            return 600
        }else if nextCount == 2{
            if indexPath.row == 0 {
                return 226
            }else if indexPath.row == 1 {
                return 40.0
            }else if indexPath.row == 2 {
                return 201.0
            }else {
                return 140.0
            }
        }else{
            if indexPath.section == 0 {
                return 78.0
            }else{
                return 511.0
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if nextCount == 1{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelGuestContactDetailCell) as? HotelGuestContactDetailCell else {
                return UITableViewCell()
            }
            return cell
        }else if nextCount == 2{
            
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelGuestAddressCell) as? HotelGuestAddressCell else {
                    return UITableViewCell()
                }
                return cell
            }else if indexPath.row == 1{
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelGuestPriceDetailCell) as? HotelGuestPriceDetailCell else {
                    return UITableViewCell()
                }
                return cell
                
            } else if indexPath.row == 2{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelDetailRoomCell) as? HotelDetailRoomCell else {
                    return UITableViewCell()
                }
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelGuestSpecialRequestCell) as? HotelGuestSpecialRequestCell else {
                    return UITableViewCell()
                }
                return cell
            }
            
        } else{
            if indexPath.section == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelGuestInfoCell) as? HotelGuestInfoCell else {
                    return UITableViewCell()
                }
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: HotelConstant.kHotelGuestDetailCell) as? HotelGuestDetailCell else {
                    return UITableViewCell()
                }
                return cell
            }
        }
        
    }
    
}
