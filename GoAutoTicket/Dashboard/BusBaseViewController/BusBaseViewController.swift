//
//  BusBaseViewController.swift
//  GoAutoTicket
//
//  Created by Tushar Lama on 07/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

class BusBaseViewController: UIViewController {

    var webview = UIWebView()
    var crossButton = UIButton()
    var backgroundView = UIView()
    var titleLabel = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setViewWithAnimation(view: UIView, hidden: Bool) {
           UIView.transition(with: view, duration: 0.6, options: .transitionCrossDissolve, animations: {
               view.isHidden = hidden
           })
       }
    
    func title(){
      //  self.navigationItem.title = "LC TRAVEL PLANNERS"
       // self.title = "LC TRAVEL PLANNERS"
    }
 
    func hideNavigationBar(){
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func showSpinner(){
       //SwiftSpinner.show("Loading...")
    }
    
    func hideSpinner(){
        DispatchQueue.main.async {
//            SwiftSpinner.hide()
        }
    }
    
    func makeNavigationBarTransparent(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func changeNavigationBackButtonColor(color:UIColor){
        self.navigationController?.navigationBar.tintColor = color
    }
    
    func changeNavigationTitleColor(color: UIColor){
        let textAttributes = [NSAttributedString.Key.foregroundColor:color]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func showNetworkError(){
      // Utility.alert(message: "Sorry some issue in network", title: "ERROR!!!", controller: self)
    }
    
    func showServiceError(){
       // Utility.alert(message: "Service Error", title: "ERROR!!!", controller: self)
    }
    
    func noInternetConnection(){
       // Utility.alert(message:"No Internet Connection", title: "ERROR!!!", controller: self)
        
    }
    
//    func sendUserToLoginPage(){
//
//        if let appdelegate = UIApplication.shared.delegate as? AppDelegate, let tabController = (appdelegate.window?.rootViewController as? TabBar) {
//            Utility.resetViewOnLogout(tabBarController: tabController)
//
//            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
//            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: Constants.kLoginViewController)
//            appDelegate.window?.rootViewController = initialViewController
//            appDelegate.window?.makeKeyAndVisible()
//        }
//    }
    
    func setInitialView(identifier: String){
//        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
//        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: identifier)
//        appDelegate.window?.rootViewController = initialViewController
//        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    func showContentInWebView(htmlString: String,title: String){
        
        backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        backgroundView.center = self.view.center
       // backgroundView.setBorderPropertiesForView(color: .clear)
        titleLabel = UILabel(frame: CGRect(x: 20, y: 5, width: backgroundView.frame.size.width - 40, height: 25))
        titleLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
        titleLabel.text = title
        webview = UIWebView(frame: CGRect(x: 20, y: backgroundView.frame.size.height/4, width:backgroundView.frame.size.width - 40 , height: 350))
        webview.clipsToBounds = true
        webview.layer.cornerRadius = 10.0
        webview.backgroundColor = .white
        webview.loadHTMLString(htmlString, baseURL: nil)
        crossButton = UIButton(frame: CGRect(x: backgroundView.bounds.width - 30, y: webview.frame.origin.y - 25, width: 25, height: 25))
        crossButton.setImage(UIImage(named: "new_CRoss.png"), for: .normal)
        crossButton.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)

        self.backgroundView.addSubview(webview)
        self.backgroundView.addSubview(titleLabel)
        self.backgroundView.addSubview(crossButton)
        self.view.addSubview(backgroundView)
    }
    
    @objc func buttonClicked() {
        self.backgroundView.removeFromSuperview()
        self.webview.removeFromSuperview()
        self.crossButton.removeFromSuperview()
        self.titleLabel.removeFromSuperview()
    }
    
   
    
}
