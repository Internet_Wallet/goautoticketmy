//
//  BusLocationSearchViewController.swift
//  GoAutoTicket
//
//  Created by iMac on 27/08/2020.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

class BusLocationSearchViewController: UIViewController {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var busSearchBar: UISearchBar!
    @IBOutlet var bussearchListTableView: UITableView!
    
    var leavingstr: String = ""
    var goStr : String = ""
    var locationLat : String = ""
    var locationLong : String = ""
    
    var  buslocationArray = [String]()
    var  mainbuslocationArray = [[String : Any]]()
    
    var locationSectionTitles = [String]()
    
    var bussearchparams:[String : Any] = [:]
    
    weak var applybuttondelegate : BusSearchViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        self.location()
     
        busSearchBar.delegate = self
        busSearchBar.showsCancelButton = false
        if #available(iOS 13.0, *) {
            busSearchBar.searchTextField.textColor = .white
        } else {
            // Fallback on earlier versions
        }
        
    }
    func locationparamsapi(){
        
        var locationparams:[String : Any] = [:]
        
        locationparams = [
             "Location": [
                 "Latitude": locationLat,
                 "Longitude": locationLong
             ],
             "Request": ""
             ]
        print("Location Params", locationparams)
        self.apirequestbussearchcall(parameter:locationparams)
    }
    
    func location(){
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
            VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
//                        if let street = addressDic["street"]  {
//                            self.township = street
//                        }
//                        if let region = addressDic["region"]  {
//                            self.devision = region
//                        }
//                        self.googleAddress = "\(addressDic)"
                        
                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                        self.locationparamsapi()
                    }
                   
                }
            }
        }
        else{
           // showLocationDisabledpopUp()
        }
    }
    
    @IBAction func backbuttonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: false)
    }
    
   

}
extension BusLocationSearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.mainbuslocationArray.count > 0{
           return self.mainbuslocationArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                return self.mainbuslocationArray[section]["type"] as? String
            }
        case 1:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                 return self.mainbuslocationArray[section]["type"] as? String
            }
        case 2:
            if self.tableView(tableView, numberOfRowsInSection: section) > 0 {
                return self.mainbuslocationArray[section]["type"] as? String
            }
        default:
            return nil // when return nil no header will be shown
        }
        return nil
      
        
       
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if let val = mainbuslocationArray[section]["item"] {
            return (val as AnyObject).count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        guard let headerView = view as? UITableViewHeaderFooterView else { return }
        headerView.textLabel?.textColor = .red
        headerView.backgroundView?.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // var cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BusLocationSearchTableViewCell
         var cell:BusLocationSearchTableViewCell? = self.bussearchListTableView.dequeueReusableCell(withIdentifier: "Cell") as? BusLocationSearchTableViewCell
        if cell == nil {
           cell = BusLocationSearchTableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
//        let sampleDict = self.mainbuslocationArray[indexPath.section]
//        if let val = sampleDict["item"] {
//            let dictTemp:[String:Any] = val[indexPath.row]
//            cell?.locationnameLabel.text = dictTemp["name"] as? String
//        }
        
         if let val = self.mainbuslocationArray[indexPath.section]["item"] {
            let itemDict:[[String:Any]] = val as!  [[String:Any]]
            cell?.locationnameLabel.text = itemDict[indexPath.row]["name"] as? String
        }
            
        cell?.selectionStyle = .none
        
        // cell.airportcodeLabel.text = airportcode[indexPath.row]
        //  cell.airportnameLabel.text = airportname[indexPath.row]
        
         self.bussearchListTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:BusLocationSearchTableViewCell? = self.bussearchListTableView.dequeueReusableCell(withIdentifier: "Cell") as? BusLocationSearchTableViewCell
         var itemDict:[[String:Any]]?
        if let val = self.mainbuslocationArray[indexPath.section]["item"] {
            itemDict = val as?  [[String:Any]]
            cell?.locationnameLabel.text = itemDict?[indexPath.row]["name"] as? String
        }
    
        if leavingstr == "Leaving"{
            
            //  UserDefaults.standard.set(cell.airportnameLabel.text, forKey: "LeavingFrom")
            //  UserDefaults.standard.synchronize()
            self.applybuttondelegate?.BusSelected(LocationDic: itemDict?[indexPath.row] ?? [:], LocationName:  cell?.locationnameLabel.text ?? "", status: "Leaving")
        }
            
        else
        {
            self.applybuttondelegate?.BusSelected(LocationDic:itemDict?[indexPath.row] ?? [:], LocationName: cell?.locationnameLabel.text ?? "", status: "Going")
        }
        
        
        self.navigationController?.popViewController(animated: false)
    }
}

extension BusLocationSearchViewController : UISearchBarDelegate{
    // Search Bar Delegate
     
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
      {
        //  UnApprovedvendorListArray = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
          var tempArray = [String]()
          if searchText.count == 0
          {
              bussearchListTableView.reloadData()
              return
          }
          if searchText.count >= 3{
            bussearchparams = [

            "Location": [
                "Latitude": locationLat,
                "Longitude": locationLong
            ],
            "Request": searchText
            ]
            self.apirequestbussearchcall(parameter: bussearchparams)
          }
          for i in 0..<mainbuslocationArray.count
          {
            let vendorDict:[[String:Any]] = [mainbuslocationArray[i]]
             let vendorId: String = String(describing: vendorDict)
             if substring(searchText, existsIn: vendorId)
              {
                  tempArray.append(vendorId)
              }
          }
          buslocationArray = tempArray
        //  bussearchListTableView.reloadData()
      }
      
      func substring(_ substr: String, existsIn str: String) -> Bool
      {
          if str.range(of: substr) != nil
          {
              return true
          }
          return false
      }
      
      func searchBarTextDidBeginEditing(_ templeSearchBar: UISearchBar)
      {
          busSearchBar.setShowsCancelButton(true, animated: true)
          bussearchListTableView.allowsSelection = false
          bussearchListTableView.isScrollEnabled = true
          var searchBarTextField: UITextField? = nil
          for mainview: UIView in busSearchBar.subviews {
              for subview: UIView in mainview.subviews {
                  if (subview is UITextField) {
                      searchBarTextField?.backgroundColor = UIColor.clear
                      searchBarTextField = (subview as? UITextField)
                      break
                  }
              }
          }
          searchBarTextField?.enablesReturnKeyAutomatically = false
      }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
         bussearchListTableView.allowsSelection = true
    }
      
      func updateSearchResults(for searchController: UISearchController)
         {
             let searchText: String? = busSearchBar.text
             if searchText?.count == 0
             {
                // airportname = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
                 bussearchListTableView.reloadData()
                 return
             }
         }
      
      func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
      {
          busSearchBar.showsCancelButton = false
          busSearchBar.resignFirstResponder()
       //   flightSearchBar = UserDefaults.standard.value(forKey: "UnApprovedList") as! [Any]
          bussearchListTableView.reloadData()
      }
      
      func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
      {
          busSearchBar.showsCancelButton = false
          busSearchBar.resignFirstResponder()
          // Do the search...
      }
    
}
extension BusLocationSearchViewController: ApiRequestProtocol{
    
    
    func apirequestbussearchcall(parameter:Any){
        //   AppUtility.showLoading(self.view)
           let apiReqObj = ApiRequestClass()
           apiReqObj.customDelegate = self
           DispatchQueue.main.async {
               print("****",parameter)
            apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/bus/search/location")!, requestData:parameter, requestType: RequestType.RequestBusLocationSearch, httpMethodName: "POST")
           }
       }
       
       func httpResponse(responseObj: Any, reqType: RequestType)
       {
           DispatchQueue.main.async{
               
               AppUtility.hideLoading(self.view)
               
               if let respMsg = responseObj as? String
               {
                   AppUtility.alert(message: respMsg, title: "", controller: self)
               }
               else
               {
                   
                   var respDict = [String:Any]()
                   if responseObj is [Any]
                   {
                      // self.tokenarray = responseObj as! [Any]
                   }
                   else if responseObj is [String:Any]
                   {
                       respDict = responseObj as! Dictionary<String,Any>
                       
                   }
                   
                   if reqType == RequestType.RequestTypeEnvironment
                   {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                  
                                
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }

                       else
                       {
                          AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                   }
                   else if reqType == RequestType.RequestBusLocationSearch {
                       if respDict.count > 0
                       {
                           
                           let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                           let code = itemDict["code"] as? Int
                           let message = itemDict["message"] as? String
                           
                           print("#####",code as Any)
                           if let isValidCode = code {
                               if isValidCode == 0 {
                                self.mainbuslocationArray.removeAll()
                                self.mainbuslocationArray = (respDict["response"] as? [[String : Any]])!
//                                for i in 0..<arrItem.count
//                                  {
//
//                                    if let sectiontitle = arrItem[i]["type"]{
//                                       println_debug(sectiontitle)
//                                        self.locationSectionTitles.append(sectiontitle as! String )
//                                        if let val = arrItem[i]["item"] {
//                                            let itemDict:[[String:Any]] = val as!  [[String:Any]]
//                                            self.mainbuslocationArray.append(contentsOf: itemDict)
//                                        }
//                                    }
//
//                                }
//
                               
                                print(self.mainbuslocationArray)
                                self.bussearchListTableView.reloadData()
                                
                                // AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                               else
                               {
                                   AppUtility.alert(message: message ?? "", title: "", controller: self)
                               }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                           
                       }
                       else
                       {
                           AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                       }
                       
                   }
                   else{
                       AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                   }
               }
           }
       }
    
}

