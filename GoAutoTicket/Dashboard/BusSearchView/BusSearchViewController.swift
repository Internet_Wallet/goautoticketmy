//
//  BusSearchViewController.swift
//  GoAutoTicket
//
//  Created by iMac on 04/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

protocol BusSearchViewControllerDelegate : class {
    func BusSelected(LocationDic :[String:Any], LocationName: String, status:String)
}

@available(iOS 13.0, *)
class BusSearchViewController: UIViewController, BusSearchViewControllerDelegate {
    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBOutlet weak var returnDateTxtField: ACFloatingTextfield!
    @IBOutlet weak var departureDateTxtField: ACFloatingTextfield!
    @IBOutlet weak var switchTripButton: UIButton!
    @IBOutlet weak var goingToTxtField: ACFloatingTextfield!
    @IBOutlet weak var leavingFromTxtField: ACFloatingTextfield!
    @IBOutlet weak var roundTripSwicth: UISwitch!
    let datePickerForBooking = UIDatePicker()
    var isPlayingDeparture = false
  
    var Leavingdict: [String:Any]?
    var Goingdict: [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bus"
        self.navigationController?.navigationBar.isHidden = false
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Bus"
        self.navigationController?.navigationBar.isHidden = false
        setUI()
    }
    
    func BusSelected(LocationDic: [String:Any], LocationName: String, status: String) {
        print(LocationName,status)
        
        if status == "Leaving" {
            leavingFromTxtField.text = LocationName
            Leavingdict = LocationDic
            print("######",Leavingdict ?? "")
        } else {
            goingToTxtField.text = LocationName
            Goingdict = LocationDic
            print("######",Goingdict ?? "")
        }
    }
    
    @IBAction func onClickRoundTrip(_ sender: UISwitch) {
        if sender.isOn{
            returnDateTxtField.isHidden = false
        }else{
            returnDateTxtField.isHidden = true
        }
        
    }
    
    @IBAction func onClickSwitchTrip(_ sender: Any) {
            let value = leavingFromTxtField.text
            leavingFromTxtField.text = goingToTxtField.text
            goingToTxtField.text = value
    }
    
    @IBAction func onClickSearchButton(_ sender: Any) {
        
        
        if leavingFromTxtField.text == ""{
            AppUtility.alert(message: "Please Enter leaving from city".localized, title: "", controller: self)
        }
        else if goingToTxtField.text == ""{
            AppUtility.alert(message: "Please Enter going to city".localized, title: "", controller: self)
        }
        else if leavingFromTxtField.text == goingToTxtField.text{
           AppUtility.alert(message: "The From and To city cannot be same".localized, title: "", controller: self)
        }
        else if departureDateTxtField.text == ""{
            AppUtility.alert(message: "Please Select depart date".localized, title: "", controller: self)
        }
        if roundTripSwicth.isOn == true{
            if returnDateTxtField.text == ""{
               AppUtility.alert(message: "Please Select return date".localized, title: "", controller: self)
            }
        }
        else {
            let busVcObj = self.storyboard?.instantiateViewController(withIdentifier: Constants.kBusSearchResultViewController) as? BusSearchResultViewController
            
            
            busVcObj?.fromLocation = Leavingdict ?? [:]
            busVcObj?.toLocation = Goingdict ?? [:]
            busVcObj?.startdate = departureDateTxtField.text ?? ""
            busVcObj?.enddate = returnDateTxtField.text ?? ""
            if let validObj = busVcObj{
                self.navigationController?.pushViewController(validObj, animated: true)
            }
        }
       
        
        
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

@available(iOS 13.0, *)
extension BusSearchViewController{
    
    fileprivate func setUI(){
        datePickerForBooking.datePickerMode = .date
        datePickerForBooking.addTarget(self, action: #selector(self.updateTextFieldForDOB), for: .valueChanged)
        searchButtonOutlet.updateLayerProperties()
        returnDateTxtField.isHidden = true
        roundTripSwicth.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        let colorTop =  UIColor(red: 255.0/255.0, green: 197.0/255.0, blue: 191.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 129.0/255.0, green: 20.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    @objc func updateTextFieldForDOB(_ sender: UIDatePicker,textField : UITextField) {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd"
        if isPlayingDeparture{
            departureDateTxtField.text = dateFormatter.string(from: sender.date)
        }else{
            returnDateTxtField.text = dateFormatter.string(from: sender.date)
        }
    }
}


@available(iOS 13.0, *)
extension BusSearchViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case leavingFromTxtField: 
            guard let homeVC = UIStoryboard(name: "BusMain", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: BusLocationSearchViewController.self)) as? BusLocationSearchViewController else { return }
            homeVC.leavingstr = "Leaving"
            homeVC.applybuttondelegate = self 
            navigationController?.pushViewController(homeVC, animated: true)
            textField.resignFirstResponder()
            
        case goingToTxtField:
            guard let homeVC = UIStoryboard(name: "BusMain", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: BusLocationSearchViewController.self)) as? BusLocationSearchViewController else { return }
            homeVC.goStr = "Going"
            homeVC.applybuttondelegate = self
            navigationController?.pushViewController(homeVC, animated: true)
            textField.resignFirstResponder()
            
        case departureDateTxtField:
            isPlayingDeparture = true
            textField.inputView = datePickerForBooking
        case returnDateTxtField:
            isPlayingDeparture = false
            textField.inputView = datePickerForBooking
        default:
            break
        }
    }
}
