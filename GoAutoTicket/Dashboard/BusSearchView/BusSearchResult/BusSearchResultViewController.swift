//
//  BusSearchResultViewController.swift
//  GoAutoTicket
//
//  Created by iMac on 05/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class BusSearchResultViewController: UIViewController {

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var resultTableView: UITableView!
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var filterButton: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet var fromLabel: UILabel!
    @IBOutlet var toLabel: UILabel!
    
    var collectionArray = ["Operator","Departure","Arrival","Rate"]
    
    var startdate = ""
    var enddate = ""
    
    var fromLocation: [String : Any] = [:]
    var toLocation : [String : Any] = [:]
    
     var bussearchparams:[String : Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.isHidden = true
         setUI()
        bussearchparams = [
            "request": [
            "criteriaInfo": [[
              "locationInfo":[
                 "fromLocation":
                 fromLocation,
               "toLocation":
                toLocation
                ],
              "dateInfo": [
                "startDate": startdate,
                "endDate": enddate
                ]
                ]],
            "Code": "",
            "Business": "bus"
            ],
            "flags": [:]
        ]
        print("@@@@@", bussearchparams)
        self.apirequestbussearchcall()
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: false)
    }
    
}

extension BusSearchResultViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.kBusResultTableViewCell) as? BusResultTableViewCell else{
            return UITableViewCell()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if #available(iOS 13.0, *) {
            if let isValidObj = self.storyboard?.instantiateViewController(identifier: "BusSeatBookingController") as? BusSeatBookingController{
                self.navigationController?.pushViewController(isValidObj, animated: true)
            }
        } else {
            // Fallback on earlier versions
        }
    }
}


extension BusSearchResultViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.kResultCollectionviewCell, for: indexPath) as? ResultCollectionviewCell else{
            return UICollectionViewCell()
        }
        cell.infoLabel.text = collectionArray[indexPath.row]
        
        if indexPath.row == 0 {
            cell.topArrowImageView.isHidden = false
        }else{
           cell.topArrowImageView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: UIScreen.main.bounds.size.width/4.5, height: 100.0)
    }
    
}

extension BusSearchResultViewController{
    
    fileprivate func setUI(){

      //  navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

      //  self.title = "Yangon -> Mandalay"
        resultTableView.backgroundView = UIImageView(image: UIImage(named: "busBackground"))
        resultTableView.delegate = self
        resultTableView.dataSource = self
        resultTableView.reloadData()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        
        
    }
}


extension BusSearchResultViewController : ApiRequestProtocol{
    func apirequestbussearchcall(){
            //   AppUtility.showLoading(self.view)
               let apiReqObj = ApiRequestClass()
               apiReqObj.customDelegate = self
               DispatchQueue.main.async {
                apiReqObj.sendHttpRequest(requestUrl: URL(string:"https://preprod-coreapi.goautoticket.com/api/v1/bus/search")!, requestData:self.bussearchparams, requestType: RequestType.RequestBusSearch, httpMethodName: "POST")
               }
           }
           
           func httpResponse(responseObj: Any, reqType: RequestType)
           {
               DispatchQueue.main.async{
                   
                   AppUtility.hideLoading(self.view)
                   
                   if let respMsg = responseObj as? String
                   {
                       AppUtility.alert(message: respMsg, title: "", controller: self)
                   }
                   else
                   {
                       
                    var respDict = [String:Any]()
                    if let resposneDataFromServer = responseObj as? [String : Any] {
                        respDict = resposneDataFromServer
                    }
                    
                       if reqType == RequestType.RequestTypeEnvironment
                       {
                           if respDict.count > 0
                           {
                               
                               let itemDict:[String:Any] = respDict["status"] as!  Dictionary<String,Any>
                               let code = itemDict["code"] as? Int
                               let message = itemDict["message"] as? String
                               
                               print("#####",code as Any)
                               if let isValidCode = code {
                                   if isValidCode == 0 {
                                      
                                    
                                   }
                                   else
                                   {
                                       AppUtility.alert(message: message ?? "", title: "", controller: self)
                                   }
                               }
                               else
                               {
                                   AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                               }
                               
                               
                           }

                           else
                           {
                              AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                       }
                       else if reqType == RequestType.RequestBusSearch {
                           if respDict.count > 0
                           {
                               if let itemDict:[String:Any] = respDict["status"] as? [String : Any] {
                              //  if  let innerdict:[String:Any] = itemDict["status"] as? [String : Any]{
                                    let code = itemDict["code"] as? Int
                                    let message = itemDict["message"] as? String
                                    print("#####",code as Any)
                                    if let isValidCode = code {
                                        if isValidCode == 0 {
                                            AppUtility.alert(message: message ?? "", title: "", controller: self)
                                        }
                                        else
                                        {
                                            AppUtility.alert(message: message ?? "", title: "", controller: self)
                                        }
                                    }
                                    else
                                    {
                                        AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                                    }
                              //  }
                              }
                            }
                           else
                           {
                            AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           }
                           else
                           {
                               AppUtility.alert(message: respDict["message"] as? String ?? "", title: "", controller: self)
                           }
                           
                       }
                   }
               }
           }
