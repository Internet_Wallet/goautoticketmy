//
//  ResultCollectionviewCell.swift
//  GoAutoTicket
//
//  Created by iMac on 05/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class ResultCollectionviewCell: UICollectionViewCell {
    
    @IBOutlet weak var topArrowImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
}
