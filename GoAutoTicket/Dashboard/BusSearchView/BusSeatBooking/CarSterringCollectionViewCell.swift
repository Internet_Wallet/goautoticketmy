//
//  CarSterringCollectionViewCell.swift
//  GoAutoTicket
//
//  Created by iMac on 05/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class CarSterringCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var carSterringImageView: UIImageView!
}
