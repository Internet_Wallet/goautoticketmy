//
//  BusSeatBookingController.swift
//  GoAutoTicket
//
//  Created by iMac on 05/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class BusSeatBookingController: UIViewController {

    @IBOutlet weak var seatBackGround: UIView!
    @IBOutlet weak var seatCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        self.title = "Yangon -> Mandalay"
          self.navigationController?.setNavigationBarHidden(false, animated: false)
        seatBackGround.addCornerAndShadow(cornerRadius: 2.0, shadowColor: .black)
        
        seatCollectionView.delegate = self
        seatCollectionView.dataSource = self
        seatCollectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension BusSeatBookingController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
             return 4
        }else{
            return 45
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.kCarSterringCollectionViewCell, for: indexPath) as? CarSterringCollectionViewCell else{
                return UICollectionViewCell()
            }
            
            if indexPath.row == 0 {
                cell.carSterringImageView.image = UIImage(named: "carSterring")
            }else if indexPath.row == 3{
                cell.carSterringImageView.image = UIImage(named: "stairs")
            }else{
                cell.carSterringImageView.isHidden = true
            }
            
            return cell
            
        }else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.kBusSeatCollectionViewCell, for: indexPath) as? BusSeatCollectionViewCell else{
                return UICollectionViewCell()
            }
            
            
            cell.seatLabel.text = String(indexPath.row + 1)
            return cell
            
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
        
    {
        
        if indexPath.section == 0{
            
            return CGSize(width: self.seatCollectionView.frame.width/5, height: 60.0)
            
        }else{
            if indexPath.row == 41 || indexPath.row == 42 || indexPath.row == 43 || indexPath.row == 44{
                return CGSize(width: self.seatCollectionView.frame.width/6, height: 80.0)
            }else{
                return CGSize(width: self.seatCollectionView.frame.width/5, height: 80.0)
            }
            
        }
        
        
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
//    {
//        let sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 10)
//        return sectionInset
//    }
    
}
