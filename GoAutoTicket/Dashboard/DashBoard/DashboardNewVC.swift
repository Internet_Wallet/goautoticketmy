//
//  DashboardNewVC.swift
//  GoAutoTicket
//
//  Created by AmulCGM on 26/06/21.
//  Copyright © 2021 vamsi. All rights reserved.
//

import UIKit

class DashboardNewVC: UIViewController {

    @IBOutlet weak var tblList:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblList.register(UINib(nibName: "BusListCell", bundle: nil), forCellReuseIdentifier: "BusListCell")
       
    }
    

}
extension DashboardNewVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusListCell") as! BusListCell
        return cell
    }
}
