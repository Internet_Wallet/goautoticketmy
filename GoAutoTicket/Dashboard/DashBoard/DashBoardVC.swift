//
//  DashBoardVC.swift
//  GoAutoTicket
//
//  Created by iMac on 03/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class DashBoardVC: BusBaseViewController {
    @IBOutlet weak var dashboardTableView: UITableView!
    @IBOutlet weak var busImageView: UIImageView!
    @IBOutlet weak var flightImageView: UIImageView!
    @IBOutlet weak var hotelImageView: UIImageView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var busView: UIView!
    @IBOutlet weak var flightView: UIView!
    @IBOutlet weak var hotelView: UIView!
    @IBOutlet weak var blackBackGroundView: UIView!
    @IBOutlet weak var viewForTableView: UIView!
    @IBOutlet weak var widthForViewTable: NSLayoutConstraint! //227
    @IBOutlet weak var backWidth: NSLayoutConstraint! //190
    @IBOutlet weak var sideMenuTableView: UITableView!
    @IBOutlet weak var leftBarButton: UIBarButtonItem!
    
    var viewModelObj = DashBoardViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dashboardTableView.register(UINib(nibName: "BusListCell", bundle: nil), forCellReuseIdentifier: "BusListCell")
        setUI()
        if ApplicationState.sharedAppState.currentUser.authKey == ""{
           getAuthToken()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func onClickSideMenu(_ sender: Any) {
        if blackBackGroundView.isHidden && viewForTableView.isHidden{
            sideMenuTableView.isScrollEnabled = false
            self.setViewWithAnimation(view: blackBackGroundView, hidden: false)
            self.setViewWithAnimation(view: viewForTableView, hidden: false)
        }else{
            self.setViewWithAnimation(view: blackBackGroundView, hidden: true)
            self.setViewWithAnimation(view: viewForTableView, hidden: true)
        }
    }
}

@available(iOS 13.0, *)
extension DashBoardVC{
    
    fileprivate func getAuthToken(){
        if Utility.isNetworkReachable(){
            showSpinner()
            let param = AuthParam(applicationId: HotelConstant.kApplicationID)
            print(param)
            viewModelObj.callAuthService(authParam: param, finished: {
                if self.viewModelObj.authModelObj != nil{
                    self.hideSpinner()
                    if let message = self.viewModelObj.authModelObj?.statusMessage{
                        if message == HotelServerMessage.kSuccess{
                            //Save the Auth token here in singleton
                            ApplicationState.sharedAppState.currentUser.authKey = self.viewModelObj.authModelObj?.authToken ?? ""
                            ApplicationState.sharedAppState.currentUser.applicationID = self.viewModelObj.authModelObj?.applicationID ?? ""
                            print(ApplicationState.sharedAppState.currentUser.authKey)
                            print(ApplicationState.sharedAppState.currentUser.applicationID)
                        }else{
                            self.hideSpinner()
                            Utility.alert(message: message, title: HotelServerMessage.kError, controller: self)
                        }
                    }else{
                        self.hideSpinner()
                        Utility.alert(message: HotelServerMessage.kErrorMessage, title: HotelServerMessage.kError, controller: self)
                    }
                }else{
                    self.hideSpinner()
                    Utility.alert(message: HotelServerMessage.kErrorMessage, title: HotelServerMessage.kError, controller: self)
                }
            })
        }else{
            Utility.alert(message: HotelServerMessage.kNoInterNet, title: "", controller: self)
        }
    }
}


@available(iOS 13.0, *)
extension DashBoardVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == sideMenuTableView{
            return 0
        }else{
            if section == 0{
                return 0
            }else{
                return 100.0
            }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == sideMenuTableView{
            return 1
        }else{
            return 2
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == sideMenuTableView{
            return UIView()
        }else{
           return headerView
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == sideMenuTableView{
                           return 7
        }else{
            if section == 0{
               return 1
            }else{
                return 10
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == sideMenuTableView{
            
            if indexPath.row == 0{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileCell") as? UserProfileCell else{
                    return UITableViewCell()
                }
                cell.selectionStyle = .none
                
                return cell
                
            }else{
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoCell") as? UserInfoCell else{
                               return UITableViewCell()
                           }
                cell.selectionStyle = .none
                if indexPath.row == 1{
                    cell.inFoLable.text = "Home"
                    cell.inFoLable.textColor = GoAutoTicketColor.setRed()
                    cell.infoImageView.image = UIImage(named: "home.png")
//                    cell.backgroundColor = .lightGray
                }
                else if indexPath.row == 2 {
                    cell.inFoLable.textColor = .black
                    cell.inFoLable.text = "My Bookings"
                    cell.infoImageView.image = UIImage(named: "profile")
//                    cell.backgroundColor = .white
                }
                else if indexPath.row == 3{
                    cell.inFoLable.textColor = .black
                    cell.inFoLable.text = "My Account"
                    cell.infoImageView.image = UIImage(named: "profile")
//                    cell.backgroundColor = .white
                    
                }
                else if indexPath.row == 4{
                    cell.inFoLable.textColor = .black
                    cell.inFoLable.text = "About"
                    cell.infoImageView.image = UIImage(named: "aboutUs.png")
//                    cell.backgroundColor = .white
                }
                else if indexPath.row == 5{
                    cell.inFoLable.textColor = .black
                    cell.inFoLable.text = "Write to Us"
                    cell.infoImageView.image = UIImage(named: "aboutUs.png")
//                    cell.backgroundColor = .white
                }
                else{
                    cell.inFoLable.textColor = .black
                    cell.inFoLable.text = "Sign Out"
                    cell.infoImageView.image = UIImage(named: "signOut.png")
//                    cell.backgroundColor = .white
                }
            
                return cell
                
            }
            
        }else{
            if indexPath.section == 1{
                       guard let cell = tableView.dequeueReusableCell(withIdentifier: "BusListCell") as? BusListCell else{
                           return UITableViewCell()
                       }
                      cell.selectionStyle = .none
                      cell.vwContainer.addShadowLikeAndroidView()
                       return cell
                   }else{
                       guard let cell = tableView.dequeueReusableCell(withIdentifier: "DashBoardCell") as? DashBoardCell else{
                           return UITableViewCell()
                       }
                       return cell
                   }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 3{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
                 let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyAccountViewController") as? MyAccountViewController
                 if let isValidObj = nextViewController{
                     self.navigationController?.pushViewController(isValidObj, animated: true)
             }
         }
        if indexPath.row == 6{
            
            UserDefaults.standard.removeObject(forKey: "UserLogedin")
            UserDefaults.standard.synchronize()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = .lightGray
    }

    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        // Add timer to be able see the effect
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (_) in
           cell!.contentView.backgroundColor = .white
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == sideMenuTableView{
                if indexPath.row == 0{
                     return 150.0
                }else{
                   return 50.0
                }
        }else{
            
            if indexPath.section == 0{
                return 100.0
            }else{
                return UITableView.automaticDimension
            }
        }
    }
    

    
}

@available(iOS 13.0, *)
extension DashBoardVC{
    
    fileprivate func setUI(){
        
        self.navigationController?.isNavigationBarHidden = false
        dashboardTableView.delegate = self
        dashboardTableView.dataSource = self
        dashboardTableView.reloadData()
        sideMenuTableView.delegate = self
        sideMenuTableView.dataSource = self
        sideMenuTableView.reloadData()
        busImageView.makeRounded()
        flightImageView.makeRounded()
        hotelImageView.makeRounded()
        blackBackGroundView.isHidden = true
        viewForTableView.isHidden = true
        self.title = "GAT Preprod"
        sideMenuTableView.separatorColor = .clear
        leftBarButton.tintColor = .white
                
        let busViewGesture = UITapGestureRecognizer(target: self, action:  #selector(self.onClickBus))
        self.busView.addGestureRecognizer(busViewGesture)
        
        let flightViewGesture = UITapGestureRecognizer(target: self, action:  #selector(self.onClickFlight))
        self.flightView.addGestureRecognizer(flightViewGesture)
        
        let hotelViewGesture = UITapGestureRecognizer(target: self, action:  #selector(self.onClickHotel))
        self.hotelView.addGestureRecognizer(hotelViewGesture)
        
    }
    
    @objc func onClickBus(sender : UITapGestureRecognizer) {
 
        let busVcObj = self.storyboard?.instantiateViewController(withIdentifier: Constants.kBusSearchViewController) as? BusSearchViewController
        if let validObj = busVcObj{
            self.navigationController?.pushViewController(validObj, animated: true)
        }
        
    }
    
    
    @objc func onClickFlight(sender : UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "FlightMain", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FlightHomeViewController") as? FlightHomeViewController
        if let isValidObj = nextViewController{
            self.navigationController?.pushViewController(isValidObj, animated: true)
        }
    }
    
    @objc func onClickHotel(sender : UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "HotelMain", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HotelViewController") as? HotelViewController
        if let isValidObj = nextViewController{
            self.navigationController?.pushViewController(isValidObj, animated: true)
        }
    }
    
    
    
    
}



