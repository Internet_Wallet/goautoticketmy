//
//  BusListCell.swift
//  GoAutoTicket
//
//  Created by AmulCGM on 26/06/21.
//  Copyright © 2021 vamsi. All rights reserved.
//

import UIKit

class BusListCell: UITableViewCell {

    @IBOutlet weak var img1:UIImageView!
    @IBOutlet weak var img2:UIImageView!
    @IBOutlet weak var imgIcon:UIImageView!
    
    @IBOutlet weak var lblName1:UILabel!
    @IBOutlet weak var lblName2:UILabel!
    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var vwContainer:UIView!
    @IBOutlet weak var veStack:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.vwContainer.addShadowLikeAndroidView(cornerRadius: 3)
        self.veStack.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}



extension UIView {

    func addShadowLikeAndroidView(cornerRadius:CGFloat = 3.0 , shadowOpacity:Float = 0.6 , offsetHeight:CGFloat = 1.5 ) {
    self.layer.shadowOffset = CGSize(width: 0.0, height: offsetHeight)
    self.layer.shadowOpacity = shadowOpacity
    self.layer.shadowRadius = 2.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
    self.layer.cornerRadius = cornerRadius
    self.layer.masksToBounds = false
}
   
    
}

