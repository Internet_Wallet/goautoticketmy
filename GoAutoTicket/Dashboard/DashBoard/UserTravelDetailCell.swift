//
//  UserTravelDetailCell.swift
//  GoAutoTicket
//
//  Created by iMac on 03/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class UserTravelDetailCell: UITableViewCell {

    @IBOutlet weak var travelDetailsLabel: UILabel!
    @IBOutlet weak var arrivalImageView: UIImageView!
    @IBOutlet weak var departureImageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
