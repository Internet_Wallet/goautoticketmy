//
//  UserProfileCell.swift
//  GoAutoTicket
//
//  Created by iMac on 04/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class UserProfileCell: UITableViewCell {

    @IBOutlet weak var userPRofilePic: UIImageView!
    @IBOutlet weak var lblProfileName : UILabel!
    @IBOutlet weak var vwGradiant : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
