//
//  Utility.swift
//  GoAutoTicket
//
//  Created by iMac on 05/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import Alamofire

class Utility: NSObject {
    
    
    /**
     - Parameters:
     - key: string key which is declare in localization file
     - Returns: return localization string
     */
    
//    static  func localized(key:String) ->String {
////        let bundle = Bundle.main
////        return bundle.localizedString(forKey: key, value: "", table: nil)
//
//        guard let path = Bundle.main.path(forResource: Constants.currentLanguage, ofType: "lproj"),
//            let bundle = Bundle(path: path) else {
//                return NSLocalizedString(key, tableName: nil, bundle: Bundle.main, value: "", comment: "")
//        }
//
//        return NSLocalizedString(key, tableName: nil, bundle: bundle, value: "", comment: "")
//
//    }
    
    //Present Alert with 5 action for language selection
    
    static var alertNew = UIAlertController()
    static func alertContollerNew(title: String, message: String, actionTitleFirst: String, actionTitleSecond: String, actionTitleThird: String, actionTitleFourth: String, actionTitleFifth: String, firstActoin: Selector?,secondActoin: Selector?,thirdActoin: Selector?,fourthActoin: Selector?,fifthActoin: Selector?, controller: UIViewController) {
        alertNew = UIAlertController(title: title, message: message, preferredStyle: .alert) // 1
        if(!actionTitleFirst.isEmpty) {
            let firstButtonAction = UIAlertAction(title: actionTitleFirst, style: .default) { (alert: UIAlertAction!) -> Void in
                if(firstActoin != nil){
                    controller.perform(firstActoin)
                }
            }
            alertNew.addAction(firstButtonAction)
        }
        
        if(!actionTitleSecond.isEmpty) {
            let secondAction = UIAlertAction(title: actionTitleSecond, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
                if(secondActoin != nil){
                    controller.perform(secondActoin)
                }
            }
            alertNew.addAction(secondAction)
        }
        
        if(!actionTitleThird.isEmpty) {
            let thirdAction1 = UIAlertAction(title: actionTitleThird, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
                if(thirdActoin != nil){
                    controller.perform(thirdActoin)
                }
            }
            alertNew.addAction(thirdAction1)
        }
        
        if(!actionTitleFourth.isEmpty) {
            let fourthAction = UIAlertAction(title: actionTitleFourth, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
                if(fourthActoin != nil){
                    controller.perform(fourthActoin)
                }
                
            }
            alertNew.addAction(fourthAction)
        }
        
        if(!actionTitleFifth.isEmpty) {
            let fifthAction = UIAlertAction(title: actionTitleFifth, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
            }
            alertNew.addAction(fifthAction)
        }
        
        controller.present(alertNew, animated: true, completion:nil)
    }
    
    
    
    ////////////////////////////////////////////////////////
    //Present Alert controller with title, message and events
    static var alert = UIAlertController()
    static func alertContoller(title: String, message: String, actionTitleFirst: String, actionTitleSecond: String, actionTitleThird: String, firstActoin: Selector?,secondActoin: Selector?,thirdActoin: Selector?, controller: UIViewController) {
        alert = UIAlertController(title: title, message: message, preferredStyle: .alert) // 1
        if(!actionTitleFirst.isEmpty) {
            let firstButtonAction = UIAlertAction(title: actionTitleFirst, style: .default) { (alert: UIAlertAction!) -> Void in
                if(firstActoin != nil){
                    controller.perform(firstActoin)
                }
            }
            alert.addAction(firstButtonAction)
        }
        
        if(!actionTitleSecond.isEmpty) {
            let secondAction = UIAlertAction(title: actionTitleSecond, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
                if(secondActoin != nil){
                    controller.perform(secondActoin)
                }
            }
            alert.addAction(secondAction)
        }
        
        if(!actionTitleThird.isEmpty) {
            let thirdAction = UIAlertAction(title: actionTitleThird, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
            }
            alert.addAction(thirdAction)
        }
        controller.present(alert, animated: true, completion:nil)
    }
    
    //Present Alert with title and message
    static func alert(message: String, title: String ,controller: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion:nil)
    }
    
    //Validate Null check in All model
    static func validateNull(Value : Any)->String{
        if Value is String {
            if(Value as! String == "null"){
                return ""
            }
            return Value as! String
        }else{
            return ""
        }
    }
    
//    //Convert UIImage to Base64
//    static func convertImageToBase64(image: UIImage) -> String {
//        let imageData = UIImagePNGRepresentation(image)!
//        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
//    }
    
    // Convert base64 to UIImage
    static func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    static func convertDateToString(sender: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone=TimeZone.current
        dateFormatter.dateFormat = "LLL dd,yyyy"
        return dateFormatter.string(from: sender)
    }
    
    static func convertBirthDateToString(sender: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone=TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: sender)
    }
    
    //converting the dates for home pillar
    static func convertStringToDate(date: String) -> Date {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "LLL dd,yyyy"
        let newDate = dateformatter.date(from: date)
        dateformatter.dateFormat="dd/MM/yyyy"
        if let newDate = newDate {
            return newDate as Date
        } else {
            return Date()
        }
        
    }
    
    static func convertTimeToString(time: Date) -> String{
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        timeFormatter.timeZone=TimeZone.current
        return timeFormatter.string(from: time)
    }
    
    static func convertDateToWebFormat(date:String)->String{
        let dateFormatter=DateFormatter()
        dateFormatter.dateFormat="LLL dd,yyyy"
        let dateNew=dateFormatter.date(from: date)
        dateFormatter.dateFormat="dd/MM/yyyy"
        return dateFormatter.string(from: dateNew!)
    }
    
    //Convert date to String
    static func convertDateToApiFormat(date:Date)->String{
        let dateFormatter=DateFormatter()
        dateFormatter.locale=NSLocale(localeIdentifier: "en_US") as Locale?
        dateFormatter.dateFormat="E MMM dd HH:mm:ss z yyyy"
        return dateFormatter.string(from: date)
    }
    
    static  func isNetworkReachable() -> Bool {
        return Constants.reachabilityManager?.isReachable ?? false
    }
    
    static func returnPlainString(getString: String) -> String{
        return getString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
  
    
   
   
    
    
}

