//
//  ColorHelper.swift
//  GoAutoTicket
//
//  Created by iMac on 04/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import Foundation
import UIKit

struct GoAutoTicketColor {
    
    
    static func setRed() -> UIColor{
        return UIColor(red: 129.0/255.0, green: 20.0/255.0, blue: 24.0/255.0, alpha: 1.0)
    }}
