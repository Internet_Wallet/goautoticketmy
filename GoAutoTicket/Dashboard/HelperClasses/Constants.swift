//
//  Constants.swift
//  GoAutoTicket
//
//  Created by iMac on 04/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import Foundation
import Alamofire


struct Constants {
    
    //Identifiers
    static let kBusSearchViewController = "BusSearchViewController"
    static let kUserProfileCell = "UserProfileCell"
    static let kBusSearchResultViewController = "BusSearchResultViewController"
    static let kResultCollectionviewCell = "ResultCollectionviewCell"
    static let kBusResultTableViewCell = "BusResultTableViewCell"
    static let kBusSeatBookingController = "BusSeatBookingController"
    static let kBusSeatCollectionViewCell = "BusSeatCollectionViewCell"
    static let kCarSterringCollectionViewCell = "CarSterringCollectionViewCell"
    static let kBusSeatDetailsCell = "BusSeatDetailsCell"
    static let kBusSeatArrangementCell = "BusSeatArrangementCell"
    
    //dummy String to check internet connectivity
    static let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")
}
