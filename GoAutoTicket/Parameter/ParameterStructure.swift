//
//  ParameterStructure.swift
//
//  Created by Tushar Lama on 14/09/17.
//  Copyright © 2017 Tushar Lama. All rights reserved.
//

import Foundation
import Alamofire


//MARK: AUTH PARAM

struct AuthParam: ParamsProtocol{
    var applicationId: String!
    
    func getParams() -> [String:Any] {
        return ["Request": ["ApplicationID": applicationId]];
    }
    
    func getMethod() -> HTTPMethod {
        return .post;
    }
}



