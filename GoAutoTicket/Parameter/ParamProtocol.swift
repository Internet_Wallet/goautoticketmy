//
//  ParamProtocol.swift
//
//  Created by Tushar Lama on 14/09/17.
//  Copyright © 2017 Tushar Lama. All rights reserved.
//

import Foundation
import Alamofire


protocol ParamsProtocol
{
    mutating func getParams() -> [String:Any];
    func getMethod() -> HTTPMethod;
}

protocol ParamsNestedProtocol
{
    mutating func getParams() -> [String : [String:Any]];
    func getMethod() -> HTTPMethod;
}

protocol ParamArrayType
{
    mutating func getParams() -> [String : [Array<Any>]];
    func getMethod() -> HTTPMethod;
}
