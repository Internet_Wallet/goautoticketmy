////
////  NetworkClass.swift
////  LC-Travel
////
////  Created by Tushar Lama on 01/10/18.
////  Copyright © 2018 Tushar Lama. All rights reserved.
////
//
import Foundation
import Alamofire
import SwiftyJSON
//
class NetworkClass {
    
    func callService(url : String,params:Parameters?, type:HTTPMethod , completionHandler:@escaping(_ jsonData : JSON,_ error : NSError?)->Void)
    {
        // make the request
        let headers = [
            "Content-Type": "application/json",
            "cache-control": "no-cache",
        ]
        
        Alamofire.request(url , method: type, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .response(completionHandler: { (response) in
                // handle generic data
            })
            .responseJSON { response in
                
                switch response.result{
                case .success:
                    let json : JSON = JSON(response.result.value ?? "[]");
                    debugPrint(json)
                    completionHandler(json,nil)
                case .failure(let error):
                    completionHandler(JSON.null,error as NSError)
                }
        }
    }
    
}

