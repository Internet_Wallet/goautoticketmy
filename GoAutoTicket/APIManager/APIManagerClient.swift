//
//  APIManagerClient.swift
//  NopCommerce
//
//  Created by BS-125 on 11/5/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation

class APIManagerClient: NSObject {
    
    var shoppingCartCount               = 0
  //  var AppStartUp : AppStartUpModel?   = nil
    var isRequestedToReloadOnlyOnce: Bool = false
    var versioncode = 1
    var DeviceTypeId = 5 // FOR IOS 5
    var medicineRequestId = 0 // 0 for create new request
    
    // Mohit
    enum UrlTypes {
        case productionUrl, testUrl, stagingUrl
    }
    
    let serverUrl: UrlTypes = .testUrl
    let baseURl_doctors = "https://preprod-coreapi.goautoticket.com"
    var OKPayment = ""
    var base_url                         = ""
    var base_url_sendSMS                   = ""
    var NST_KEY_Value       = ""
    var NST_SECRET_Value    = ""
    var secretKey = ""
    var productionMode = false
    var merchantID = ""
    var currencyCode = "104"
    var ApplicationID = ""
    var username = ""
    var password = ""
    
    
    func urlEnable(urlTo: UrlTypes) {
        switch urlTo {
        case .testUrl:
            base_url                         = "https://preprod-coreapi.goautoticket.com"
            base_url_sendSMS                 = "http://69.160.4.149:1009/api/vendor/VerifyMobileNumber"
          //  NST_KEY_Value                    = "bm9wU3RhdGlvblRva2Vu"
           // NST_SECRET_Value                 = "bm5xS6V5"
           
            username = ""
            password = ""
            
        case .stagingUrl:
            base_url                         = "https://preprod-coreapi.goautoticket.com"
            base_url_sendSMS                 = "http://13.250.225.91/api/vendor/VerifyMobileNumber"
       
          //  NST_KEY_Value                    = "bm9wU3RhdGlvblRva2Vu"
          //  NST_SECRET_Value                 = "bm5xS6V5"
            username = ""
            password = ""
        case .productionUrl:
            base_url                         = "https://preprod-coreapi.goautoticket.com"
            base_url_sendSMS                 = "https://onestop-kitchen.com/api/customer/VerifyMobileNumber"
           
          //  NST_KEY_Value                    = "bm9wU3RhdGlvblRva2Vu"
         //   NST_SECRET_Value                 = "bm5xS6V5"
            username = ""
            password = ""
            
        }
    }
    
 
    
    var baseURL                         = "https://preprod-coreapi.goautoticket.com"
    var defaultBaseUrl                  = "https://preprod-coreapi.goautoticket.com"
    
    
    class var sharedInstance:APIManagerClient {
        return Static.instance
    }
    private struct Static {
        static let instance = Static.createInstance()
        static func createInstance()->APIManagerClient {
            
            let myInstance = APIManagerClient()
            return myInstance
        }
    }
    override init() {
        
    }
  
}

