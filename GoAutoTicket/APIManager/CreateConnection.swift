////
////  CreateConnection.swift
////  LC-Travel
////
////  Created by Tushar Lama on 01/10/18.
////  Copyright © 2018 Tushar Lama. All rights reserved.
////
//
import Foundation
//import SwiftHash
import SwiftyJSON
//
//
class CreateConnection{
  var networkObj = NetworkClass()

        func callAuthService(param: AuthParam,completion:@escaping(_ jsonData : AuthenticateClientModel?,_ error : NSError?)->Void){
         let param = AuthParam.getParams(param)
         
        networkObj.callService(url: "https://preprod-coreapi.goautoticket.com/api/v1/auth", params: param(), type: .post, completionHandler: { (response,error) in
             
             // check for an error
             guard error == nil else {
                 completion(nil, error);
                 return;
             }
             
             // send back to caller
            completion(AuthenticateClientModel(data: response), nil)
         })
     }
    
  
}

