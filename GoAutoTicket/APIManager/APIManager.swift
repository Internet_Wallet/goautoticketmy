//
//  APIManager.swift
//  NopCommerce
//
//  Created by BS-125 on 11/4/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import JWT

open class APIManager: NSObject {
    
    var baseURL                         = APIManagerClient.sharedInstance.baseURL
    var NST_KEY_Value                   = APIManagerClient.sharedInstance.NST_KEY_Value
    var NST_SECRET_Value                = APIManagerClient.sharedInstance.NST_SECRET_Value
    
    var username = APIManagerClient.sharedInstance.username
    var password = APIManagerClient.sharedInstance.password
    var baseURL1                        = APIManagerClient.sharedInstance.base_url
    var barcodeResponse :                NSNumber?
   
    let deviceId                        = UIDevice.current.identifierForVendor?.uuidString ?? ""
    let headers                         = ["DeviceId": UIDevice.current.identifierForVendor?.uuidString ?? ""]
 
    
    
   
    
    func getHeaderDic() -> [String:String] {
        Alamofire.SessionManager.default.session.configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        println_debug("\nDevice id : \(self.deviceId)\n")
        let manager = APIManagerClient.sharedInstance
        let key = manager.username
        let secret = manager.password
        print("APP Key : \(key) And Secret : \(secret)")
        let encodedString = JWT.encode(claims: ["NST_KEY": "\(key)"], algorithm: .hs512("\(secret)".data(using: .utf8)!))
        var headerDictionary = self.headers
        if let token = Vendor_loginmodel.shared.token {
            headerDictionary = ["Token":token,"DeviceId": self.deviceId,"NST":encodedString]
            println_debug("Header bb : \(headerDictionary)\n")
        } else {
            headerDictionary = ["DeviceId": self.deviceId,"NST":encodedString]
            println_debug("Without Access Token of Header : \(headerDictionary)\n")
        }
        return headerDictionary
    }
    
    func displayErrorMessage(json: JSON) -> String {
        var errorMessage = ""
        let errorList = json["ErrorList"].arrayObject
        if errorList != nil{
            for error in errorList! {
                errorMessage += "\(error as! String)\n"
            }
        }
        return errorMessage
    }
    
    // MARK: ShoppingCartCount
    open func getShoppingCartCount(onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/categories",  APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: ShoppingCartCount
    open func SaveNotifyRequest(_ productId: String,onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "/product/SaveNotifyRequest/%@",  APIManagerClient.sharedInstance.base_url,productId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                print(json)
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    open func ScanBarcodeId(_ productId: Int,  onSuccess successBlock: @escaping ((JSON) -> Void), onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/product/GetProductIdbyBarcode/\(productId)", APIManagerClient.sharedInstance.base_url ) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                successBlock(json)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    open func RateAndReview(_ data:[String:Any],  onSuccess successBlock: ((NSNumber) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/reviews/ReviewsOfAppAndAddress", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: data, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                successBlock(self.barcodeResponse ?? 0)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    

    
    
    
    
    
    
    
    
    // MARK: Get languages from server
//    func getLanguagesFromServer(onSuccess successBlock: ((GetLanguageModel) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
//        //        let urlParameters = String(format: "%@/GetLanguage", baseURL) as String
//        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
//        let urlParameters = String(format: "%@/GetLanguage", APIManagerClient.sharedInstance.base_url) as String
//        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
//            response in
//            switch response.result {
//            case .success(let data):
//                let json = JSON(data)
//                println_debug(json)
//                println_debug("\n")
//
//                if let languageModelObj  = Mapper<GetLanguageModel>().map(JSONObject: json.dictionaryObject){
//                    println_debug("\nPlay")
//                    successBlock(languageModelObj)
//                }else{
//                    errorBlock("Could not print JSON")
//                }
//            case .failure(let error):
//                errorBlock("\(error.localizedDescription)")
//            }
//        }
//    }
    
    // MARK: Set languages In Server
    func setLanguageInServer(languageId : Int, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!){
        //        let urlParameters = String(format: "%@/SetLanguage/%d",baseURL, languageId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
        let urlParameters = String(format: "%@/SetLanguage/%d",APIManagerClient.sharedInstance.base_url, languageId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                }else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    

    
    
    open func testNewApi(){
        //        let urlParameters = String(format: "%@/v1/categories", APIManagerClient.sharedInstance.base_url) as String
        let urlParameters = String(format: "%@/catalog/homepagecategorieswithproduct", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                break
            case .failure(_): break
                
            }
        }
    }
    
   
    // MARK: TestURL
    open func testUrl(_ url : String , onSuccess successBlock: ((String) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let testUrlString = String(format: "%@/api", url)
        let urlParameters = String(format: "%@/categories", testUrlString) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200{
                    successBlock(testUrlString)
                } else{
                    let errorMessage = "Invalid URL"
                    errorBlock(errorMessage)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    

     
     open func medicineRequest(params: [String: Any],  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
     
     let urlParameters = String(format: "%@/medicine/medicinerequest", APIManagerClient.sharedInstance.base_url) as String
     Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
     switch response.result {
     case .success(let data):
     let json = JSON(data)
     let statusCode = json["StatusCode"].int
     if statusCode == 200 {
     successBlock()
     } else {
     successBlock()
     }
     case .failure(let error):
     errorBlock("\(error.localizedDescription)")
     }
     }
     }
   
    //MARK: SetPaymentMethods
    open  func setPaymentMethods(_ couponText : NSString,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/checkoutsavepaymentmethod", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":couponText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
        let urlParameters = String(format: "%@/checkout/checkoutsavepaymentmethod", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["PaymentMethod":couponText,"VersionCode":APIManagerClient.sharedInstance.versioncode], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
                
            }
        }
    }
    
 
  
    
  
    
    
   
    
    /*
     func uploadAudio(successBlock: @escaping (_ success: String) -> Void){
     let audioData: NSData = NSData()
     
     
     Alamofire.Manager.upload(.PUT,
     URL,
     headers: headers,
     multipartFormData: { multipartFormData in
     multipartFormData.appendBodyPart(data: "3".dataUsingEncoding(NSUTF8StringEncoding), name: "from_account_id")
     multipartFormData.appendBodyPart(data: "4".dataUsingEncoding(NSUTF8StringEncoding), name: "to_account_id")
     multipartFormData.appendBodyPart(data: audioData, name: "file", fileName: "file", mimeType: "application/octet-stream")
     },
     encodingCompletion: { encodingResult in
     switch encodingResult {
     
     case .Success(let upload, _, _):
     upload.responseJSON { response in
     
     }
     
     case .Failure(let encodingError): break
     // Error while encoding request:
     }
     })
     }
     */
    
    
    
    
    
    func convertToArrDictionary(text: String) -> [AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [AnyObject]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }

    
   
    
    
 
 
    
  
    
    //MARK: SignIn
    open func LoginInUser(_ params:[String: AnyObject], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
        let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/login")!
        println_debug(params)
        println_debug(url1)
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1)
        request.timeoutInterval = 90
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
        let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
        
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
        request.allHTTPHeaderFields = self.getHeaderDic()
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if let errorResponse = error {
                println_debug(errorResponse)
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if let dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                    
                                    
                                }
                            } catch {}
                        }
                    }else {
                        println_debug(httpStatus)
                        errorBlock("\(error?.localizedDescription ?? "")")
                    }
                }else {
                    println_debug("Fail")
                    errorBlock("\(error?.localizedDescription ?? "")")
                }
            }
        }
        dataTask.resume()
    }
   //MARK: Register
    open func RegisterUser(_ params:[String: AnyObject], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
        let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/registervendorapi")!
        println_debug(params)
        println_debug(url1)
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1)
        request.timeoutInterval = 90
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
        let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
        
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
        request.allHTTPHeaderFields = self.getHeaderDic()
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if let errorResponse = error {
                println_debug(errorResponse)
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if let dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                    
                                    
                                }
                            } catch {}
                        }
                    }else {
                        println_debug(httpStatus)
                        errorBlock("\(error?.localizedDescription ?? "")")
                    }
                }else {
                    println_debug("Fail")
                    errorBlock("\(error?.localizedDescription ?? "")")
                }
            }
        }
        dataTask.resume()
    }
    
    
    //MARK: ForgotUSer
     open func ForgotUser(_ params:[String: AnyObject], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
         
         let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/registervendorapi")!
         println_debug(params)
         println_debug(url1)
         
         let session             = URLSession.shared
         let request             = NSMutableURLRequest(url:url1)
         request.timeoutInterval = 90
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         //request.setValue("application/json", forHTTPHeaderField: "Accept")
         request.httpMethod = "POST"
         
         let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
         let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
         
         request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
         request.allHTTPHeaderFields = self.getHeaderDic()
         
         let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
             
             if let errorResponse = error {
                 println_debug(errorResponse)
             }else {
                 if let httpStatus = response as? HTTPURLResponse {
                     if httpStatus.statusCode == 200 {
                         if let json = data {
                             do {
                                 let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                 if let dataDict = dict as? Dictionary<String,AnyObject> {
                                     println_debug(dataDict)
                                     if let success = dataDict["StatusCode"] as? NSNumber{
                                         if success == 200 {
                                             successBlock(dataDict)
                                         }
                                         else{
                                             successBlock(dataDict)
                                         }
                                     }
                                     
                                     
                                 }
                             } catch {}
                         }
                     }else {
                         println_debug(httpStatus)
                         errorBlock("\(error?.localizedDescription ?? "")")
                     }
                 }else {
                     println_debug("Fail")
                     errorBlock("\(error?.localizedDescription ?? "")")
                 }
             }
         }
         dataTask.resume()
     }
    
    
    
    //MARK: Forgor Password
    open func forgotPassword(_ params:[String: AnyObject], onSuccess successBlock:((String?) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlParameters = String(format: "%@/customer/passwordrecovery", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                if json["StatusCode"].int == 200 {
                    let message = json["SuccessMessage"].string
                    successBlock(message)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: Customer Info
    open func loadCustomerInfo(onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/info", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock(json.dictionaryObject! as [String : AnyObject])
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    open func saveCustomerInfo(_ params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/info", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                }  else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
 
    //MARK: DeleteAddress
    open  func deleteAddress(_ addressId:NSInteger, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/address/remove/%d", baseURL, addressId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/address/remove/%d", APIManagerClient.sharedInstance.base_url, addressId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
 
    
  
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: Paypal Confirm
    open  func paypalConfirm(_ PaymentId : NSString, orderId: NSInteger ,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/checkout/checkpaypalaccount", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["PaymentId":PaymentId,"OrderId":orderId] , encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    let message = self.displayErrorMessage(json: json)
                    if (message != "") {
                        errorBlock(message)
                    } else {
                        errorBlock("Payment Failed.Please try again")
                    }
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
                
            }
        }
    }
    
    //MARK: Authorize.Net Confirm
    open func getAuthorizeDotNet(_ params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlParameters = String(format: "%@/checkout/checkauthorizepayment", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    let message = self.displayErrorMessage(json: json)
                    if (message != "") {
                        errorBlock(message)
                    }else{
                        errorBlock("Payment Failed.Please try again")
                    }
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: Redirect Payment Confirm
    open func getRedirectPayment(_ orderId: NSInteger, onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!){
        let urlParameters = String(format: "%@/checkout/checkredirectpayment", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["OrderId": orderId], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    let message = self.displayErrorMessage(json: json)
                    if (message != "") {
                        errorBlock(message)
                    }else{
                        errorBlock("Checkout Failed.Please try again")
                    }
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
  
 
 
    
  
    
    
    //MARK:- Webapi manager
    public func genericClass(url: URL, param: AnyObject, httpMethod: String, header: Bool? = false, addAddress: String? = "", hbData: Data? = nil, handle :@escaping (_ result: Any, _ success: Bool, _ data: Data?) -> Void) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:false)
                if let httpBodyData = hbData {
                    request.httpBody = httpBodyData
                }
            }
        }
        if header ?? false {
            request.allHTTPHeaderFields = self.getHeaderDic()
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false, nil)
            } else {
                do {
                    guard let dataResponse = data else {
                        handle("error", false, nil)
                        return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    handle(json, true, dataResponse)
                } catch {
                    handle(error.localizedDescription, false, nil)
                }
            }
        }
        dataTask.resume()
    }
}


extension Alamofire.SessionManager{
    @discardableResult
    open func requestWithoutCache(
        _ url: URLConvertible,
        method: HTTPMethod = .post,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = JSONEncoding.default,
        headers: HTTPHeaders? = nil)// also you can add URLRequest.CachePolicy here as parameter
        -> DataRequest
    {
        do {
            var urlRequest = try URLRequest(url: url, method: method, headers: headers)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
            return request(encodedURLRequest)
        } catch {
            // TODO: find a better way to handle error
            print(error)
            return request(URLRequest(url: URL(string: "http://example.com/wrong_request")!))
        }
    }
}
