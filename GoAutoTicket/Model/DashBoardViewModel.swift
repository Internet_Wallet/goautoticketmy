//
//  DashBoardViewModel.swift
//  GATFlightModule
//
//  Created by iMac on 22/01/2020.
//  Copyright © 2020 E J ANTONY. All rights reserved.
//

import Foundation

class DashBoardViewModel{
      var createConnectionobj =  CreateConnection()
      var authModelObj: AuthenticateClientModel?
    
    
    func callAuthService(authParam: AuthParam,finished:@escaping () -> Void){
        authModelObj = nil
        createConnectionobj.callAuthService(param:authParam , completion: {[weak self] (response,error) in
            guard let data = response else{
                finished()
                return
            }
            self?.authModelObj = data
            finished()
        })
    }
    
    
}


